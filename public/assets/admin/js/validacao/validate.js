$(document).ready(function(){

	var section = $('select[name="secao"]').val();
	var categoria = $('select[name="categoria"]').val();

	if(section > 0){
		$('#categoria').prop("disabled", true);
	}else if(categoria >0){
		$('#secao').prop("disabled", true);
	}

	$('#secao').click(function(){

		var val = $('select[name="secao"]').val();

		if(val > 0){
			$('#categoria').prop("disabled", true);
		}else{
			$('#categoria').removeAttr('disabled');
		}
	});

	$('#categoria').click(function(){

		var val = $('select[name="categoria"]').val();

		if(val > 0){
			$('#secao').prop("disabled", true);
		}else{
			$('#secao').removeAttr('disabled');
		}
	});

});