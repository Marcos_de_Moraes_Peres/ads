$(document).ready(function(){

	var setor = $('select[name="insetor"]').val();

	if(setor == 0){
		$('#category').prop("disabled", true);
	}

	$('#setor').click(function(){

		var val = $('select[name="insetor"]').val();

		if(val < 1){
			$('#category').prop("disabled", true);
			$('#category').val('');
		}else{
			$('#category').removeAttr('disabled');
		}
	});

});