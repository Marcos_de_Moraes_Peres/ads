$(document).ready(function(){

	var section = $('select[name="idsection"]').val();
	var categoria = $('select[name="idsubcategory"]').val();

	if(section > 0){
		$('#category').prop("disabled", true);
	}else if(categoria >0){
		$('#section').prop("disabled", true);
	}

	$('#section').click(function(){

		var val = $('select[name="idsection"]').val();

		if(val > 0){
			$('#category').prop("disabled", true);
		}else{
			$('#category').removeAttr('disabled');
		}
	});

	$('#category').click(function(){

		var val = $('select[name="idsubcategory"]').val();

		if(val > 0){
			$('#section').prop("disabled", true);
		}else{
			$('#section').removeAttr('disabled');
		}
	});

});