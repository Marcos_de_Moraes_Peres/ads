<div class="navbar-collapse collapse">
  <div class="col-md-10 col-md-offset-1">
    <ul class="nav navbar-nav">
      
          @foreach($menuObject as $secao)
            @if(isset($secao->linkInt))
              <li><a href="{{URL::to($secao->linkInt)}}">{{$secao->nome}}</a></li>
            @else
              <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown">{{$secao->nome}}<b class="caret"></b></a>
              <ul class="dropdown-menu">
                @foreach($secao->filhos as $categoria)
                  @if($categoria->link)
                    <li><a href="{{$categoria->link}}" target="_blank">{{$categoria->nome}}</a></li>
                  @elseif(isset($categoria->linkInt))
                    <li><a href="{{URL::to($categoria->linkInt)}}">{{$categoria->nome}}</a></li>
                  @endif
                @endforeach
              </ul>
              </li>
            @endif
          @endforeach
      </ul>
    </div>
  </div><!--/.navbar-collapse -->
</div>
</div>