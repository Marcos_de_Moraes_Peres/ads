$(document).ready(function() {

	 $('#validateNoticia').validate({
 		  errorClass: 'error',
      successClass: 'success',
      rules:{
          titulo:{
              required: true,
          },
          slug:{
              required: true,
          },
          cliente:{
              required: true,
          },
          headInicio:{
              required: true,
          }
      },
      messages:{
          titulo:{
              required: "O campo Título é obrigatório.",
          },
          slug:{
              required: "O campo Nome URL é obrigatório.",
          },
          cliente:{
              required: "O campo Cliente é obrigatório.",
          },
          headInicio:{
              required: "O campo Data de Publicação é obrigatório.",
          }
      }
    });

   $('#validateUsuario').validate({
        errorClass: 'error',
        successClass: 'success',
        rules:{
            titulo:{
                required: true,
            },
            login:{
                required: true,
            },
            password:{
                required: true,
            }
        },
        messages:{
            titulo:{
                required: "O campo Nome é obrigatório.",
            },
            login:{
                required: "O campo Login é obrigatório.",
            },
            password:{
                required: "O campo Senha é obrigatório.",
            }
        }
    });

   $('#validateFiltro').validate({
        errorClass: 'error',
        successClass: 'success',
        rules:{
            headInicio:{
                required: true,
            },
            headFim:{
                required: true,
            }
        },
        messages:{
            headInicio:{
                required: "O campo Início é obrigatório.",
            },
            headFim:{
                required: "O campo Fim é obrigatório.",
            }
        }
    }); 

    $('#validateVideo').validate({
        errorClass: 'error',
        successClass: 'success',
        rules:{
            url:{
                required: true,
            }
        },
        messages:{
            url:{
                required: "O campo URL é obrigatório.",
            }
        }
    });

   $('#validateImagem').validate({
        errorClass: 'error',
        successClass: 'success',
        rules:{
            ziparchive:{
                extension:'zip'
            },
            arquivo:{
                extension: 'pjpeg|jpeg|jpg|png|gif|bmp',
            }
        },
        messages:{
            ziparchive:{
                extension: "Insira apenas arquivos ZIP.",
            },
            arquivo:{
                extension: "Insira apenas Imagens"
            }
        }
    });

   $('#validateCase').validate({
      errorClass: 'error',
      successClass: 'success',
      rules:{
          titulo:{
              required: true
          },
          slug:{
              required: true
          },
          servico:{
              required: true
          }
      },
      messages:{
          titulo:{
              required: "O campo Título é obrigatório."
          },
          slug:{
              required: "O campo Nome URL é obrigatório."
          },
          servico:{
              required: "O campo Serviço é obrigatório."
          }
      }
    });

   $('#validateAcaoNoticia').validate({
      errorClass: 'error',
      successClass: 'success',
      rules:{
          titulo:{
              required: true
          },
          slug:{
              required: true
          },
          resumo:{
              required: true
          },
          arquivo:{
              required: true
          }
      },
      messages:{
          titulo:{
              required: "O campo Título é obrigatório."
          },
          slug:{
              required: "O campo Nome URL é obrigatório."
          },
          resumo:{
              required: "O campo Resumo é obrigatório."
          },
          arquivo:{
              required: "O campo Imagem é obrigatório."
          }
      }
    });

   $('#validateBolhetim').validate({
      errorClass: 'error',
      successClass: 'success',
      rules:{
          titulo:{
              required: true
          },
          arquivo:{
              required: true
          },
          slug:{
              required: true
          },
          noticia1:{
              required: true
          },
          noticia2:{
              required: true
          },
          noticia3:{
              required: true
          },
          noticia4:{
              required: true
          },
      },
      messages:{
          titulo:{
              required: "O campo Título é obrigatório."
          },
          slug:{
              required: "O campo Nome URL é obrigatório."
          },
          arquivo:{
              required: "O campo Imagem é obrigatório."
          },
          noticia1:{
              required: "O campo Noticia 1 é obrigatório."
          },
          noticia2:{
              required: "O campo Noticia 2 é obrigatório."
          },
          noticia3:{
              required: "O campo Noticia 3 é obrigatório."
          },
          noticia4:{
              required: "O campo Noticia 4 é obrigatório."
          },
      }
    });

   $('#validateDestaque').validate({
      errorClass: 'error',
      successClass: 'success',
      rules:{
          titulo:{
              required: true
          },
          descricao:{
              required: true
          },
          link:{
              required: true
          }
      },
      messages:{
          titulo:{
              required: "O campo Título é obrigatório."
          },
          descricao:{
              required: "O campo Descrição é obrigatório."
          },
          link:{
              required: "O campo Link é obrigatório."
          }
      }
    });

   $('#validateSecao').validate({
      errorClass: 'error',
      successClass: 'success',
      rules:{
          titulo:{
              required: true,
          },
          slug:{
              required: true,
          }
      },
      messages:{
          titulo:{
              required: "O campo Título é obrigatório.",
          },
          slug:{
              required: "O campo Nome URL é obrigatório.",
          }
      }
    });

   $('#validateDepoimento').validate({
      errorClass: 'error',
      successClass: 'success',
      rules:{
          conteudo:{
              required: true,
          },
          nome:{
              required: true,
          },
          idcliente:{
              required: true,
          }
      },
      messages:{
          conteudo:{
              required: "O campo Conteudo é obrigatório.",
          },
          nome:{
              required: "O campo Nome é obrigatório.",
          },
          idcliente:{
              required: "O campo Empresa é obrigatório.",
          }
      }
    });

   $('#validateSlide').validate({
      errorClass: 'error',
      successClass: 'success',
      rules:{
          titulo:{
              required: true,
          },
          descricao:{
              required: true,
          },
          link:{
              required: true,
          }
      },
      messages:{
          titulo:{
              required: "O campo Título é obrigatório.",
          },
          descricao:{
              required: "O campo Descrição é obrigatório.",
          },
          link:{
              required: "O campo Link é obrigatório.",
          }
      }
    });



////////////////////// Máscaras

$('#tel').focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
        element.mask("(99) 99999-999?9");
    } else {
        element.mask("(99) 9999-9999?9");
    }
}).trigger('focusout');

$("#cep").mask("99999-999");


/////////////////// Confirmação

$('.excluir').click(function() {
  return confirm("Tem certeza que deseja excluir o registro?");
});

 //////////////////// Date Picker

var dateToday = new Date();
var dates = $("#headInicio, #headFim").datepicker({
      dateFormat: 'dd/mm/yy',
      dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
      dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
      dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
      monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
      monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
      nextText: 'Próximo',
      prevText: 'Anterior',
      onSelect: function(selectedDate) {
        var option = this.id == "headInicio" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
        dates.not(this).datepicker("option", option, date);
     }
});

/////////////////////  Toogle Idiomas

$('#toogleEn').click(function(){
    $(".ingles").slideToggle('slow');
});


///////////////////// Sortable

    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();

///////////////////// MultiSelect


    var secondConfigurationSet = {
      includeSelectAllOption: true,
      enableFiltering: true
    };

     function rebuildMultiselect(options) {
      $('#pageGal').multiselect('setOptions', options);
      $('#pageGal').multiselect('rebuild');

      $('#productGal').multiselect('setOptions', options);
      $('#productGal').multiselect('rebuild');

      $('#noticia1').multiselect('setOptions', options);
      $('#noticia1').multiselect('rebuild');

      $('#noticia2').multiselect('setOptions', options);
      $('#noticia2').multiselect('rebuild');

      $('#noticia3').multiselect('setOptions', options);
      $('#noticia3').multiselect('rebuild');

      $('#noticia4').multiselect('setOptions', options);
      $('#noticia4').multiselect('rebuild');
    }

    rebuildMultiselect(secondConfigurationSet);

/////////////////////  Fechar alert

    $('.alert').fadeOut(3000);


/////////////////////  Ajax

    $('#consult').click(function(){

        var idioma =  $('select[name="idioma"]').val();

       $.post(
            "adminSpotlight/check",
            {
                ididioma : idioma
            },
            function(data){

                $('input:text').val('');
                $('textarea').val('');

                var index = data.destaques.length;
                if(index){
                    for(i = 0; i < index; i++){
                        $('#titulo'+i).val(data.destaques[i].titulo);
                        $('#iddestaque'+i).val(data.destaques[i].id);
                        $('#descricao'+i).val(data.destaques[i].descricao);
                        $('#link'+i).val(data.destaques[i].link);
                    }
                }
            }
        );
    });
});