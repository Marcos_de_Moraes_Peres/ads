<?php

class HomeNoticiaController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug)
	{
		$ididioma = Session::get('idioma');

		if($ididioma == 1){
			$noticia = DB::select("Select a.* from tb_noticias a left join tb_noticias b on a.id = b.idtraducao
								   where b.slug = '$slug' and a.ididioma = $ididioma");


			//Se vier da tradução redireciona
			if($noticia){
				return Redirect::to('noticia/'.$noticia[0]->slug);
			}else{
				$noticia = DB::select("Select a.*, b.nome as cliente from tb_noticias a left join tb_clientes b
									   on a.idcliente = b.id where a.slug = '$slug'");

				$clientes = DB::select("Select a.nome, a.slug from tb_clientes a inner join tb_noticias b on b.idcliente = a.id
										where a.status = 1 and a.ididioma = 1 group by a.nome order by a.nome");

				$secao = DB::select("Select slug from tb_paginas where idsecao = 18 and ididioma = 1");

				$imagens = DB::select("Select c.link from tb_noticias a
									   left join tb_galerias_locais b on a.id = b.idnoticia
									   left join tb_imagens c on b.idgaleria = c.idgaleria
									   where a.slug = '$slug' order by c.ordem");

				$videos = DB::select("Select c.link from tb_noticias a
									   left join tb_galerias_locais b on a.id = b.idnoticia
									   left join tb_videos c on b.idgaleria = c.idgaleria
									   where a.slug = '$slug' order by c.ordem");
			}
		}else{

			$noticia = DB::select("Select b.* from tb_noticias a left join tb_noticias b 
								  on a.id = b.idtraducao where a.slug = '$slug' and b.ididioma = $ididioma");

			//Se vier da tradução redireciona
			if($noticia){
				return Redirect::to('new/'.$noticia[0]->slug);
			}else{
				$noticia = DB::select("Select a.*, b.nome as cliente from tb_noticias a left join tb_clientes b
									   on a.idcliente = b.idtraducao where a.slug = '$slug'");


				$clientes = DB::select("Select a.nome, a.slug from tb_clientes a inner join tb_noticias b on b.idcliente = a.id
										where a.status = 1 and b.ididioma = 2 group by a.nome order by a.nome");

				$secao = DB::select("Select slug from tb_paginas where idsecao = 18 and ididioma = 2");

				$imagens = DB::select("Select c.link from tb_noticias a
									   left join tb_galerias_locais b on a.idtraducao = b.idnoticia
									   left join tb_imagens c on b.idgaleria = c.idgaleria
									   where a.slug = '$slug' order by c.ordem");

				$videos = DB::select("Select c.link from tb_noticias a
									   left join tb_galerias_locais b on a.idtraducao = b.idnoticia
									   left join tb_videos c on b.idgaleria = c.idgaleria
									   where a.slug = '$slug' order by c.ordem");
			}

			if($noticia[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}

		}

		if($noticia){

			$data = array(
				'noticia' 	=> $noticia,
				'clientes'	=> $clientes,
				'secao'		=> $secao,
				'imagens'	=> $imagens,
				'videos'	=> $videos
			);

			Session::put('title',$noticia[0]->titulo);

			$this->layout->content = View::make('home.noticia')->with($data);

		}else{
			return Redirect::to('/');
		}
	}

}
