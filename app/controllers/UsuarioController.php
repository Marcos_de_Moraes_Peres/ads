<?php

class UsuarioController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$usuarios = DB::table('tb_usuarios as a')
		->select(array('a.*'))
		->orderBy('a.nome')
		->paginate(15);

		$this->layout->content = View::make('list.usuario')->with('usuarios',$usuarios);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('create.usuario');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$usuario = new User();

		$usuario->nome = Input::get('titulo');
		$usuario->email = Input::get('login');
		$usuario->password = Hash::make(Input::get('password'));

		$usuario->save();

		Session::flash('success','Usuário cadastrado com sucesso!');
		return Redirect::to('adminUser');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$usuario = DB::select("Select * from tb_usuarios where id = $id");

		$this->layout->content = View::make('edit.usuario')->with('usuario',$usuario);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		if(Input::get('password')){
			$data = array(
				'nome' 		=> Input::get('titulo'),
				'email' 	=> Input::get('login'),
				'password' 	=> Hash::make(Input::get('password'))
			);
		}else{
			$data = array(
				'nome' 		=> Input::get('titulo'),
				'email' 	=> Input::get('login')
			);
		}

		User::where('id','=',$id)->update($data);

		Session::flash('success','Usuário Atualizado com sucesso');
		return Redirect::to('adminUser');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::where('id','=',$id)->delete();

		Session::flash('success','Usuário excluído com sucesso!');
		return Redirect::to('adminUser');
	}

}