<?php

class CaseController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cases = DB::table('tb_cases as a')
		->leftjoin('tb_servicos as b','a.idservico','=','b.id')
		->select(array('a.*','b.titulo as servico'))
		->where('a.ididioma','=',1)
		->orderBy('a.status','DESC')
		->get();

		$bandeiras = DB::select("Select * from tb_cases where ididioma = 2");

		$data = array(
			'bandeiras'	=> $bandeiras,
			'cases'		=> $cases
		);

		$this->layout->content = View::make('list.cases')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$servicos = DB::select("Select id, titulo from tb_servicos where status = 1 and ididioma = 1");

		$data = array(
			'servicos'	=> $servicos
		);

		$this->layout->content = View::make('create.cases')->with($data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$case = new Cases();

		$case->titulo = Input::get('titulo');
		$case->ididioma = 1;
		$case->ordem = 9999;
		$case->slug = Input::get('slug');
		$case->idservico = Input::get('servico');
		$case->usuario = Auth::user()->nome;
		$case->resumo = Input::get('resumo');
		$case->conteudo = Input::get('conteudo');
		$case->status = Input::get('status');

		$file = Input::file('arquivo');
		$destino = base_path().'/assets/img/cases/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

			$case->idfile = $idfile;

		}

		$case->save();

		$id = $case->id;

		if(Input::get('tituloEn')){
			$case = new Cases();

			$case->titulo = Input::get('tituloEn');
			$case->ididioma = 2;
			$case->ordem = 9999;
			$case->slug = Input::get('slugEn');
			$case->idservico = Input::get('servico');
			$case->resumo = Input::get('resumoEn');
			$case->conteudo = Input::get('conteudoEn');
			$case->status = Input::get('status');
			$case->idtraducao = $id;

			if($file){
				$case->idfile = $idfile;
			}

			$case->save();
		}

		Session::flash('success',"Case cadastrado com sucesso!");
		return Redirect::to('adminCase');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$case = DB::select("Select a.*, b.titulo servico from tb_cases a left join tb_servicos b on 
							a.idservico = b.id where a.id = $id");

		$caseEn = DB::select("Select * from tb_cases where idtraducao = $id");

		$idservico = $case[0]->idservico;

		$servicos = DB::select("Select id, titulo from tb_servicos where id <> $idservico");

		$data = array(
			'case'		=> $case,
			'servicos'	=> $servicos,
			'caseEn'	=> $caseEn
		);

		$this->layout->content = View::make('edit.cases')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$file = Input::file('arquivo');
		$destino = base_path().'/assets/img/cases/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

		}

		if($file){
			$data = array(
				'titulo' 	  => Input::get('titulo'),
				'idservico'	  => Input::get('servico'),
				'status' 	  => Input::get('status'),
				'conteudo' 	  => Input::get('conteudo'),
				'resumo' 	  => Input::get('resumo'),
				'usuario' 	  => Auth::user()->nome,
				'slug' 	 	  => Input::get('slug'),
				'idfile'	  => $idfile
			);
		}else{
			$data = array(
				'titulo' 	  => Input::get('titulo'),
				'idservico'	  => Input::get('servico'),
				'status' 	  => Input::get('status'),
				'slug' 		  => Input::get('slug'),
				'usuario' 	  => Auth::user()->nome,
				'resumo' 	  => Input::get('resumo'),
				'conteudo' 	  => Input::get('conteudo')
			);
		}

		Cases::where('id','=',$id)->update($data);

		if($cases = DB::select("Select * from tb_cases where id = $id")){

			if(Input::get('tituloEn')){
				$idEn = DB::select("Select * from tb_cases where idtraducao = $id and ididioma = 2");
				$nomeEn = Input::get('tituloEn');
				$conteudoEn = Input::get('conteudoEn');
				$resumoEn = Input::get('resumoEn');
				$slugEn = Input::get('slugEn');
				$ordem = $cases[0]->ordem;


				if(isset($idEn[0]->id)){
					if($file){
						DB::update("Update tb_cases set titulo = ?, conteudo = ?, idfile = ?, slug = ?, resumo = ?
									where idtraducao = $id and ididioma = 2",
									array($nomeEn,$conteudoEn,$idfile,$slugEn,$resumoEn));
					}else{
						DB::update("Update tb_cases set titulo = ?, conteudo = ?, slug = ?, resumo = ?
									where idtraducao = $id and ididioma = 2",
									array($nomeEn,$conteudoEn,$slugEn,$resumoEn));
					}
				}else{
					$case = new Cases();

					$case->titulo = Input::get('tituloEn');
					$case->ididioma = 2;
					$case->ordem = $ordem;
					$case->slug = Input::get('slugEn');
					$case->idservico = Input::get('servico');
					$case->status = Input::get('status');
					$case->conteudo = Input::get('conteudoEn');
					$case->resumo = Input::get('resumoEn');
					$case->idtraducao = $id;

					if($file){
						$case->idfile = $idfile;
					}else{
						if($idRet = DB::select("Select idfile from tb_cases where id = $id")){
							$case->idfile = $idRet[0]->idfile;
						}
					}

					$case->save();
				}

			}
		}else{
			DB::delete("delete from tb_cases where idtraducao = $id and ididioma = 2");
		}

		Session::flash('success',"Case atualizado com sucesso!");
		return Redirect::to('adminCase');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Cases::where('id','=',$id)->delete();
		Cases::where('idtraducao','=',$id)->delete();

		Session::flash('success',"Registros excluídos com sucesso");
		return Redirect::to('adminCase');
	}

	public function getOrder(){
		$cases = DB::select("Select * from tb_cases where status = 1 and ididioma = 1 order by ordem");

		$this->layout->content = View::make('order.cases')->with('cases',$cases);
	}

	public function postFilter(){
		$datas = Input::all();

		array_shift($datas);

		foreach($datas as $key => $data){
			$datafile = array(
				'ordem' => $key+1
			);

			Cases::where('id','=',$data)->update($datafile);
			Cases::where('idtraducao','=',$data)->update($datafile);
		}


		Session::flash('success',"Registros ordenados com sucesso!");
		return Redirect::to('adminCases/order');

	}


}
