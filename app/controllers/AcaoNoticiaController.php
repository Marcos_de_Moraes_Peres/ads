<?php

class AcaoNoticiaController extends \BaseController {

	protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$noticias = DB::select("Select * from tb_acao_noticias order by created_at desc");

		$this->layout->content = View::make('list.acaoNoticia')->with('noticias',$noticias);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('create.acaoNoticia');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$noticia = new AcaoNoticia();


		$noticia->titulo = Input::get('titulo');
		$noticia->slug = Input::get('slug');
		$noticia->resumo = Input::get('resumo');
		$noticia->texto = Input::get('conteudo');

		$file = Input::file('arquivo');
		$destino = base_path().'/public/assets/img/acao/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = $file->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

			$noticia->idfile = $idfile;

		}

		$noticia->save();

		Session::flash('success', "Notícia incluída com sucesso!");
		return Redirect::to('adminAcaoNoticia');

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$noticia = AcaoNoticia::find($id);

		$this->layout->content = View::make('edit.acaoNoticia')->with('noticia',$noticia);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$noticia = AcaoNoticia::find($id);

		$noticia->titulo = Input::get('titulo');
		$noticia->slug = Input::get('slug');
		$noticia->resumo = Input::get('resumo');
		$noticia->texto = Input::get('conteudo');

		$file = Input::file('arquivo');
		$destino = base_path().'/public/assets/img/acao/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = $file->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

			$noticia->idfile = $idfile;

		}

		$noticia->save();

		Session::flash('success', "Notícia atualizada com sucesso!");
		return Redirect::to('adminAcaoNoticia');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		AcaoNoticia::where('id','=',$id)->delete();

		Session::flash('success',"Notícia Excluída com sucesso!");
		return Redirect::to('adminAcaoNoticia');
	}


}
