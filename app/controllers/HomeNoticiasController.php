<?php

class HomeNoticiasController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug)
	{
		$date = date('Y-m-d');

		$ididioma = Session::get('idioma');

		if($ididioma == 1){
			$pagina = DB::select("Select a.* from tb_paginas a left join tb_paginas b on a.id = b.idtraducao
								  where b.slug = '$slug' and a.ididioma = $ididioma");


			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('noticias/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$noticias = DB::select("Select a.*,b.nome as cliente from tb_noticias a left join tb_clientes b on a.idcliente = b.id
									    where a.ididioma = 1 and a.status = 1 and a.publicacao <= '$date' and b.status = 1 
									    order by a.publicacao desc limit 20");

				$clientes = DB::select("Select a.nome, a.slug from tb_clientes a inner join tb_noticias b on b.idcliente = a.id
										where a.status = 1 and a.ididioma = 1 group by a.nome order by a.nome");
			}

		}else{

			$pagina = DB::select("Select b.* from tb_paginas a left join tb_paginas b 
								  on a.id = b.idtraducao where a.slug = '$slug' and b.ididioma = $ididioma");

			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('news/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$noticias = DB::select("Select a.*,b.nome as cliente from tb_noticias a left join tb_clientes b on a.idcliente = b.id
									    where a.ididioma = 2 and a.status = 1 and a.publicacao <= '$date' and b.status = 1
									    order by a.publicacao desc limit 20");

				$clientes = DB::select("Select a.nome, a.slug from tb_clientes a inner join tb_noticias b on b.idcliente = a.id
										where a.status = 1 and b.ididioma = 2 group by a.nome order by a.nome");
			}

			if($pagina[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}

		}

		if($pagina){

			$data = array(
				'pagina' 	=> $pagina,
				'noticias'	=> $noticias,
				'clientes'	=> $clientes
			);

			Session::put('title',$pagina[0]->titulo);

			$this->layout->content = View::make('home.noticias')->with($data);

		}else{
			return Redirect::to('/');
		}
	}


}
