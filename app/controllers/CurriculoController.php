<?php

class CurriculoController extends \BaseController {

protected $layout = 'layouts.admin.master';

	public function index(){
		$departamentos = Departamento::all();

		$this->layout->content = View::make('list.curriculo')->with('departamentos',$departamentos);
	}

	public function edit($id){
		Curriculo::where('id','=',$id)->delete();

		Session::flash('success','Currículo excluído com sucesso!');
		return Redirect::to('adminCurriculo');
	}

	public function getFilter(){
		$dateIni = Input::get('headInicio');
		$dateFim = Input::get('headFim');

		$iddepto = Input::get('depto');

		if(preg_match('#(\d{1,2})\D(\d{1,2})\D(\d{4})#',$dateIni,$match)){
			$time = mktime(0,0,0,$match[2],$match[1],$match[3]);
			$timeBegin = date('Y-m-d',$time);
		}

		if(preg_match('#(\d{1,2})\D(\d{1,2})\D(\d{4})#',$dateFim,$match)){
			$time = mktime(0,0,0,$match[2],$match[1],$match[3]);
			$timeEnd = date('Y-m-d',$time);
		}

		$dataIni = $timeBegin.' 00:00:00';
		$dataFim = $timeEnd.' 23:59:59';

		$departamentos = Departamento::all();

		if($iddepto == 0){
			$curriculos = DB::select("Select a.*, c.arquivo as file from tb_curriculos a inner join tb_files c on a.idfile = c.id
								      where a.created_at between '$dataIni' and '$dataFim'");
		}else{
			$curriculos = DB::select("Select a.*, c.arquivo as file from tb_curriculos a inner join tb_files c on a.idfile = c.id
								      where a.created_at between '$dataIni' and '$dataFim' and a.iddepto = $iddepto");
		}

		$data = array(
			'departamentos'	=> $departamentos,
			'curriculos'	=> $curriculos,
			'dataInicio'	=> $dateIni,
			'dataFim'		=> $dateFim,
			'iddepto'		=> $iddepto
		);

		$this->layout->content = View::make('list.curriculo')->with($data);
	}

}