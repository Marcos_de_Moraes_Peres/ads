<?php

class AcaoBolhetimController extends \BaseController {


	protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$bolhetim = DB::select("Select * from tb_bolhetim order by created_at desc");

		$data = array(
			'bolhetim'	=> $bolhetim
		);

		$this->layout->content = View::make('list.bolhetim')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$noticias = DB::select("Select id, titulo from tb_acao_noticias order by created_at desc");

		$base = '2014';

		$data = array(
			'base'		=> $base,
			'noticias'	=> $noticias
		);

		$this->layout->content = View::make('create.bolhetim')->with($data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$noticia = new Bolhetim();

		$noticia->titulo = Input::get('titulo');
		$noticia->slug = Input::get('slug');
		$noticia->mes = Input::get('mes');
		$noticia->ano = Input::get('ano');
		$noticia->status = Input::get('status');
		$noticia->idnoticia1 = Input::get('noticia1');
		$noticia->idnoticia2 = Input::get('noticia2');
		$noticia->idnoticia3 = Input::get('noticia3');
		$noticia->idnoticia4 = Input::get('noticia4');

		$file = Input::file('arquivo');
		$destino = base_path().'/public/assets/img/acao/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = $file->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

			$noticia->idfile = $idfile;

		}

		$noticia->save();

		Session::flash('success', "Bolhetim incluído com sucesso!");
		return Redirect::to('adminAcaoBolhetim');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$new = Bolhetim::find($id);

		$noticias = DB::select("Select id, titulo from tb_acao_noticias order by created_at desc");

		$base = '2014';

		$data = array(
			'noticias'	=> $noticias,
			'new'		=> $new,
			'base'		=> $base
		);

		$this->layout->content = View::make('edit.bolhetim')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$noticia = Bolhetim::find($id);

		$noticia->titulo = Input::get('titulo');
		$noticia->slug = Input::get('slug');
		$noticia->mes = Input::get('mes');
		$noticia->ano = Input::get('ano');
		$noticia->status = Input::get('status');
		$noticia->idnoticia1 = Input::get('noticia1');
		$noticia->idnoticia2 = Input::get('noticia2');
		$noticia->idnoticia3 = Input::get('noticia3');
		$noticia->idnoticia4 = Input::get('noticia4');

		$file = Input::file('arquivo');
		$destino = base_path().'/public/assets/img/acao/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = $file->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

			$noticia->idfile = $idfile;

		}

		$noticia->save();

		Session::flash('success', "Bolhetim incluído com sucesso!");
		return Redirect::to('adminAcaoBolhetim');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Bolhetim::where('id','=',$id)->delete();

		Session::flash('success',"Bolhetim excluído com sucesso!");
		return Redirect::to('adminAcaoBolhetim');
	}


}
