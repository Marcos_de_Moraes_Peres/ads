<?php

class HomeServicoController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug)
	{
		$ididioma = Session::get('idioma');

		if($ididioma == 1){
			$servico = DB::select("Select a.* from tb_servicos a left join tb_servicos b on a.id = b.idtraducao
								  where b.slug = '$slug' and a.ididioma = $ididioma");


			//Se vier da tradução redireciona
			if($servico){
				return Redirect::to('servico/'.$servico[0]->slug);
			}else{
				$servico = DB::select("Select a.*, b.arquivo from tb_servicos a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$idservico = $servico[0]->id;

				$cases = DB::select("Select a.slug, b.arquivo from tb_cases a left join tb_files b on 
									 a.idfile = b.id where ididioma = 1 and status = 1 and idservico = $idservico
									 order by a.ordem");

				$secao = DB::select("Select slug from tb_paginas where idsecao = 14
									 and ididioma = 1");
				
			}

		}else{

			$servico = DB::select("Select b.* from tb_servicos a left join tb_servicos b 
								  on a.id = b.idtraducao where a.slug = '$slug' and b.ididioma = $ididioma");

			//Se vier da tradução redireciona
			if($servico){
				return Redirect::to('service/'.$servico[0]->slug);
			}else{
				$servico = DB::select("Select a.*, b.arquivo from tb_servicos a left join tb_files b on
									   a.idfile = b.id where a.slug = '$slug'");

				$idservico = $servico[0]->idtraducao;

				$cases = DB::select("Select a.slug, b.arquivo from tb_cases a left join tb_files b on 
									 a.idfile = b.id where ididioma = 2 and status = 1 and a.idservico = $idservico
									 order by a.ordem");

				$secao = DB::select("Select slug from tb_paginas where idsecao = 14
									 and ididioma = 2");

			}

			if($servico[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}

		}

		if($servico){

			$data = array(
				'servico'	=> $servico,
				'cases'		=> $cases,
				'secao'		=> $secao
			);

			Session::put('title',$servico[0]->titulo);

			$this->layout->content = View::make('home.servico')->with($data);

		}else{
			return Redirect::to('/');
		}
	}

}
