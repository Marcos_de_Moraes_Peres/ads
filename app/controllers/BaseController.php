<?php

class BaseController extends Controller {

	protected function setupLayout()
	{	

		if(!Session::get('idioma')){
			Session::put('idioma',1);
			Session::put('campo','id');
			Session::put('local','local_pt');
		}

		$idioma = Session::get('idioma');
		$campo 	= Session::get('campo');

		// Criação do Menu //
		$secoes = DB::select("Select $campo as id, titulo ,link from tb_secoes where status = 1 
							  and ididioma = $idioma order by ordem");

		$menuObject = $this->objectMenu($secoes);

		$this->layout = View::make($this->layout)->with('menuObject',$menuObject);
	}

	protected function objectMenu($secoes){

		foreach($secoes as &$secao){

			$idsecao = $secao->id;
			$idioma = Session::get('idioma');
			$campo 	= Session::get('campo');
			$local 	= Session::get('local');

			$categorias = DB::select("Select $campo as id, titulo, link from tb_categorias where status = 1 
									  and ididioma = $idioma and idsecao = $idsecao order by ordem");

			foreach($categorias as &$categoria){
				$idcategoria = $categoria->id;

				$paginas = DB::select("Select a.slug, b.$local as local 
									   from tb_paginas a left join tb_templates b on 
									   a.idtemplate = b.id where a.idcategoria = $idcategoria and a.ididioma = $idioma");

				if($paginas){
					$categoria->linkInt = $paginas[0]->local."/".$paginas[0]->slug;
				}

				$servicos = DB::select("Select a.slug, b.$local as local 
									   from tb_servicos a left join tb_templates b on 
									   a.idtemplate = b.id where a.idcategoria = $idcategoria and a.ididioma = $idioma");

				if($servicos){
					$categoria->linkInt = $servicos[0]->local."/".$servicos[0]->slug;
				}

			}

			$secao->filhos = $categorias;

			$secaoPage = DB::select("Select a.slug, b.$local as local
									 from tb_paginas a left join tb_templates b on a.idtemplate = b.id 
									 where a.idsecao = $idsecao and a.ididioma = $idioma");


			if($secaoPage){
				$secao->linkInt = $secaoPage[0]->local."/".$secaoPage[0]->slug;
			}
		}
		return $secoes;
	}
}
