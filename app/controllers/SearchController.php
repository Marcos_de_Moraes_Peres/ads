<?php

class SearchController extends \BaseController {

protected $layout = 'layouts.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = array(
			'paginas'	=> '',
			'servicos'	=> '',
			'cases'		=> '',
			'clientes'	=> '',
			'noticias'	=> ''
		);

		$this->layout->content = View::make('home.search')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$ididioma = Session::get('idioma');
		$busca = Input::get('busca');

		if($ididioma == 1){

			$paginas = DB::select("Select a.titulo, a.resumo, a.slug, a.updated_at, b.local_pt as local
								   from tb_paginas a left join tb_templates b on a.idtemplate = b.id
								   where a.ididioma = 1 and (a.titulo like '%$busca%' or a.resumo like '%$busca%')");

			$servicos = DB::select("Select a.titulo, a.slug, a.updated_at, b.local_pt as local
								    from tb_servicos a left join tb_templates b on a.idtemplate = b.id
								    where a.ididioma = 1 and a.titulo like '%$busca%'");

			$cases = DB::select("Select a.titulo, a.resumo, a.slug, a.updated_at from tb_cases a
								 where a.ididioma = 1 and (a.titulo like '%$busca%' or a.resumo like '%$busca%')");

			$clientes = DB::select("Select a.nome, a.slug, a.updated_at from tb_clientes a
								    where a.ididioma = 1 and a.nome like '%$busca%'");

			$noticias = DB::select("Select a.titulo, a.resumo, a.slug, a.updated_at, b.nome as cliente
									from tb_noticias a left join tb_clientes b on a.idcliente = b.id
									where a.ididioma = 1 and (a.titulo like '%$busca%' 
									or a.resumo like '%$busca%')");


			Session::put('title',"Busca");

		}else{

			$paginas = DB::select("Select a.titulo, a.resumo, a.slug, a.updated_at, b.local_en as local
								   from tb_paginas a left join tb_templates b on a.idtemplate = b.id
								   where a.ididioma = 2 and (a.titulo like '%$busca%' or a.resumo like '%$busca%')");

			$servicos = DB::select("Select a.titulo, a.slug, a.updated_at, b.local_en as local
								    from tb_servicos a left join tb_templates b on a.idtemplate = b.id
								    where a.ididioma = 2 and a.titulo like '%$busca%'");

			$cases = DB::select("Select a.titulo, a.resumo, a.slug, a.updated_at from tb_cases a
								 where a.ididioma = 2 and (a.titulo like '%$busca%' or a.resumo like '%$busca%')");

			$clientes = DB::select("Select a.nome, a.slug, a.updated_at from tb_clientes a
								    where a.ididioma = 2 and a.nome like '%$busca%'");

			$noticias = DB::select("Select a.titulo, a.resumo, a.slug, a.updated_at, b.nome as cliente
									from tb_noticias a left join tb_clientes b on a.idcliente = b.idtraducao
									where a.ididioma = 2 and (a.titulo like '%$busca%' 
									or a.resumo like '%$busca%')");

			Session::put('title',"Search");
		}

		$data = array(
			'paginas'	=> $paginas,
			'servicos'	=> $servicos,
			'cases'		=> $cases,
			'clientes'	=> $clientes,
			'noticias'	=> $noticias
		);

		$this->layout->content = View::make('home.search')->with($data);

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
