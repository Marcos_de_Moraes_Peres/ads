<?php

class ImagemController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$galeria = DB::select("Select * from tb_galerias where id = $id");

		$imagens = DB::select("Select * from tb_imagens where idgaleria = $id order by ordem");

		$data = array(
			'galeria' => $galeria,
			'imagens' => $imagens
		);

		$this->layout->content = View::make('edit.imagem')->with($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$arquivo = Input::file('ziparchive');
		$file = Input::file('arquivo');

		$path = base_path().'/assets/img/temp/';
		$destino = base_path().'/assets/img/galeria/';

		if($arquivo){

			// Descompactando o arquivo dentro do diretório path

			$zip = new ZipArchive();
			$x = $zip->open($arquivo); // open the zip file to extract
			if ($x === true) {
				$zip->extractTo($path); // place in the directory with same name
				$zip->close();
			}

			$diretorio = dir($path);

			while($arquivo = $diretorio->read()){
				//para retirar os arquivos ocultos
				if($arquivo <> '.' && $arquivo <> '..'){
					$imagem = new Imagem();

					$nome = str_random(5).$arquivo;

					$imagem->idgaleria = $id;
					$imagem->link = $nome;
					$imagem->ordem = 9999;

					$imagem->save();

					copy($path.$arquivo,$destino.$nome);
					unlink($path.$arquivo);
				}
			}

			$diretorio->close();
		}


		if($file){

			$datafiles = new Imagem();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->link = $filename;
			$datafiles->idgaleria = $id;
			$datafiles->ordem = 9999;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

		}

		Session::flash('success','Registros atualizados com sucesso');
		return Redirect::to("adminImages/$id/edit");

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function postOrder(){
		$datas = Input::all();

		array_shift($datas);

		foreach($datas as $key => $data){
			$datafile = array(
				'ordem' => $key+1
			);

			$idimagem = $data;

			Imagem::where('id','=',$data)->update($datafile);
		}

		$imagem = DB::select("Select * from tb_imagens where id = $idimagem");

		$idgaleria = $imagem[0]->idgaleria;

		Session::flash('success',"Registros ordenados com sucesso!");
		return Redirect::to("adminImages/$idgaleria/edit");
	}

	public function getExcluir($id){
		$destino = base_path().'/assets/img/galeria/';

		$imagem = DB::select("Select * from tb_imagens where id = $id");

		$nome = $imagem[0]->link;
		$idgaleria = $imagem[0]->idgaleria;

		//Excluindo a foto da pasta de origem
		unlink($destino.$nome);
		Imagem::where('id','=',$id)->delete();

		Session::flash('success','Registro excluído com sucesso"');
		return Redirect::to("adminImages/$idgaleria/edit");
	}

	public function getAll($id){
		$destino = base_path().'/assets/img/galeria/';

		$imagens = DB::select("Select * from tb_imagens where idgaleria = $id");

		foreach($imagens as $imagem){
			$nome = $imagem->link;

			//Excluindo a foto da pasta de origem
			unlink($destino.$nome);
		}

		Imagem::where('idgaleria','=',$id)->delete();

		Session::flash('success','Registros excluídos com sucesso"');
		return Redirect::to("adminImages/$id/edit");
	}

}