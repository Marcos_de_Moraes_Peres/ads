<?php

class HomeNoticiaClienteController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug)
	{
		$date = date('Y-m-d');
		$ididioma = Session::get('idioma');

		if($ididioma == 1){

			$verify = DB::select("Select a.* from tb_clientes a left join tb_clientes b on a.id = b.idtraducao
								  where b.slug = '$slug' and a.ididioma = $ididioma and a.slug <> b.slug");

			if($verify){
				$cliente = DB::select("Select a.* from tb_clientes a left join tb_clientes b on a.id = b.idtraducao
								  	   where b.slug = '$slug' and a.ididioma = $ididioma");
			}else{
				$cliente = null;
			}


			//Se vier da tradução redireciona
			if($cliente){
				return Redirect::to('noticias-cliente/'.$cliente[0]->slug);
			}else{
				$cliente = DB::select("Select a.*, b.arquivo from tb_clientes a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$id = $cliente[0]->id;

				$noticias = DB::table('tb_noticias as a')
				->leftjoin('tb_clientes as b','a.idcliente','=','b.id')
				->select(array('a.*','b.nome as cliente'))
				->where('a.ididioma','=',1)
				->where('a.status','=',1)
				->where('a.idcliente','=',$id)
				->where('a.publicacao','<=',$date)
				->orderBy('a.publicacao','DESC')
				->paginate(10);

				$clientes = DB::select("Select a.nome, a.slug from tb_clientes a inner join tb_noticias b on b.idcliente = a.id
										where a.status = 1 and b.ididioma = 1 group by a.nome order by a.nome");

				$secao = DB::select("Select slug from tb_paginas where idsecao = 18 and ididioma = 1");
			}

			$id = $cliente[0]->id;
		}else{

			$verify = DB::select("Select b.*, c.arquivo from tb_clientes a left join tb_clientes b 
								  on a.id = b.idtraducao left join tb_files c on b.idfile = c.id
								  where a.slug = '$slug' and b.ididioma = $ididioma and a.slug <> b.slug");

			if($verify){
				$cliente = DB::select("Select b.*, c.arquivo from tb_clientes a left join tb_clientes b 
									  on a.id = b.idtraducao left join tb_files c on b.idfile = c.id
									  where a.slug = '$slug' and b.ididioma = $ididioma");
			}else{
				$cliente = null;
			}

			//Se vier da tradução redireciona
			if($cliente){
				return Redirect::to('news-client/'.$cliente[0]->slug);
			}else{
				$cliente = DB::select("Select a.*, b.arquivo from tb_clientes a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$id = $cliente[0]->idtraducao;

				$noticias = DB::table('tb_noticias as a')
				->leftjoin('tb_clientes as b','a.idcliente','=','b.id')
				->select(array('a.*','b.nome as cliente'))
				->where('a.ididioma','=',2)
				->where('a.status','=',1)
				->where('a.idcliente','=',$id)
				->where('a.publicacao','<=',$date)
				->orderBy('a.publicacao','DESC')
				->paginate(10);

				$clientes = DB::select("Select a.nome, a.slug from tb_clientes a inner join tb_noticias b on b.idcliente = a.id
										where a.status = 1 and a.ididioma = 2 group by a.nome order by a.nome");

				$secao = DB::select("Select slug from tb_paginas where idsecao = 18 and ididioma = 2");
			}

			if(!$cliente){
				return Redirect::to('/');
			}

			if($cliente[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}

			$id = $cliente[0]->idtraducao;
		}

		if($cliente){

			$data = array(
				'cliente' 	=> $cliente,
				'noticias'	=> $noticias,
				'clientes'	=> $clientes,
				'secao'		=> $secao
			);

			Session::put('title',$cliente[0]->nome);

			$this->layout->content = View::make('home.noticiacliente')->with($data);

		}else{
			return Redirect::to('/');
		}
	}


}
