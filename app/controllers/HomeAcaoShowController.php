<?php

class HomeAcaoShowController extends \BaseController {

	protected $layout = 'layouts.acao';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($slug)
	{

		$boletim = Bolhetim::where('slug','=',$slug)->get();

		$noticia1 = DB::select("Select b.*, c.arquivo from tb_bolhetim a left join tb_acao_noticias b on a.idnoticia1 = b.id
								left join tb_files c on b.idfile = c.id where a.slug = '$slug'");

		$noticia2 = DB::select("Select b.*, c.arquivo from tb_bolhetim a left join tb_acao_noticias b on a.idnoticia2 = b.id
								left join tb_files c on b.idfile = c.id where a.slug = '$slug'");

		$noticia3 = DB::select("Select b.*, c.arquivo from tb_bolhetim a left join tb_acao_noticias b on a.idnoticia3 = b.id
								left join tb_files c on b.idfile = c.id where a.slug = '$slug'");

		$noticia4 = DB::select("Select b.*, c.arquivo from tb_bolhetim a left join tb_acao_noticias b on a.idnoticia4 = b.id
								left join tb_files c on b.idfile = c.id where a.slug = '$slug'");

		$data = array(
			'noticia1'	=> $noticia1,
			'noticia2'	=> $noticia2,
			'noticia3'	=> $noticia3,
			'noticia4'	=> $noticia4,
			'boletim'	=> $boletim
		);

		$this->layout->content = View::make('home.acao')->with($data);
	}

	public function show($slug)
	{
		$boletim1 = DB::select("Select b.* from tb_acao_noticias a left join tb_bolhetim b on a.id = b.idnoticia1
								where a.slug = '$slug' ");

		$boletim2 = DB::select("Select b.* from tb_acao_noticias a left join tb_bolhetim b on a.id = b.idnoticia2
								where a.slug = '$slug' ");

		$boletim3 = DB::select("Select b.* from tb_acao_noticias a left join tb_bolhetim b on a.id = b.idnoticia3
								where a.slug = '$slug' ");

		$boletim4 = DB::select("Select b.* from tb_acao_noticias a left join tb_bolhetim b on a.id = b.idnoticia4
								where a.slug = '$slug' ");

		if(isset($boletim1[0]->id)){
			$boletim = $boletim1;
		}elseif(isset($boletim2[0]->id)){
			$boletim = $boletim2;
		}elseif(isset($boletim3[0]->id)){
			$boletim = $boletim3;
		}else{
			$boletim = $boletim4;
		}

		$new = DB::select("	Select a.*, b.arquivo from tb_acao_noticias a left join tb_files b on a.idfile = b.id
							where a.slug = '$slug'");

		$data = array(
			'boletim'	=> $boletim,
			'new'		=> $new
		);

		$this->layout->content = View::make('home.showinfo')->with($data);

	}

}
