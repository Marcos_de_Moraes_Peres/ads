<?php

class VideoController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$galeria = DB::select("Select * from tb_galerias where id = $id");

		$videos = DB::select("Select * from tb_videos where idgaleria = $id order by ordem");

		$data = array(
			'galeria' => $galeria,
			'videos'	  => $videos
		);

		$this->layout->content = View::make('edit.video')->with($data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$youtube = "youtube";
		$vimeo = "vimeo";
		
		$url = Input::get('url');

		//Função para saber se existe a string dentro da url
		if(stripos($url, $youtube) !== false){

			//Trocando URL por Embed
			$nameYoutube = str_replace("watch?v=","embed/",$url);

			$video = new Video();

			$video->idgaleria = $id;
			$video->link = $nameYoutube;
			$video->ordem = 9999;

			$video->save();

			Session::flash('success',"Video do Youtube cadastrado com sucesso!");
			return Redirect::to("videos/$id/edit");

		}elseif(stripos($url, $vimeo) !== false){
			
			$part = explode('/', $url);
			$partVimeo = end($part);
			$nameVimeo = '//player.vimeo.com/video/'.$partVimeo;

			$video = new Video();

			$video->idgaleria = $id;
			$video->link = $nameVimeo;
			$video->ordem = 9999;

			$video->save();

			Session::flash('success',"Video do Vimeo cadastrado com sucesso!");
			return Redirect::to("adminVideos/$id/edit");

		}else{

			Session::flash('danger',"Insira apenas videos do Youtube ou Vimeo!");
			return Redirect::to("adminVideos/$id/edit");

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function postOrder(){
		$datas = Input::all();

		array_shift($datas);

		foreach($datas as $key => $data){
			$datafile = array(
				'ordem' => $key+1
			);

			$idvideo = $data;

			Video::where('id','=',$data)->update($datafile);
		}

		$video = DB::select("Select * from tb_videos where id = $idvideo");

		$idgaleria = $video[0]->idgaleria;

		Session::flash('success',"Videos ordenados com sucesso!");
		return Redirect::to("adminVideos/$idgaleria/edit");
	}

	public function getExcluir($id){
		
		$video = DB::select("Select * from tb_videos where id = $id");

		$idgaleria = $video[0]->idgaleria;

		Video::where('id','=',$id)->delete();

		Session::flash('success','Video excluído com sucesso"');
		return Redirect::to("adminVideos/$idgaleria/edit");
	}

	public function getAll($id){
		Video::where('idgaleria','=',$id)->delete();

		Session::flash('success','Videos excluídos com sucesso"');
		return Redirect::to("adminVideos/$id/edit");
	}

}