<?php

class DepoimentoController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$depoimentos = DB::table('tb_depoimentos as a')
		->select(array('a.*'))
		->where('a.ididioma','=',1)
		->orderBy('a.status','DESC')
		->get();

		$bandeiras = DB::select("select * from tb_depoimentos where ididioma = 2");

		$data = array(
			'depoimentos' 	=> $depoimentos,
			'bandeiras'		=> $bandeiras
		);

		$this->layout->content = View::make('list.depoimentos')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$clientes = DB::select("Select * from tb_clientes where status = 1 and ididioma = 1");

		$this->layout->content = View::make('create.depoimentos')->with('clientes',$clientes);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$depoimento = new Depoimento();

		$depoimento->conteudo = Input::get('conteudo');
		$depoimento->nome = Input::get('nome');
		$depoimento->idcliente = Input::get('idcliente');
		$depoimento->status = Input::get('status');
		$depoimento->ordem = 9999;
		$depoimento->usuario = Auth::user()->nome;
		$depoimento->ididioma = 1;

		$file = Input::file('arquivo');
		$destino = base_path().'/assets/img/depoimentos/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

			$depoimento->idfile = $idfile;

		}

		$depoimento->save();

		$id = $depoimento->id;

		if(Input::get('conteudoEn')){
			$depoimento = new Depoimento();

			$depoimento->conteudo = Input::get('conteudoEn');
			$depoimento->nome = Input::get('nome');
			$depoimento->idcliente = Input::get('idcliente');
			$depoimento->status = Input::get('status');
			$depoimento->ordem = 9999;
			$depoimento->ididioma = 2;
			$depoimento->idtraducao = $id;

			if($file){
				$depoimento->idfile = $idfile;
			}

			$depoimento->save();
		}

		Session::flash('success',"Depoimento Cadastrado com sucesso!");
		return Redirect::to('adminDepoimento');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$depoimento = DB::select("select a.*,b.nome as cliente from tb_depoimentos a left join tb_clientes b
								  on a.idcliente = b.id where a.id = $id");

		$depoimentoEn = DB::select("select * from tb_depoimentos where idtraducao = $id");

		$idcliente = $depoimento[0]->idcliente;

		$clientes = DB::select("select * from tb_clientes where ididioma = 1 and status = 1 
								and id <> $idcliente");

		$data = array(
			'depoimento'	=> $depoimento,
			'depoimentoEn'	=> $depoimentoEn,
			'clientes'		=> $clientes
		);

		$this->layout->content = View::make('edit.depoimentos')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$file = Input::file('arquivo');
		$destino = base_path().'/assets/img/depoimentos/';
		$idfile = 0;

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

		}

		if($idfile){
			$data = array(
				'conteudo'	=> Input::get('conteudo'),
				'nome'	   	=> Input::get('nome'),
				'idcliente'	=> Input::get('idcliente'),
				'status'   	=> Input::get('status'),
				'usuario' => Auth::user()->nome,
				'idfile'	=> $idfile
			);
		}else{
			$data = array(
				'conteudo'	=> Input::get('conteudo'),
				'nome'	   	=> Input::get('nome'),
				'idcliente'	=> Input::get('idcliente'),
				'usuario' => Auth::user()->nome,
				'status'   	=> Input::get('status')
			);
		}

		Depoimento::where('id','=',$id)->update($data);

		if($secao = DB::select("Select * from tb_depoimentos where id = $id")){

			if(Input::get('conteudoEn')){
				$idEn = DB::select("Select id from tb_depoimentos where idtraducao = $id");
				$ordem = $secao[0]->ordem;
				$depFile = $secao[0]->idfile;

				if(isset($idEn[0]->id)){

					if($depFile){
						$data = array(
							'conteudo'	=> Input::get('conteudoEn'),
							'nome'	   	=> Input::get('nome'),
							'idcliente'	=> Input::get('idcliente'),
							'status'   	=> Input::get('status'),
							'idfile'	=> $depFile
						);
					}else{
						$data = array(
							'conteudo'	=> Input::get('conteudoEn'),
							'nome'	   	=> Input::get('nome'),
							'idcliente'	=> Input::get('idcliente'),
							'status'   	=> Input::get('status')
						);
					}

					Depoimento::where('idtraducao','=',$id)->update($data);

				}else{
					$depoimento = new Depoimento();

					$depoimento->conteudo = Input::get('conteudo');
					$depoimento->status = Input::get('status');
					$depoimento->nome = Input::get('nome');
					$depoimento->idcliente = Input::get('idcliente');
					$depoimento->ididioma = 2;
					$depoimento->idtraducao = $id;
					$depoimento->ordem = $ordem;

					if($depFile){
						$depoimento->idfile = $depFile;
					}

					$depoimento->save();
				}
			}else{
				DB::delete("delete from tb_depoimentos where idtraducao = $id");
			}
		}

		Session::flash('success','Depoimento atualizado com sucesso!');
		return Redirect::to('adminDepoimento');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Depoimento::where('id','=',$id)->delete();
		Depoimento::where('idtraducao','=',$id)->delete();

		Session::flash('success','Registro excluído com sucesso!');
		return Redirect::to('adminDepoimento');
	}

	public function getOrder(){
		$depoimentos = DB::select("Select * from tb_depoimentos where status = 1 and ididioma = 1 order by ordem");

		$this->layout->content = View::make('order.depoimentos')->with('depoimentos',$depoimentos);
	}

	public function postFilter(){
		$datas = Input::all();

		array_shift($datas);

		foreach($datas as $key => $data){
			$datafile = array(
				'ordem' => $key+1
			);

			Depoimento::where('id','=',$data)->update($datafile);
			Depoimento::where('idtraducao','=',$data)->update($datafile);
		}


		Session::flash('success',"Registros ordenados com sucesso!");
		return Redirect::to('adminDepoimentos/order');

	}

}