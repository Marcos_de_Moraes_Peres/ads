<?php

class PaginaController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$paginas = DB::table('tb_paginas as a')
		->leftjoin('tb_secoes as b','a.idsecao','=','b.id')
		->leftjoin('tb_categorias as c','a.idcategoria','=','c.id')
		->select(array('a.*','b.titulo as secao','c.titulo as categoria'))
		->where('a.ididioma','=',1)
		->orderBy('a.status','DESC')
		->paginate(15);

		$bandeiras = DB::select("select * from tb_paginas where ididioma = 2");

		$data = array(
			'paginas'   => $paginas,
			'bandeiras' => $bandeiras
		);

		$this->layout->content = View::make('list.pagina')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$templates = Template::all();
		$categorias = DB::select("Select a.* from tb_categorias a left join tb_paginas b on a.id = b.idcategoria
								  left join tb_servicos c on a.id = c.idcategoria
								  where a.ididioma = 1 and a.status = 1 and b.id is null and c.id is null");
		$secoes = DB::select("Select a.* from tb_secoes a left join tb_paginas b on a.id = b.idsecao
							  where a.ididioma = 1 and a.status = 1 and b.id is null");

		$data = array(
			'templates'  => $templates,
			'categorias' => $categorias,
			'secoes' 	 => $secoes
		);

		$this->layout->content = View::make('create.pagina')->with($data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$pagina = new Pagina();

		$pagina->titulo = Input::get('titulo');
		$pagina->ididioma = 1;
		$pagina->idtemplate = Input::get('template');
		$pagina->slug = Input::get('slug');
		$pagina->idcategoria = Input::get('categoria');
		$pagina->idsecao = Input::get('secao');
		$pagina->resumo = Input::get('resumo');
		$pagina->usuario = Auth::user()->nome;
		$pagina->conteudo = Input::get('conteudo');

		$file = Input::file('arquivo');
		$destino = base_path().'/assets/img/paginas/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

			$pagina->idfile = $idfile;

		}

		$pagina->save();

		$id = $pagina->id;

		if(Input::get('tituloEn')){
			$pagina = new Pagina();

			$pagina->titulo = Input::get('tituloEn');
			$pagina->ididioma = 2;
			$pagina->idtemplate = Input::get('template');
			$pagina->slug = Input::get('slugEn');
			$pagina->idcategoria = Input::get('categoria');
			$pagina->idsecao = Input::get('secao');
			$pagina->resumo = Input::get('resumoEn');
			$pagina->conteudo = Input::get('conteudoEn');
			$pagina->idtraducao = $id;

			if($file){
				$pagina->idfile = $idfile;
			}

			$pagina->save();
		}

		Session::flash('success',"Página cadastrada com sucesso!");
		return Redirect::to('pages');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$pagina = DB::select("Select a.*, b.titulo as categoria, c.titulo as secao, d.titulo as template
							  from tb_paginas a left join tb_categorias b on a.idcategoria = b.id
							  left join tb_secoes c on a.idsecao = c.id 
							  left join tb_templates d on a.idtemplate = d.id
							  where a.id = $id");

		$idcategoria = $pagina[0]->idcategoria;
		$idsecao = $pagina[0]->idsecao;
		$idtemplate = $pagina[0]->idtemplate;

		if($idcategoria){

			$categorias = DB::select("Select a.* from tb_categorias a left join tb_paginas b on a.id = b.idcategoria
									  left join tb_servicos c on a.id = c.idcategoria
								  	  where a.ididioma = 1 and a.id <> $idcategoria and a.status = 1 and b.id is null
								  	  and c.id is null");

		}else{
			$categorias = DB::select("Select a.* from tb_categorias a left join tb_paginas b on a.id = b.idcategoria
									  left join tb_servicos c on a.id = c.idcategoria
								  	  where a.ididioma = 1 and a.status = 1 and b.id is null and c.id is null");
		}

		if($idsecao){
			$secoes = DB::select("Select a.* from tb_secoes a left join tb_paginas b on a.id = b.idsecao
							  	  where a.ididioma = 1 and a.id <> $idsecao and a.status = 1 and b.id is null");
		}else{
			$secoes = DB::select("Select a.* from tb_secoes a left join tb_paginas b on a.id = b.idsecao
							  	  where a.ididioma = 1 and a.status = 1 and b.id is null");
		}

		$templates = DB::select("Select * from tb_templates where id <> $idtemplate");
		

		$paginaEn = DB::select("Select * from tb_paginas where idtraducao = $id and ididioma = 2");

		$data = array(
			'pagina' 	 => $pagina,
			'categorias' => $categorias,
			'secoes' 	 => $secoes,
			'templates'	 => $templates,
			'paginaEn'   => $paginaEn
		);

		$this->layout->content = View::make('edit.pagina')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$file = Input::file('arquivo');
		$destino = base_path().'/assets/img/paginas/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

		}

		if($file){
			$data = array(
				'titulo' 	  => Input::get('titulo'),
				'ididioma' 	  => 1,
				'idtemplate'  => Input::get('template'),
				'idcategoria' => Input::get('categoria'),
				'idsecao' 	  => Input::get('secao'),
				'resumo' 	  => Input::get('resumo'),
				'conteudo' 	  => Input::get('conteudo'),
				'usuario'	  => Auth::user()->nome,
				'slug' 	 	  => Input::get('slug'),
				'idfile'	  => $idfile
			);
		}else{
			$data = array(
				'titulo' 	  => Input::get('titulo'),
				'ididioma' 	  => 1,
				'idtemplate'  => Input::get('template'),
				'idcategoria' => Input::get('categoria'),
				'idsecao' 	  => Input::get('secao'),
				'slug' 		  => Input::get('slug'),
				'usuario' 	  => Auth::user()->nome,
				'resumo' 	  => Input::get('resumo'),
				'conteudo' 	  => Input::get('conteudo')
			);
		}

		Pagina::where('id','=',$id)->update($data);

		if(Input::get('tituloEn')){
			$idEn = DB::select("Select * from tb_paginas where idtraducao = $id and ididioma = 2");
			$nomeEn = Input::get('tituloEn');
			$resumoEn = Input::get('resumoEn');
			$conteudoEn = Input::get('conteudoEn');
			$slugEn = Input::get('slugEn');
			$idtemplateEn = Input::get('template');

			if(isset($idEn[0]->id)){
				if($file){
					DB::update("Update tb_paginas set titulo = ?, resumo = ?, conteudo = ?, idfile = ?, slug = ?, idtemplate = ?
								where idtraducao = $id and ididioma = 2",
								array($nomeEn,$resumoEn,$conteudoEn,$idfile,$slugEn,$idtemplateEn));
				}else{
					DB::update("Update tb_paginas set titulo = ?, resumo = ?, conteudo = ?, slug = ?, idtemplate = ?
								where idtraducao = $id and ididioma = 2",
								array($nomeEn,$resumoEn,$conteudoEn,$slugEn,$idtemplateEn));
				}
			}else{
				$pagina = new Pagina();

				$pagina->titulo = Input::get('tituloEn');
				$pagina->ididioma = 2;
				$pagina->idtemplate = Input::get('template');
				$pagina->idcategoria = Input::get('categoria');
				$pagina->idsecao = Input::get('secao');
				$pagina->resumo = Input::get('resumoEn');
				$pagina->slug = Input::get('slugEn');
				$pagina->conteudo = Input::get('conteudoEn');
				$pagina->idtraducao = $id;

				if($file){
					$pagina->idfile = $idfile;
				}else{
					if($idRet = DB::select("Select idfile from tb_paginas where id = $id")){
						$pagina->idfile = $idRet[0]->idfile;
					}
				}

				$pagina->save();
			}
		}else{
			DB::delete("delete from tb_paginas where idtraducao = $id and ididioma = 2");
		}

		Session::flash('success',"Página atualizada com sucesso!");
		return Redirect::to('pages');

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Pagina::where('id','=',$id)->delete();
		Pagina::where('idtraducao','=',$id)->delete();

		Session::flash('success','Página excluída com sucesso!');
		return Redirect::to('pages');
	}


}
