<?php

class ContatoController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('list.contato');
	}

	public function getExcel(){

		$dateIni = Input::get('inicio');
		$dateFim = Input::get('fim');

		if(preg_match('#(\d{1,2})\D(\d{1,2})\D(\d{4})#',$dateIni,$match)){
			$time = mktime(0,0,0,$match[2],$match[1],$match[3]);
			$timeBegin = date('Y-m-d',$time);
		}

		if(preg_match('#(\d{1,2})\D(\d{1,2})\D(\d{4})#',$dateFim,$match)){
			$time = mktime(0,0,0,$match[2],$match[1],$match[3]);
			$timeEnd = date('Y-m-d',$time);
		}

		$dataIni = $timeBegin.' 00:00:00';
		$dataFim = $timeEnd.' 23:59:59';

		$contatos = DB::select("Select * from tb_contatos where created_at between '$dataIni' 
								and '$dataFim'");

		$data[0] = array('Nome','Empresa','Telefone','Email','Data');

		foreach($contatos as $key => $contato){
			$data[$key+1] = array("$contato->nome","$contato->empresa","$contato->telefone","$contato->email",date('d/m/Y',strtotime("$contato->created_at")));
		}

		Excel::create('Filename', function($excel) use ($data) {
		    $excel->sheet('Sheetname', function($sheet) use ($data) {

		        $sheet->fromArray($data, null, 'A1', false, false);

		    });
		})->download('xls');

	}

	public function edit($id){
		Contato::where('id','=',$id)->delete();

		Session::flash('success','Contato excluído com sucesso!');
		return Redirect::to('adminContato');
	}

	public function getFilter(){
		$dateIni = Input::get('headInicio');
		$dateFim = Input::get('headFim');

		if(preg_match('#(\d{1,2})\D(\d{1,2})\D(\d{4})#',$dateIni,$match)){
			$time = mktime(0,0,0,$match[2],$match[1],$match[3]);
			$timeBegin = date('Y-m-d',$time);
		}

		if(preg_match('#(\d{1,2})\D(\d{1,2})\D(\d{4})#',$dateFim,$match)){
			$time = mktime(0,0,0,$match[2],$match[1],$match[3]);
			$timeEnd = date('Y-m-d',$time);
		}

		$dataIni = $timeBegin.' 00:00:00';
		$dataFim = $timeEnd.' 23:59:59';

		$contatos = DB::select("Select * from tb_contatos where created_at between '$dataIni' 
								and '$dataFim'");

		$data = array(
			'contatos'   => $contatos,
			'dataInicio' => $dateIni,
			'dataFim' 	 => $dateFim
		);

		$this->layout->content = View::make('list.contato')->with($data);
	}

}