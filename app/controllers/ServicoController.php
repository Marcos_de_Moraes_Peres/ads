<?php

class ServicoController extends \BaseController {

protected $layout = 'layouts.admin.master';

	public function index()
	{
		$servicos =  DB::select("Select a.*, b.titulo as categoria from tb_servicos a left join tb_categorias b on
								 a.idcategoria = b.id where a.status = 1 and a.ididioma = 1 order by a.ordem");

		$bandeiras = DB::select("select * from tb_servicos where ididioma = 2");

		$data = array(
			'servicos'	=> $servicos,
			'bandeiras'	=> $bandeiras
		);

		$this->layout->content = View::make('list.servicos')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$categorias = DB::select("Select a.* from tb_categorias a left join tb_paginas b on a.id = b.idcategoria
								  left join tb_servicos c on a.id = c.idcategoria
								  where a.ididioma = 1 and a.status = 1 and b.id is null and c.id is null");

		$data = array(
			'categorias'	=> $categorias
		);

		$this->layout->content = View::make('create.servicos')->with($data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$servico = new Servico();

		$servico->titulo = Input::get('titulo');
		$servico->ididioma = 1;
		$servico->idtemplate = 6;
		$servico->ordem = 9999;
		$servico->usuario = Auth::user()->nome;
		$servico->slug = Input::get('slug');
		$servico->idcategoria = Input::get('categoria');
		$servico->status = Input::get('status');
		$servico->conteudo = Input::get('conteudo');

		$file = Input::file('arquivo');
		$destino = base_path().'/assets/img/servicos/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

			$servico->idfile = $idfile;

		}

		$servico->save();

		$id = $servico->id;

		if(Input::get('tituloEn')){
			$servico = new Servico();

			$servico->titulo = Input::get('tituloEn');
			$servico->ididioma = 2;
			$servico->idtemplate =6;
			$servico->ordem = 9999;
			$servico->slug = Input::get('slugEn');
			$servico->idcategoria = Input::get('categoria');
			$servico->status = Input::get('status');
			$servico->conteudo = Input::get('conteudoEn');
			$servico->idtraducao = $id;

			if($file){
				$servico->idfile = $idfile;
			}

			$servico->save();
		}

		Session::flash('success',"Serviço cadastrado com sucesso!");
		return Redirect::to('adminServico');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$servico = DB::select("Select a.*, b.titulo as categoria from tb_servicos a left join tb_categorias b on 
							  a.idcategoria = b.id where a.id = $id");

		$idcategoria = $servico[0]->idcategoria;

		if($idcategoria){
			$categorias = DB::select("Select a.* from tb_categorias a left join tb_paginas b on a.id = b.idcategoria
									  left join tb_servicos c on a.id = c.idcategoria
								  	  where a.ididioma = 1 and a.id <> $idcategoria and a.status = 1 and b.id is null
								  	  and c.id is null");

		}else{
			$categorias = DB::select("Select a.* from tb_categorias a left join tb_paginas b on a.id = b.idcategoria
									  left join tb_servicos c on a.id = c.idcategoria
								  	  where a.ididioma = 1 and a.status = 1 and b.id is null and c.id is null");
		}

		$servicoEn = DB::select("Select * from tb_servicos where idtraducao = $id and ididioma = 2");

		$data = array(
			'servico'		=> $servico,
			'categorias'	=> $categorias,
			'servicoEn'		=> $servicoEn
		);

		$this->layout->content = View::make('edit.servicos')->with($data);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		$file = Input::file('arquivo');
		$destino = base_path().'/assets/img/servicos/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

		}

		if($file){
			$data = array(
				'titulo' 	  => Input::get('titulo'),
				'idcategoria' => Input::get('categoria'),
				'status' 	  => Input::get('status'),
				'conteudo' 	  => Input::get('conteudo'),
				'slug' 	 	  => Input::get('slug'),
				'usuario'	  => Auth::user()->nome,
				'idfile'	  => $idfile
			);
		}else{
			$data = array(
				'titulo' 	  => Input::get('titulo'),
				'idcategoria' => Input::get('categoria'),
				'status' 	  => Input::get('status'),
				'slug' 		  => Input::get('slug'),
				'usuario' 	  => Auth::user()->nome,
				'conteudo' 	  => Input::get('conteudo')
			);
		}

		Servico::where('id','=',$id)->update($data);

		if($servicos = DB::select("Select * from tb_servicos where id = $id")){

			if(Input::get('tituloEn')){
				$idEn = DB::select("Select * from tb_servicos where idtraducao = $id and ididioma = 2");
				$nomeEn = Input::get('tituloEn');
				$conteudoEn = Input::get('conteudoEn');
				$slugEn = Input::get('slugEn');
				$ordem = $servicos[0]->ordem;


				if(isset($idEn[0]->id)){
					if($file){
						DB::update("Update tb_servicos set titulo = ?, conteudo = ?, idfile = ?, slug = ?
									where idtraducao = $id and ididioma = 2",
									array($nomeEn,$conteudoEn,$idfile,$slugEn));
					}else{
						DB::update("Update tb_servicos set titulo = ?, conteudo = ?, slug = ?
									where idtraducao = $id and ididioma = 2",
									array($nomeEn,$conteudoEn,$slugEn));
					}
				}else{
					$servico = new Pagina();

					$servico->titulo = Input::get('tituloEn');
					$servico->ididioma = 2;
					$servico->idtemplate =6;
					$servico->ordem = $ordem;
					$servico->slug = Input::get('slugEn');
					$servico->idcategoria = Input::get('categoria');
					$servico->status = Input::get('status');
					$servico->conteudo = Input::get('conteudoEn');
					$servico->idtraducao = $id;

					if($file){
						$servico->idfile = $idfile;
					}else{
						if($idRet = DB::select("Select idfile from tb_servicos where id = $id")){
							$servico->idfile = $idRet[0]->idfile;
						}
					}

					$servico->save();
				}

			}
		}else{
			DB::delete("delete from tb_servicos where idtraducao = $id and ididioma = 2");
		}

		Session::flash('success',"Serviço atualizado com sucesso!");
		return Redirect::to('adminServico');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Servico::where('id','=',$id)->delete();
		Servico::where('idtraducao','=',$id)->delete();

		Session::flash('success',"Serviço excluído com sucesso!");
		return Redirect::to('adminServico');
	}

	public function getOrder(){
		$servicos = DB::select("Select * from tb_servicos where status = 1 and ididioma = 1 order by ordem");

		$this->layout->content = View::make('order.servicos')->with('servicos',$servicos);
	}

	public function postFilter(){
		$datas = Input::all();

		array_shift($datas);

		foreach($datas as $key => $data){
			$datafile = array(
				'ordem' => $key+1
			);

			Servico::where('id','=',$data)->update($datafile);
			Servico::where('idtraducao','=',$data)->update($datafile);
		}


		Session::flash('success',"Registros ordenados com sucesso!");
		return Redirect::to('adminServicos/order');

	}


}
