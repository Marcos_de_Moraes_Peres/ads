<?php

class HomeCurriculoController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug)
	{
		$ididioma = Session::get('idioma');

		$v1 = rand(1,5);
		$v2 = rand(1,5);

		$soma = $v1 + $v2;

		Session::put('soma',$soma);

		if($ididioma == 1){
			$pagina = DB::select("Select a.* from tb_paginas a left join tb_paginas b on a.id = b.idtraducao
								  where b.slug = '$slug' and a.ididioma = $ididioma");


			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('curriculo/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");
			}

		}else{

			$pagina = DB::select("Select b.* from tb_paginas a left join tb_paginas b 
								  on a.id = b.idtraducao where a.slug = '$slug' and b.ididioma = $ididioma");

			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('curriculo/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

			}

			if($pagina[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}

		}

		if($pagina){

			$data = array(
				'pagina'	=> $pagina,
				'v1'		=> $v1,
				'v2'		=> $v2
			);

			Session::put('title',$pagina[0]->titulo);

			$this->layout->content = View::make('home.curriculo')->with($data);

		}else{
			return Redirect::to('/');
		}
	}

	public function getFormCurriculo(){

		$soma = Input::get('soma');

		$slug = Input::get('slug');

		if(Session::get('soma') == $soma){

			$curriculo = new Curriculo();
			$datafiles = new Files();

			$curriculo->nome = Input::get('nome');
			$curriculo->email = Input::get('email');
			$curriculo->telefone = Input::get('tel');
			$curriculo->iddepto = Input::get('depto');
			$curriculo->iddepto = Input::get('situacao');

			$file = Input::file('arquivo');


			if($file){

					$destinationPath = base_path().'/assets/docs/';
					$filename = str_random(5).$file->getClientOriginalName();

					$datafiles->arquivo = $filename;

					$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

					$datafiles->save();

					$curriculo->idfile = $datafiles->id;

				}


			$curriculo->save();

			$data = array(
				'nome' => Input::get('nome'),
				'email' => Input::get('email'),
				'telefone' => Input::get('tel'),
				'iddepto' => Input::get('depto')
			);

			Mail::send('emails.curriculo', $data, function($message)
			{
				$message->to('contato@adsbrasil.com.br')->subject('Envio de currículo - Site ADS');
			});

			Session::flash('success', 'Curriculo foi enviado com sucesso!');
			return Redirect::to("curriculo/$slug");
		}else{
			Session::put('nome',Input::get('nome'));
			Session::put('email',Input::get('email'));
			Session::put('tel',Input::get('tel'));
			Session::put('depto',Input::get('depto'));

			Session::flash('danger', 'Valor da soma incorreta!');
			return Redirect::to("curriculo/$slug");
		}

	}

}