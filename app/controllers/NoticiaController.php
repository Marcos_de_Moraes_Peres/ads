<?php

class NoticiaController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$noticias = DB::table('tb_noticias as a')
		->leftjoin('tb_clientes as b','a.idcliente','=','b.id')
		->select(array('a.*','b.nome as cliente'))
		->where('a.ididioma','=',1)
		->orderBy('a.status','DESC')
		->get();

		$bandeiras = DB::select("select * from tb_noticias where ididioma = 2");

		$data = array(
			'noticias'	=> $noticias,
			'bandeiras'	=> $bandeiras
		);

		$this->layout->content = View::make('list.noticia')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$clientes = DB::select('Select * from tb_clientes where status = 1 and ididioma = 1 order by nome');

		$this->layout->content = View::make('create.noticia')->with('clientes',$clientes);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$noticia = new Noticia();

		$date = Input::get('headInicio');

		if(preg_match('#(\d{1,2})\D(\d{1,2})\D(\d{4})#',$date,$match)){
			$time = mktime(0,0,0,$match[2],$match[1],$match[3]);
			$timeBegin = date('Y-m-d',$time);
		}

		$noticia->titulo = Input::get('titulo');
		$noticia->ididioma = 1;
		$noticia->status = Input::get('status');
		$noticia->publicacao = $timeBegin;
		$noticia->slug = date('dmY').Input::get('slug');
		$noticia->idcliente = Input::get('cliente');
		$noticia->resumo = Input::get('resumo');
		$noticia->usuario = Auth::user()->nome;
		$noticia->conteudo = Input::get('conteudo');

		$noticia->save();

		$id = $noticia->id;

		if(Input::get('tituloEn')){
			$noticia = new Noticia();

			$noticia->titulo = Input::get('tituloEn');
			$noticia->ididioma = 2;
			$noticia->status = Input::get('status');
			$noticia->publicacao = $timeBegin;
			$noticia->slug = date('dmY').Input::get('slugEn');
			$noticia->idcliente = Input::get('cliente');
			$noticia->resumo = Input::get('resumoEn');
			$noticia->conteudo = Input::get('conteudoEn');
			$noticia->idtraducao = $id;

			$noticia->save();
		}

		Session::flash('success',"Notícia cadastrada com sucesso!");
		return Redirect::to('adminNoticia');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$noticia = DB::select("Select a.*,b.nome as cliente from tb_noticias a left join tb_clientes b on 
							   a.idcliente = b.id where a.id = $id");

		$idcliente = $noticia[0]->idcliente;

		$clientes = DB::select("Select * from tb_clientes where status = 1 and id <> $idcliente order by nome");

		$noticiaEn = DB::select("Select * from tb_noticias where idtraducao = $id");

		$data = array(
			'noticia'	=> $noticia,
			'noticiaEn'	=> $noticiaEn,
			'clientes'	=> $clientes
		);

		$this->layout->content = View::make('edit.noticia')->with($data);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$date = Input::get('headInicio');

		if(preg_match('#(\d{1,2})\D(\d{1,2})\D(\d{4})#',$date,$match)){
			$time = mktime(0,0,0,$match[2],$match[1],$match[3]);
			$timeBegin = date('Y-m-d',$time);
		}

		$numeros = substr(Input::get('slug'), 0,7);

		if(is_numeric($numeros)){
			$slug = Input::get('slug');
			$slug_en = Input::get('slugEn');
		}else{
			$slug = date('dmY').Input::get('slug');
			$slug_en = date('dmY').Input::get('slugEn');
		}

		$data = array(
			'titulo'		=> Input::get('titulo'),
			'ididioma'		=> 1,
			'status'   		=> Input::get('status'),
			'publicacao'	=> $timeBegin,
			'slug'   		=> $slug,
			'idcliente'   	=> Input::get('cliente'),
			'resumo'   		=> Input::get('resumo'),
			'usuario' 		=> Auth::user()->nome,
			'conteudo'   	=> Input::get('conteudo')
		);

		Noticia::where('id','=',$id)->update($data);

		if(Input::get('tituloEn')){
			$idEn = DB::select("Select id from tb_noticias where idtraducao = $id");

			if(isset($idEn[0]->id)){

				$data = array(
					'titulo'		=> Input::get('tituloEn'),
					'ididioma'		=> 2,
					'status'   		=> Input::get('status'),
					'publicacao'	=> $timeBegin,
					'slug'   		=> $slug_en,
					'idcliente'   	=> Input::get('cliente'),
					'resumo'   		=> Input::get('resumoEn'),
					'conteudo'   	=> Input::get('conteudoEn')
				);

				Noticia::where('idtraducao','=',$id)->update($data);

			}else{
				$noticia = new Noticia();

				$noticia->titulo = Input::get('tituloEn');
				$noticia->ididioma = 2;
				$noticia->status = Input::get('status');
				$noticia->publicacao = $timeBegin;
				$noticia->slug = $slug_en;
				$noticia->idcliente = Input::get('cliente');
				$noticia->resumo = Input::get('resumoEn');
				$noticia->conteudo = Input::get('conteudoEn');
				$noticia->idtraducao = $id;

				$noticia->save();
			}
		}else{
			DB::delete("delete from tb_noticias where idtraducao = $id");
		}

		Session::flash('success','Notícia atualizada com sucesso!');
		return Redirect::to('adminNoticia');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Noticia::where('id','=',$id)->delete();
		Noticia::where('idtraducao','=',$id)->delete();

		Session::flash('success',"Notícia excluída com sucesso!");
		return Redirect::to('adminNoticia');
	}


}
