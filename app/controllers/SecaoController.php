<?php

class SecaoController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$secoes = DB::select("Select * from tb_secoes where ididioma = 1 order by status desc");

		$bandeiras = DB::select("Select idtraducao from tb_secoes where idtraducao > 0");

		$data = array(
			'secoes' 	=> $secoes,
			'bandeiras' => $bandeiras
		);

		$this->layout->content = View::make('list.secao')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('create.secao');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$secao = new Secao();

		$secao->titulo = Input::get('titulo');
		$secao->link = Input::get('link');
		$secao->status = Input::get('status');
		$secao->usuario = Auth::user()->nome;
		$secao->ordem = 9999;
		$secao->ididioma = 1;

		$secao->save();

		$idsecao = $secao->id;

		if(Input::get('tituloEn')){
			$secao = new Secao();

			$secao->titulo = Input::get('tituloEn');
			$secao->link = Input::get('link');
			$secao->status = Input::get('status');
			$secao->ordem = 9999;
			$secao->ididioma = 2;
			$secao->idtraducao = $idsecao;

			$secao->save();
		}

		Session::flash('success','Seção cadastrada com sucesso!');
		return Redirect::to('sections');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$secao = Secao::find($id);

		$traducao = DB::select("Select * from tb_secoes where idtraducao = $id");

		$data = array(
			'secao' 	=> $secao,
			'traducao' 	=> $traducao
		);

		$this->layout->content = View::make('edit.secao')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = array(
			'titulo'	=> Input::get('titulo'),
			'link'	   	=> Input::get('link'),
			'usuario'   => Auth::user()->nome,
			'status'   	=> Input::get('status')
		);

		Secao::where('id','=',$id)->update($data);

		if($secao = DB::select("Select * from tb_secoes where id = $id")){

			if(Input::get('tituloEn')){
				$idEn = DB::select("Select id from tb_secoes where idtraducao = $id");
				$nomeEn = Input::get('tituloEn');
				$linkEn = Input::get('link');
				$statusEn = Input::get('status');
				$ordem = $secao[0]->ordem;

				if(isset($idEn[0]->id)){

					$data = array(
						'titulo' => $nomeEn,
						'status' => $statusEn,
						'link'   => $linkEn
					);

					Secao::where('idtraducao','=',$id)->update($data);

				}else{
					$section = new Secao();

					$section->titulo = $nomeEn;
					$section->status = $statusEn;
					$section->link = $linkEn;
					$section->ididioma = 2;
					$section->idtraducao = $id;
					$section->ordem = $ordem;

					$section->save();
				}
			}else{
				DB::delete("delete from tb_secoes where idtraducao = $id");
			}
		}

		Session::flash('success','Seção atualizada com sucesso!');
		return Redirect::to('sections');

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Secao::where('id','=',$id)->delete();
		Secao::where('idtraducao','=',$id)->delete();

		Session::flash('success','Seção excluída com sucesso!');
		return Redirect::to('sections');
	}

	public function getOrder(){
		$secoes = DB::select("Select * from tb_secoes where status = 1 and ididioma = 1 order by ordem");

		$this->layout->content = View::make('order.secao')->with('secoes',$secoes);
	}

	public function postFilter(){
		$datas = Input::all();

		array_shift($datas);

		foreach($datas as $key => $data){
			$datafile = array(
				'ordem' => $key+1
			);

			Secao::where('id','=',$data)->update($datafile);
			Secao::where('idtraducao','=',$data)->update($datafile);
		}


		Session::flash('success',"Registros ordenados com sucesso!");
		return Redirect::to('section/order');

	}


}
