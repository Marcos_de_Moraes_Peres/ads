<?php

class HomeContatoController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug)
	{

		/*$release = DB::select("Select * from releases");

		foreach($release as $res){

			$noticia = new Noticia();

			$noticia->titulo 		= $res->release_titulo;
			$noticia->resumo 		= $res->release_chamada;
			$noticia->conteudo 		= $res->release_conteudo;
			$noticia->publicacao 	= $res->release_data;
			$noticia->slug 			= $res->release_titulo;
			$noticia->idcliente		= $res->release_cliente;
			$noticia->created_at	= $res->data_cad;
			$noticia->updated_at	= $res->data_upd;
			$noticia->status 		= 1;
			$noticia->ididioma		= 1;

			$noticia->save();

		}*/

		/*$release = DB::select("Select * from releases");
		foreach($release as $res){

			$conteudo = $res->release_conteudo;
			$id = $res->release_ID;

			$conteudo = str_replace('&lt;', '<', $conteudo);
			$conteudo = str_replace('&gt;', '>', $conteudo);		
			$conteudo = str_replace('&nbsp;', '', $conteudo);		
			$conteudo = str_replace('&Aacute;', 'Á', $conteudo);
			$conteudo = str_replace('&aacute;', 'á', $conteudo);
			$conteudo = str_replace('&Acirc;', 'Â', $conteudo);
			$conteudo = str_replace('&acirc;', 'â', $conteudo);
			$conteudo = str_replace('&Agrave;', 'À', $conteudo);
			$conteudo = str_replace('&agrave;', 'à', $conteudo);
			$conteudo = str_replace('&Atilde;', 'Ã', $conteudo);
			$conteudo = str_replace('&atilde;', 'ã', $conteudo);
			$conteudo = str_replace('&Eacute;', 'É', $conteudo);
			$conteudo = str_replace('&eacute;', 'é', $conteudo);
			$conteudo = str_replace('&Ecirc;', 'Ê', $conteudo);
			$conteudo = str_replace('&ecirc;', 'ê', $conteudo);
			$conteudo = str_replace('&Egrave;', 'È', $conteudo);
			$conteudo = str_replace('&egrave;', 'è', $conteudo);
			$conteudo = str_replace('&Iacute;', 'Í', $conteudo);
			$conteudo = str_replace('&iacute;', 'í', $conteudo);
			$conteudo = str_replace('&Icirc;', 'Î', $conteudo);
			$conteudo = str_replace('&icirc;', 'î', $conteudo);
			$conteudo = str_replace('&Igrave;', 'Ì', $conteudo);
			$conteudo = str_replace('&igrave;', 'ì', $conteudo);
			$conteudo = str_replace('&Oacute;', 'Ó', $conteudo);
			$conteudo = str_replace('&oacute;', 'ó', $conteudo);
			$conteudo = str_replace('&Ocirc;', 'Ô', $conteudo);
			$conteudo = str_replace('&ocirc;', 'ô', $conteudo);
			$conteudo = str_replace('&ocirc;', 'ô', $conteudo);
			$conteudo = str_replace('&Ograve;', 'Ò', $conteudo);
			$conteudo = str_replace('&ograve;', 'ò', $conteudo);
			$conteudo = str_replace('&Otilde;', 'Õ', $conteudo);
			$conteudo = str_replace('&otilde;', 'õ', $conteudo);
			$conteudo = str_replace('&Uacute;', 'Ú', $conteudo);
			$conteudo = str_replace('&uacute;', 'ú', $conteudo);
			$conteudo = str_replace('&uacute;', 'ú', $conteudo);
			$conteudo = str_replace('&Ucirc;', 'Û', $conteudo);
			$conteudo = str_replace('&ucirc;', 'û', $conteudo);
			$conteudo = str_replace('&Ugrave;', 'Ù', $conteudo);
			$conteudo = str_replace('&ugrave;','ù', $conteudo);
			$conteudo = str_replace('&Uuml;','Ü', $conteudo);
			$conteudo = str_replace('&uuml;','ü', $conteudo);
			$conteudo = str_replace('&Ccedil;','Ç', $conteudo);
			$conteudo = str_replace('&ccedil;','ç', $conteudo);
			$conteudo = str_replace('&amp;','&', $conteudo);
			$conteudo = str_replace('&quot;','"', $conteudo);
			$conteudo = str_replace('&reg;','®', $conteudo);
			$conteudo = str_replace('&copy;','©', $conteudo);

			$data = array(
				'release_conteudo' => $conteudo
			);

			Release::where('release_ID','=',$id)->update($data);

		}*/

		$ididioma = Session::get('idioma');

		$v1 = rand(1,5);
		$v2 = rand(1,5);

		$soma = $v1 + $v2;

		Session::put('soma',$soma);

		if($ididioma == 1){
			$pagina = DB::select("Select a.* from tb_paginas a left join tb_paginas b on a.id = b.idtraducao
								  where b.slug = '$slug' and a.ididioma = $ididioma");


			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('contato/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");
			}

		}else{

			$pagina = DB::select("Select b.* from tb_paginas a left join tb_paginas b 
								  on a.id = b.idtraducao where a.slug = '$slug' and b.ididioma = $ididioma");

			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('contact/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

			}

			if($pagina[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}

		}

		if($pagina){

			$data = array(
				'pagina'	=> $pagina,
				'v1'		=> $v1,
				'v2'		=> $v2
			);

			Session::put('title',$pagina[0]->titulo);

			$this->layout->content = View::make('home.contato')->with($data);

		}else{
			return Redirect::to('/');
		}
	}

	public function getFormContato(){

		$soma = Input::get('soma');

		if(Session::get('soma') == $soma){

			$contato = new Contato();

			$contato->nome = Input::get('nome');
			$contato->empresa = Input::get('empresa');
			$contato->telefone = Input::get('tel');
			$contato->email = Input::get('email');
			$contato->assunto = Input::get('assunto');
			$contato->mensagem = Input::get('mensagem');

			$contato->save();

			$data = array(
				'nome' => Input::get('nome'),
				'empresa' => Input::get('empresa'),
				'telefone' => Input::get('tel'),
				'email' => Input::get('email'),
				'assunto' => Input::get('assunto'),
				'mensagem' => Input::get('mensagem')
			);

			Mail::send('emails.contato', $data, function($message)
			{
				$message->to('contato@adsbrasil.com.br')->subject('Contato - Site ADS');
			});

			$slug = Input::get('slug');

			if(Session::get('idioma') == 1){
				Session::flash('success',"Mensagem enviada com sucesso!");
				return Redirect::to("contato/$slug");
			}else{
				Session::flash('success',"Message sent successfully!");
				return Redirect::to("contact/$slug");
			}

		}else{

			Session::put('nome',Input::get('nome'));
			Session::put('empresa',Input::get('empresa'));
			Session::put('email',Input::get('email'));
			Session::put('tel',Input::get('tel'));
			Session::put('assunto',Input::get('assunto'));
			Session::put('mensagem',Input::get('mensagem'));

			$slug = Input::get('slug');

			if(Session::get('idioma') == 1){
				Session::flash('danger',"Valor da soma incorreta!");
				return Redirect::to("contato/$slug");
			}else{
				Session::flash('danger',"Incorrect value of sum!");
				return Redirect::to("contact/$slug");
			}
		}

	}

}