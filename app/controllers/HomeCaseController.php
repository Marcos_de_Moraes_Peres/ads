<?php

class HomeCaseController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug)
	{
		$ididioma = Session::get('idioma');

		if($ididioma == 1){
			$verify =  DB::select("Select a.* from tb_cases a left join tb_cases b on a.id = b.idtraducao
									where b.slug = '$slug' and a.ididioma = $ididioma and a.slug <> b.slug");

			if($verify){
				$case = DB::select("Select a.* from tb_cases a left join tb_cases b on a.id = b.idtraducao
									where b.slug = '$slug' and a.ididioma = $ididioma");
			}else{
				$case = null;
			}


			//Se vier da tradução redireciona
			if($case){
				return Redirect::to('case/'.$case[0]->slug);
			}else{
				$case = DB::select("Select a.*, b.arquivo from tb_cases a left join tb_files b
									on a.idfile = b.id where a.slug = '$slug' and a.ididioma = 1");

				$secao = DB::select("Select slug from tb_paginas where idsecao = 15 and ididioma = 1");

				$imagens = DB::select("Select c.link from tb_cases a
									   left join tb_galerias_locais b on a.id = b.idcase
									   left join tb_imagens c on b.idgaleria = c.idgaleria
									   where a.slug = '$slug' order by c.ordem");

				$videos = DB::select("Select c.link from tb_cases a
									   left join tb_galerias_locais b on a.id = b.idcase
									   left join tb_videos c on b.idgaleria = c.idgaleria
									   where a.slug = '$slug' order by c.ordem");

				$cases = DB::select("Select a.*, b.arquivo, c.titulo as servico from tb_cases a left join tb_files b on
									 a.idfile = b.id left join tb_servicos c on a.idservico = c.id where a.ididioma = 1
									 and a.status = 1 order by a.ordem asc");

				if(!$case){
					return Redirect::to('/');
				}
			}
		}else{

			$verify = DB::select("Select b.* from tb_cases a left join tb_cases b 
								on a.id = b.idtraducao where a.slug = '$slug' and b.ididioma = $ididioma and a.slug <> b.slug");

			if($verify){
				$case = DB::select("Select b.* from tb_cases a left join tb_cases b 
									on a.id = b.idtraducao where a.slug = '$slug' and b.ididioma = $ididioma");	
			}else{
				$case = null;
			}

			//Se vier da tradução redireciona
			if($case){
				return Redirect::to('case/'.$case[0]->slug);
			}else{
				$case = DB::select("Select a.*, b.arquivo from tb_cases a left join tb_files b
									on a.idfile = b.id where a.slug = '$slug' and a.ididioma = 2");

				$secao = DB::select("Select slug from tb_paginas where idsecao = 15 and ididioma = 2");

				$imagens = DB::select("Select c.link from tb_cases a
									   left join tb_galerias_locais b on a.idtraducao = b.idcase
									   left join tb_imagens c on b.idgaleria = c.idgaleria
									   where a.slug = '$slug' order by c.ordem");

				$videos = DB::select("Select c.link from tb_cases a
									   left join tb_galerias_locais b on a.idtraducao = b.idcase
									   left join tb_videos c on b.idgaleria = c.idgaleria
									   where a.slug = '$slug' order by c.ordem");

				$cases = DB::select("Select a.*, b.arquivo, c.titulo as servico from tb_cases a left join tb_files b on
									 a.idfile = b.id left join tb_servicos c on a.idservico = c.idtraducao where a.ididioma = 2
									 and a.status = 1 order by a.ordem asc");

			}

			if(!$case){
				return Redirect::to('/');
			}

			if($case[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}

		}

		if($case){

			$data = array(
				'case' 		=> $case,
				'secao'		=> $secao,
				'imagens'	=> $imagens,
				'cases'		=> $cases,
				'videos'	=> $videos
			);

			Session::put('title',$case[0]->titulo);

			$this->layout->content = View::make('home.case')->with($data);

		}else{
			return Redirect::to('/');
		}
	}

}
