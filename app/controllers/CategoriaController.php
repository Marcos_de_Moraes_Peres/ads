<?php

class CategoriaController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categorias = DB::select("Select a.*, b.titulo as secao from tb_categorias a left join tb_secoes b 
								  on a.idsecao = b.id where a.ididioma = 1 order by a.status desc");

		$bandeiras = DB::select("Select idtraducao from tb_categorias where idtraducao > 0");

		$data = array(
			'categorias' => $categorias,
			'bandeiras'  => $bandeiras
		);

		$this->layout->content = View::make('list.categoria')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$secoes = DB::select('Select * from tb_secoes where status = 1 and ididioma = 1');

		$this->layout->content = View::make('create.categoria')->with('secoes',$secoes);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$categoria = new Categoria();

		$categoria->titulo = Input::get('titulo');
		$categoria->link = Input::get('link');
		$categoria->status = Input::get('status');
		$categoria->ordem = 9999;
		$categoria->ididioma = 1;
		$categoria->usuario = Auth::user()->nome;
		$categoria->idsecao = Input::get('secao');

		$categoria->save();

		$idcategoria = $categoria->id;

		if(Input::get('tituloEn')){
			$categoria = new Categoria();

			$categoria->titulo = Input::get('tituloEn');
			$categoria->link = Input::get('link');
			$categoria->status = Input::get('status');
			$categoria->ordem = 9999;
			$categoria->ididioma = 2;
			$categoria->idtraducao = $idcategoria;
			$categoria->idsecao = Input::get('secao');

			$categoria->save();
		}

		Session::flash('success','Categoria cadastrada com sucesso!');
		return Redirect::to('category');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$categoria = DB::select("Select a.*,b.titulo as secao from tb_categorias a left join tb_secoes b on
								 a.idsecao = b.id where a.id = $id");

		$idsecao = $categoria[0]->idsecao;

		$secoes = DB::select("Select * from tb_secoes where id <> $idsecao and ididioma = 1");

		$traducao = DB::select("Select * from tb_categorias where idtraducao = $id");

		$data = array(
			'categoria' => $categoria,
			'traducao' 	=> $traducao,
			'secoes'	=> $secoes
		);

		$this->layout->content = View::make('edit.categoria')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = array(
			'titulo'	=> Input::get('titulo'),
			'link'	   	=> Input::get('link'),
			'status'   	=> Input::get('status'),
			'usuario' 	=> Auth::user()->nome,
			'idsecao' 	=> Input::get('secao')
		);

		Categoria::where('id','=',$id)->update($data);

		if($categoria = DB::select("Select * from tb_categorias where id = $id")){

			if(Input::get('tituloEn')){
				$idEn = DB::select("Select id from tb_categorias where idtraducao = $id");
				$nomeEn = Input::get('tituloEn');
				$linkEn = Input::get('link');
				$statusEn = Input::get('status');
				$idsecaoEn = Input::get('secao');
				$ordem = $categoria[0]->ordem;

				if(isset($idEn[0]->id)){

					DB::update("Update tb_categorias set titulo = ?,status = ?, link = ?,idsecao = ?
					 where idtraducao = $id",array($nomeEn,$statusEn,$linkEn,$idsecaoEn));

				}else{
					$category = new Categoria();

					$category->titulo = $nomeEn;
					$category->status = $statusEn;
					$category->link = $linkEn;
					$category->ididioma = 2;
					$category->idtraducao = $id;
					$category->idsecao = $idsecaoEn;
					$category->ordem = $ordem;

					$category->save();
				}
			}else{
				DB::delete("delete from tb_categorias where idtraducao = $id");
			}
		}

		Session::flash('success','Categoria atualizada com sucesso!');
		return Redirect::to('category');

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Responsesecoes
	 */
	public function destroy($id)
	{
		Categoria::where('id','=',$id)->delete();
		Categoria::where('idtraducao','=',$id)->delete();

		Session::flash('success','Categoria excluída com sucesso!');
		return Redirect::to('category');
	}

	public function getOrder(){

		$secoes = DB::select("Select * from tb_secoes where ididioma = 1 and status = 1");

		$this->layout->content = View::make('order.categoria')->with('secoes',$secoes);
	}

	public function postFilter(){
		$datas = Input::all();

		array_shift($datas);

		foreach($datas as $key => $data){
			$datafile = array(
				'ordem' => $key+1
			);
			$idcat = $data;

			Categoria::where('id','=',$data)->update($datafile);
			Categoria::where('idtraducao','=',$data)->update($datafile);
		}

		$categoria = DB::select("Select * from tb_categorias where id = $idcat");

		$idsecao = $categoria[0]->idsecao;

		Session::flash('success',"Registros ordenados com sucesso!");
		$this->postFiltro($idsecao);

	}

	public function postFiltro($idsecao = null){
		if($idsecao){
			$id = $idsecao;
		}else{
			$id = Input::get('idsecao');
		}

		$secoes = DB::select("Select a.* from tb_secoes a where status = 1 and ididioma = 1");

		$categorias = DB::select("Select * from tb_categorias where status = 1 and ididioma = 1 and idsecao = $id order by ordem"); 

		$data = array(
			'secoes' 	 => $secoes,
			'categorias' => $categorias,
			'idsecao'	 => $id
		);

		$this->layout->content = View::make('order.categoria')->with($data);

	}


}
