<?php

class HomeClienteController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug)
	{
		$date = date('Y-m-d');
		$ididioma = Session::get('idioma');

		if($ididioma == 1){

			$verify = DB::select("Select a.*,c.arquivo from tb_clientes a left join tb_clientes b on a.id = b.idtraducao
								  left join tb_files c on a.idfile = c.id
								  where b.slug = '$slug' and a.ididioma = $ididioma and a.slug <> b.slug");

			if($verify){
				$cliente = DB::select("Select a.*,c.arquivo from tb_clientes a left join tb_clientes b on a.id = b.idtraducao
								  	   left join tb_files c on a.idfile = c.id
								  	   where b.slug = '$slug' and a.ididioma = $ididioma");
			}else{
				$cliente = null;
			}


			//Se vier da tradução redireciona
			if($cliente){
				return Redirect::to('cliente/'.$cliente[0]->slug);
			}else{
				$cliente = DB::select("Select a.*, b.arquivo from tb_clientes a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug' and ididioma = 1");

				$idcliente = $cliente[0]->id;

				$noticias = DB::select("Select a.*, b.slug as url from tb_noticias a left join tb_clientes b on
										a.idcliente = b.id where a.status = 1 and a.ididioma = 1 
										and a.publicacao <= '$date' and a.idcliente = $idcliente 
										order by a.publicacao desc limit 4");

				$secao = DB::select("Select slug from tb_paginas where idsecao = 16 and ididioma = 1");

				if(!$cliente){
					return Redirect::to('/');
				}

			}

		}else{

			$verify = DB::select("Select b.*, c.arquivo from tb_clientes a left join tb_clientes b 
								  on a.id = b.idtraducao left join tb_files c on b.idfile = c.id
								  where a.slug = '$slug' and b.ididioma = $ididioma and a.slug <> b.slug");

			if($verify){
				$cliente = DB::select("Select b.*, c.arquivo from tb_clientes a left join tb_clientes b 
									  on a.id = b.idtraducao left join tb_files c on b.idfile = c.id
									  where a.slug = '$slug' and b.ididioma = $ididioma");
			}else{
				$cliente = null;
			}

			//Se vier da tradução redireciona
			if($cliente){
				return Redirect::to('client/'.$cliente[0]->slug);
			}else{
				$cliente = DB::select("Select a.*, b.arquivo from tb_clientes a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug' and a.ididioma = 2");

				if(isset($cliente[0])){

					$idcliente = $cliente[0]->idtraducao;

					$noticias = DB::select("Select a.*, b.slug as url from tb_noticias a left join tb_clientes b on
											a.idcliente = b.idtraducao where a.status = 1 and a.ididioma = 2
											and a.publicacao <= '$date' and a.idcliente = $idcliente 
											order by a.publicacao desc limit 4");

					$secao = DB::select("Select slug from tb_paginas where idsecao = 16 and ididioma = 2");
				}

			}

			if(!$cliente){
				return Redirect::to('/');
			}

			if($cliente[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}

		}

		if($cliente){

			$data = array(
				'cliente' 	=> $cliente,
				'noticias'	=> $noticias,
				'secao'		=> $secao
			);

			Session::put('title',$cliente[0]->nome);

			$this->layout->content = View::make('home.cliente')->with($data);

		}else{
			return Redirect::to('/');
		}
	}

}
