<?php

class GaleriaController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$galerias = DB::select('Select * from tb_galerias where status = 1');

		foreach($galerias as &$galeria){

			$idgaleria = $galeria->id;

			$cases = DB::select("Select a.titulo as cases from tb_cases a inner join 
								 tb_galerias_locais b on a.id = b.idcase
								 where b.idgaleria = $idgaleria");

			$noticias = DB::select("Select a.titulo as noticias from tb_noticias a inner join 
									tb_galerias_locais b on a.id = b.idnoticia where 
									b.idgaleria = $idgaleria");

			$galeria->cases = $cases;
			$galeria->noticias = $noticias;

		}

		$this->layout->content = View::make('list.galeria')->with('galerias',$galerias);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$cases = DB::select('Select * from tb_cases where ididioma = 1');

		$noticias = DB::select('Select * from tb_noticias where ididioma = 1 and status = 1');

		$data = array(
			'cases' 	=> $cases,
			'noticias' 	=> $noticias
		);

		$this->layout->content = View::make('create.galeria')->with($data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$galeria = new Galeria();

		$galeria->titulo = Input::get('titulo');
		$galeria->status = Input::get('status');
		$galeria->usuario = Auth::user()->nome;

		$galeria->save();

		$idgaleria = $galeria->id;

		$idcases = Input::get('idcase');
		if($idcases){
			foreach($idcases as $idcase){
				if($idcase <> 0){
					$local = new GaleriaLocal();

					$local->idcase = $idcase;
					$local->idgaleria = $idgaleria;

					$local->save();
				}
			}
		}

		$idnoticias = Input::get('idnoticia');
		if($idnoticias){
			foreach($idnoticias as $idnoticia){
				if($idnoticia <> 0){
					$local = new GaleriaLocal();

					$local->idnoticia = $idnoticia;
					$local->idgaleria = $idgaleria;

					$local->save();
				}
			}
		}

		Session::flash('success','Registro criado com sucesso!');
		return Redirect::to('adminGallery');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$galeria = DB::select("Select * from tb_galerias where id = $id");

		$selectedCases = DB::select("Select a.id, a.titulo from tb_cases a inner join tb_galerias_locais b
									 on a.id = b.idcase where idgaleria = $id and a.ididioma = 1");
		$cases = DB::select("Select a.id, a.titulo from tb_cases a left join tb_galerias_locais b
							 on a.id = b.idcase where idgaleria is null and a.ididioma = 1");

		$selectedNoticias = DB::select("Select a.id, a.titulo from tb_noticias a inner join 
										tb_galerias_locais b on a.id = b.idnoticia where idgaleria = $id 
										and a.ididioma = 1");
		$noticias = DB::select("Select a.id, a.titulo from tb_noticias a left join tb_galerias_locais b
								on a.id = b.idnoticia where idgaleria is null and a.ididioma = 1");

		$data = array(
			'galeria' 			=> $galeria,
			'selectedCases' 	=> $selectedCases,
			'cases' 			=> $cases,
			'selectedNoticias' 	=> $selectedNoticias,
			'noticias' 			=> $noticias
		);

		$this->layout->content = View::make('edit.galeria')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = array(
			'titulo'  => Input::get('titulo'),
			'usuario' => Auth::user()->nome,
			'status'  => Input::get('status')
		);

		Galeria::where('id','=',$id)->update($data);
		GaleriaLocal::where('idgaleria','=',$id)->delete();

		$idcases = Input::get('idcase');
		foreach($idcases as $idcase){
			if($idcase <> 0){
				$local = new GaleriaLocal();

				$local->idcase = $idcase;
				$local->idgaleria = $id;

				$local->save();
			}
		}

		$idnoticias = Input::get('idnoticia');	
		foreach($idnoticias as $idnoticia){
			if($idnoticia <> 0){
				$local = new GaleriaLocal();

				$local->idnoticia = $idnoticia;
				$local->idgaleria = $id;

				$local->save();
			}
		}

		Session::flash('success','Registro atualizado com sucesso!');
		return Redirect::to('adminGallery');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Galeria::where('id','=',$id)->delete();
		GaleriaLocal::where('idgaleria','=',$id)->delete();

		// Excluindo Imagens relacionadas

			$destino = base_path().'/assets/img/galeria/';

			$imagens = DB::select("Select * from tb_imagens where idgaleria = $id");

			foreach($imagens as $imagem){
				$nome = $imagem->link;

				//Excluindo a foto da pasta de origem
				unlink($destino.$nome);
			}

			Imagem::where('idgaleria','=',$id)->delete();

		// Excluindo Videos Relacionados

			Video::where('idgaleria','=',$id)->delete();

		Session::flash('success','Galeria excluída com sucesso!');
		return Redirect::to('adminGallery');
	}


}
