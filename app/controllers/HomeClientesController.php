<?php

class HomeClientesController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug)
	{
		$ididioma = Session::get('idioma');

		if($ididioma == 1){
			$pagina = DB::select("Select a.*,c.arquivo from tb_paginas a left join tb_paginas b on a.id = b.idtraducao
								  left join tb_files c on a.idfile = c.id
								  where b.slug = '$slug' and a.ididioma = $ididioma");


			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('clientes/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$clientes = DB::select("Select a.*,b.arquivo from tb_clientes a left join tb_files b on
										a.idfile = b.id where status = 1 and ididioma = 1 and visibilidade = 1
										order by ordem");
			}

		}else{

			$pagina = DB::select("Select b.*, c.arquivo from tb_paginas a left join tb_paginas b 
								  on a.id = b.idtraducao left join tb_files c on b.idfile = c.id
								  where a.slug = '$slug' and b.ididioma = $ididioma");

			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('clients/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$clientes = DB::select("Select a.*,b.arquivo from tb_clientes a left join tb_files b on
										a.idfile = b.id where status = 1 and ididioma = 2 and visibilidade = 1
										order by ordem");
			}

			if($pagina[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}

		}

		if($pagina){

			$data = array(
				'pagina' 	=> $pagina,
				'clientes'	=> $clientes
			);

			Session::put('title',$pagina[0]->titulo);

			$this->layout->content = View::make('home.clientes')->with($data);

		}else{
			return Redirect::to('/');
		}
	}

}
