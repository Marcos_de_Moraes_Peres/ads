<?php

class SlideController extends \BaseController {


protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$slides = DB::table('tb_slides as a')
		->leftjoin('tb_idiomas as b','a.ididioma','=','b.id')
		->select(array('a.*'))
		->where('a.idtraducao','=',null)
		->orderBy('a.status','DESC')
		->orderBy('a.ididioma','ASC')
		->paginate(15);

		$bandeiras = DB::select("Select * from tb_slides where ididioma <> 1 order by ididioma");

		$data = array(
			'slides' 	=> $slides,
			'bandeiras'	=> $bandeiras
		);

		$this->layout->content = View::make('list.slide')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('create.slide');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$slide = new Slide();
		$datafiles = new Files();

		$slide->titulo = Input::get('titulo');
		$slide->descricao = Input::get('descricao');
		$slide->link = Input::get('link');
		$slide->status = Input::get('status');
		$slide->usuario = Auth::user()->nome;
		$slide->ididioma = 1;
		$slide->ordem = 9999;

		$file = Input::file('arquivo');

		$idfile = 0;

		if($file){

			$destinationPath = base_path().'/assets/img/slides/';
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

			$slide->idfile = $datafiles->id;

		}

		$slide->save();

		$id = $slide->id;

		if(Input::get('descricaoEn')){
			$slide = new Slide();

			$slide->titulo = Input::get('titulo');
			$slide->descricao = Input::get('descricaoEn');
			$slide->link = Input::get('link');
			$slide->status = Input::get('status');
			$slide->ididioma = 2;
			$slide->ordem = 9999;
			$slide->idtraducao = $id;
			$slide->idfile = $idfile;

			$slide->save();
		}

		Session::flash('success','Slide cadastrado com sucesso!');
		return Redirect::to('adminSlide');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$slide = DB::select("Select a.* from tb_slides a where id = $id");

		$slideEn = DB::select("Select * from tb_slides where idtraducao = $id and ididioma = 2");

		$data = array(
			'slide'		=> $slide,
			'slideEn'	=> $slideEn,
		);

		$this->layout->content = View::make('edit.slide')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$file = Input::file('arquivo');

		if($file){
			$datafiles = new Files();

			$destinationPath = base_path().'/assets/img/slides/';
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$data = array(
				'titulo' => Input::get('titulo'),
				'descricao' => Input::get('descricao'),
				'link' => Input::get('link'),
				'status' => Input::get('status'),
				'usuario' => Auth::user()->nome,
				'ididioma' => 1,
				'idfile' => $datafiles->id
			);
		}else{
			$data = array(
				'titulo' => Input::get('titulo'),
				'descricao' => Input::get('descricao'),
				'link' => Input::get('link'),
				'usuario' => Auth::user()->nome,
				'status' => Input::get('status'),
				'ididioma' => 1
			);
		}

		Slide::where('id','=',$id)->update($data);

		if($banner = DB::select("Select * from tb_slides where id = $id")){

			if(Input::get('descricaoEn')){
				$idEn = DB::select("Select * from tb_slides where idtraducao = $id and ididioma = 2");
				$nomeEn = Input::get('titulo');
				$descricaoEn = Input::get('descricaoEn');
				$idfile = $banner[0]->idfile;
				$ordem = $banner[0]->ordem;

				if(isset($idEn[0]->id)){
					DB::update("Update tb_slides set titulo = ?, descricao = ?, idfile = ?, link = ?
								where idtraducao = $id and ididioma = 2",array($nomeEn,$descricaoEn,$idfile,Input::get('link')));
				}else{
					$slide = new Slide();

					$slide->titulo = Input::get('titulo');
					$slide->descricao = Input::get('descricaoEn');
					$slide->link = Input::get('link');
					$slide->status = Input::get('status');
					$slide->ididioma = 2;
					$slide->ordem = $ordem;
					$slide->idtraducao = $id;
					$slide->idfile = $idfile;

					$slide->save();
				}
			}else{
				DB::delete("delete from tb_slides where idtraducao = $id and ididioma = 2");
			}

		}

		Session::flash('success',"Slide Atualizado com sucesso!");
		return Redirect::to('adminSlide');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Slide::where('id','=',$id)->delete();
		Slide::where('idtraducao','=',$id)->delete();

		Session::flash('success',"Slide excluído com sucesso!");
		return Redirect::to('adminSlide');
	}

	public function getOrder(){
		$slides = DB::select("Select a.* from tb_slides a where status = 1 and ididioma = 1 order by ordem");

		$this->layout->content = View::make('order.slide')->with('slides',$slides);
	}

	public function postFilter(){
		$datas = Input::all();

		array_shift($datas);

		foreach($datas as $key => $data){
			$datafile = array(
				'ordem' => $key+1
			);

			Slide::where('id','=',$data)->update($datafile);
			Slide::where('idtraducao','=',$data)->update($datafile);
		}

		Session::flash('success',"Registros ordenados com sucesso!");
		return Redirect::to('adminSlides/order');

	}

	public function getFiltro(){
		$status = Input::get('status');
		$titulo = Input::get('titulo');


		if($titulo){
			$qTitulo = "titulo like '%$titulo%' and";
		}else{
			$qTitulo = '';
		}

		if($status == 2){
			$qStatus = '';
		}else{
			$qStatus = "status = $status";
		}

		$slides = DB::select("Select * from tb_slides where idtraducao = null and $qTitulo and $qStatus ");

		$bandeiras = DB::select("Select * from tb_slides where ididioma <> 1 order by ididioma");

		$data = array(
			'status' 	=> $status,
			'titulo'	=> $titulo,
			'slides' 	=> $slides,
			'bandeiras'	=> $bandeiras
		);

		$this->layout->content = View::make('list.slide')->with($data);
	}


}
