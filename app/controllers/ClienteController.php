<?php

class ClienteController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$clientes = DB::table('tb_clientes as a')
		->select(array('a.*'))
		->where('a.ididioma','=',1)
		->where('a.id','<>',51)
		->orderBy('a.status','DESC')
		->get();

		$bandeiras = DB::select("select * from tb_clientes where ididioma = 2");

		$data = array(
			'clientes'	=> $clientes,
			'bandeiras'	=> $bandeiras
		);

		$this->layout->content = View::make('list.cliente')->with($data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content =  View::make('create.cliente');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$cliente = new Cliente();

		$cliente->nome = Input::get('titulo');
		$cliente->ididioma = 1;
		$cliente->slug = Input::get('slug');
		$cliente->status = Input::get('status');
		$cliente->site = Input::get('site');
		$cliente->atendimento = Input::get('atendimento');
		$cliente->visibilidade = 1;
		$cliente->usuario = Auth::user()->nome;
		$cliente->ordem = 9999;
		$cliente->conteudo = Input::get('conteudo');

		$file = Input::file('arquivo');
		$destino = base_path().'/assets/img/clientes/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

			$cliente->idfile = $idfile;

		}

		$cliente->save();

		$id = $cliente->id;

		if(Input::get('tituloEn')){
			$cliente = new Cliente();

			$cliente->nome = Input::get('tituloEn');
			$cliente->ididioma = 2;
			$cliente->slug = Input::get('slugEn');
			$cliente->status = Input::get('status');
			$cliente->site = Input::get('site');
			$cliente->atendimento = Input::get('atendimento');
			$cliente->visibilidade = 1;
			$cliente->ordem = 9999;
			$cliente->conteudo = Input::get('conteudoEn');
			$cliente->idtraducao = $id;

			if($file){
				$cliente->idfile = $idfile;
			}

			$cliente->save();
		}

		Session::flash('success',"Cliente cadastrado com sucesso!");
		return Redirect::to('adminCliente');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$cliente = DB::select("Select * from tb_clientes where id = $id");

		$clienteEn = DB::select("Select * from tb_clientes where idtraducao = $id");

		$data = array(
			'clienteEn'	=> $clienteEn,
			'cliente' 	=> $cliente
		);

		$this->layout->content = View::make('edit.cliente')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$file = Input::file('arquivo');
		$destino = base_path().'/assets/img/clientes/';

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

		}

		if($file){
			$data = array(
				'nome' 	  	  	=> Input::get('titulo'),
				'ididioma' 	  	=> 1,
				'status'	  	=> Input::get('status'),		
				'conteudo' 	  	=> Input::get('conteudo'),
				'slug' 	 	  	=> Input::get('slug'),
				'site' 	 	  	=> Input::get('site'),
				'atendimento'  	=> Input::get('atendimento'),
				'usuario' 		=> Auth::user()->nome,
				'idfile'	  	=> $idfile
			);
		}else{
			$data = array(
				'nome' 	  	 	=> Input::get('titulo'),
				'ididioma' 	 	=> 1,
				'status'	 	=> Input::get('status'),	
				'conteudo' 	  	=> Input::get('conteudo'),
				'site' 	 	  	=> Input::get('site'),
				'atendimento'  	=> Input::get('atendimento'),
				'usuario'		=> Auth::user()->nome,
				'slug' 	 	 	=> Input::get('slug')
			);
		}

		Cliente::where('id','=',$id)->update($data);

		if(Input::get('tituloEn')){
			$idEn = DB::select("Select * from tb_clientes where idtraducao = $id");
			$content = DB::select("Select * from tb_clientes where id = $id");
			$nomeEn = Input::get('tituloEn');
			$conteudoEn = Input::get('conteudoEn');
			$slugEn = Input::get('slugEn');

			if(isset($idEn[0]->id)){
				$visibi = $idEn[0]->visibilidade;
				if($file){
					DB::update("Update tb_clientes set nome = ?, conteudo = ?, idfile = ?, slug = ?, visibilidade = ?
								where idtraducao = $id",
								array($nomeEn,$resumoEn,$conteudoEn,$idfile,$slugEn,$visibi));
				}else{
					DB::update("Update tb_clientes set nome = ?, conteudo = ?, slug = ?, visibilidade = ?
								where idtraducao = $id",
								array($nomeEn,$conteudoEn,$slugEn,$visibi));
				}
			}else{
				$cliente = new Cliente();

				$cliente->nome = Input::get('tituloEn');
				$cliente->ididioma = 2;
				$cliente->slug = Input::get('slugEn');
				$cliente->conteudo = Input::get('conteudoEn');
				$cliente->atendimento = Input::get('atendimento');
				$cliente->site = Input::get('site');
				$cliente->idtraducao = $id;
				$cliente->ordem = $content[0]->ordem;

				if($file){
					$cliente->idfile = $idfile;
				}else{
					if($idRet = DB::select("Select idfile from tb_clientes where id = $id")){
						$cliente->idfile = $idRet[0]->idfile;
					}
				}

				$cliente->save();
			}
		}else{
			DB::delete("delete from tb_clientes where idtraducao = $id");
		}

		Session::flash('success',"Cliente atualizado com sucesso!");
		return Redirect::to('adminCliente');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Cliente::where('id','=',$id)->delete();
		Cliente::where('idtraducao','=',$id)->delete();

		Session::flash('success','Cliente excluído com sucesso!');
		return Redirect::to('adminCliente');
	}

	public function getOrder(){
		$clientes = DB::select("Select * from tb_clientes where status = 1 and id <> 51 and ididioma = 1 order by ordem");

		$this->layout->content = View::make('order.cliente')->with('clientes',$clientes);
	}

	public function postFilter(){
		$datas = Input::all();

		array_shift($datas);

		foreach($datas as $key => $data){
			$datafile = array(
				'ordem' => $key+1
			);

			Cliente::where('id','=',$data)->update($datafile);
			Cliente::where('idtraducao','=',$data)->update($datafile);
		}


		Session::flash('success',"Registros ordenados com sucesso!");
		return Redirect::to('adminClientes/order');

	}

}
