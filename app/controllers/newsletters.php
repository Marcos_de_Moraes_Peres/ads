<?php 

include('home.php');

class Newsletters extends Application{

	public function index_action(){

		Application::check();

		$this->load(array('Newsletter'));

		$data['newsletters'] = Newsletter::all('ml_newsletters', 'order by created DESC');
		
		#array('conditions' => array('month(created) =?', date('m')), 'order' => 'created DESC')

		View::display('newsletters/index', $data);
	}

	/*public function show(){
		
		Application::check();

		View::display('users/show');
	}*/

	public function previews(){

		$this->load(array('Newsletter'));

		$id = $this->getParam('id');

		$data['newsletter']		= Newsletter::find('ml_newsletters', $id);
		
		$data['news']		= $this->getNewsNew($id);
		$data['events']		= $this->getNewsEvent($id);
		$data['birthdays']	= $this->getNewsBirthday($id);

		View::display('newsletters/preview', $data, 'public');

	}

	public function create(){

		Application::check();

		$data['newsletter'] = $this->startNewsleter();

		Controller::redirect('newsletters/generate/id/' . $data['newsletter']->id);
	}

	public function generate(){
		
		Application::check();

		$this->load(array('Newsletter', 'Headline', 'Event'));

		$id = $this->getParam('id');

		$data['newsletter']		= Newsletter::find('ml_newsletters', $id);
		
		$data['news_new']		= $this->getNewsNew($id);
		$data['news_event']		= $this->getNewsEvent($id);
		$data['news_birthday']	= $this->getNewsBirthday($id);

		$data['news'] 		= Headline::find_by_sql('select top 10 * from ml_news order by created desc');
		$data['events'] 	= Headline::find_by_sql('select top 10 * from ml_events order by created desc');

		View::display('newsletters/new', $data);
	
	}

	public function update(){

		Application::check();

		$data 	= $this->getAttributes();
		$rel 	= $this->getRelationships();
	
		$newsletter 	= $this->setAttributes($data);
		$relationships 	= $this->setRelationships($data['id'], $rel);

		Controller::redirect("newsletters/generate/id/" . $data['id']);

	}

	public function delete(){
		
		Application::check();
	
		$this->load(array('Newsletter'));

		$id = $this->getParam('id');

		if($newsletter = Newsletter::find('ml_newsletters', $id)){
			$newsletter_delete = Newsletter::find_by_sql('delete from ml_newsletters where id = ' . $newsletter->id);

			Controller::redirect("newsletters/");
		}
		
	}

	public function remove(){

		$paramns = $this->getParam();

		switch ($paramns['tipo']) {
			case 'evento':
		
					$this->removeEvent($paramns['newsletter'], $paramns['id']);

					Controller::redirect("newsletters/generate/id/". $paramns['newsletter']);

				break;
			
			case 'noticia':

					$this->removeNew($paramns['newsletter'], $paramns['id']);					
					
					Controller::redirect("newsletters/generate/id/". $paramns['newsletter']);

				break;

			case 'aniversario':

					$this->removeBirthday($paramns['newsletter'], $paramns['id']);
					
					Controller::redirect("newsletters/generate/id/". $paramns['newsletter']);

				break;

			default:
				
				Controller::redirect("newsletters/generate/id/". $paramns['newsletter']);

				break;
		}
	}

	public function search(){
		
		$m 	= $this->postParam('month');
		$y	= $this->postParam('year');

		$data['newsletters'] = Newsletter::find('all', array('conditions' => array('substring(date, 4,2) =? AND substring(date, 7,4) =? ', $m, $y)));

		View::display('newsletters/search', $data);
	}

	protected function startNewsleter(){

		$this->load(array('Newsletter'));

		$attr = array(
			'status' 	=> 'em produção',
			'date' 		=> date('d/m/Y'),
			);
		
		$data['newsletter'] = Newsletter::create('ml_newsletters', $attr);

		$data['birthdays'] = $this->setBirthdaysToday($data['newsletter']->id);

		return $data['newsletter'];
	}

	protected function getAttributes(){

		$this->load(array('Newsletter'));

		$id = $this->postParam('id');

		$noticia = Newsletter::find_by_sql("select * from ml_newsletters where id = $id");

		if($noticia[0]->edition <> null){
			$edition = $noticia[0]->edition;
		}else{
			$edition = Newsletter::find_by_sql('select top 1 edition from ml_newsletters order by edition desc');

			$edition = (int)str_replace('.','',$edition[0]->edition) + 1;
		}

		$data = array( 
			'id'				=> $this->postParam('id'),
			'featured'			=> $this->postParam('featured'),
			'year'				=> $this->postParam('year'),
			'edition'			=> $edition,
			'date'				=> $this->postParam('date')
			#'status'			=> $this->postParam('status')
		);

		foreach ($data as $key => $value) {
			
			if(!is_null($value) && isset($value) && !empty($value)){

				$respond[$key] = $value;
			}

		}

		return $respond;
	}

	protected function getRelationships(){

		$rel = array(
			'new'				=> $this->postParam('new_id'),
			'event'				=> $this->postParam('event_id'),
			'birthday'			=> $this->postParam('birthday')
		);

		return $rel;
	}

	protected function setAttributes($data){

		$this->load(array('Newsletter'));

		foreach ($data as $key => $value) {
			$attr[$key] = $value;			
		}
		
		array_shift($attr);

		$update['newsletter'] = Newsletter::update_attributes('ml_newsletters', $data['id'], $attr);
		
		return $update;
	}

	public function setRelationships($id, $data){

		$rel['new'] 		= $this->setNew($id, $data['new']);
		$rel['event']		= $this->setEvent($id, $data['event']);
		$rel['birthday']	= $this->setBirthdays($id, $data['birthday']);

		return $rel;
	}

	protected function setNew($newsletter_id, $new){

		$this->load(array('NewsNew'));

		$attr = array(
			'new_id' 			=> $new,
			'newsletter_id'		=> $newsletter_id,
			);

		$data['new'] = NewsNew::create('ml_news_new', $attr);

		return $data['new'];
	}

	protected function setEvent($newsletter_id, $event){

		$this->load(array('NewsEvent'));

		$attr = array(
			'event_id' 			=> $event,
			'newsletter_id'		=> $newsletter_id,
			);

		$data['event'] =  NewsEvent::create('ml_news_events', $attr);

		return $data['event'];

	}

	protected function setBirthdays($newsletter_id, $birthdayDate){
		
		$this->load(array('Birthday', 'NewsBirthday'));

		$data['news_birthday'] 		= Birthday::all('ml_birthdays', 'where born_in ="' . $birthdayDate . '"');
	
		foreach ($data['news_birthday'] as $birthday) {

			$attr = array(
				'birthday_id' 		=> $birthday->id,
				'newsletter_id'		=> $newsletter_id,
				);

			$data['birthday'] = NewsBirthday::create('ml_news_birthdays', $attr);

		}
		
		return true;
	}

	protected function setBirthdaysToday($newsletter_id){

		$this->load(array('Birthday', 'NewsBirthday'));

		$data['today'] = Birthday::birthdayToday();

		foreach ($data['today'] as $birthday) {

			$attr = array(
				'birthday_id' 	=> $birthday->id,
				'newsletter_id' => $newsletter_id,
				);

			$add = NewsBirthday::create('ml_news_birthdays', $attr);
			
		}

		return true;
	}

	protected function removeNew($newsletter_id, $new_id){

		$this->load(array('NewsNew'));

		$new = NewsNew::find('ml_news_new', null, 'where newsletter_id = '. $newsletter_id .' AND new_id =' . $new_id);
	
		$new_delete = NewsNew::find_by_sql('delete from ml_news_new where id = ' . $new->id);

		return true;
	}

	protected function removeEvent($newsletter_id, $event_id){
		
		$this->load(array('NewsEvent'));

		$event = NewsEvent::find('ml_news_events', null, ' where newsletter_id ='. $newsletter_id .' AND event_id = ' . $event_id);
		
		$event_delete = NewsEvent::find_by_sql('delete from ml_news_events where id = ' . $event->id);

		return true;
	}

	protected function removeBirthday($newsletter_id, $birthday_id){
		
		$this->load(array('NewsBirthday'));

		$birthday = NewsBirthday::find('ml_news_birthdays', null, 'where newsletter_id = '. $newsletter_id .' AND birthday_id = ' . $birthday_id);
		
		$birthday_delete = NewsBirthday::find_by_sql('delete from ml_news_birthdays where id = '. $birthday->id);

		return true;
	}

	public function sendMail(){	

		$this->load(array('Newsletter'));

		$newsletter 	= Newsletter::find('ml_newsletters', $this->postParam('id'));
		$news			= $this->getNewsNew($newsletter->id);
		$events			= $this->getNewsEvent($newsletter->id);
		$birthdays		= $this->getNewsBirthday($newsletter->id);

		$header .= "Content-Type: text/html;";
		#$header .= "charset=iso-8859-1\n";

		$content .= '<table border="0" cellpadding="0" cellspacing="0" width="750" align="center" style="font-family:Verdana; font-size:12px; line-height:18px; text-align:justify">';
		$content .= "<tr>"; 
		$content .= "<td colspan='2'>" . View::image("template_boletim/topo.jpg", "style='display:block'") . "</td>";
		$content .= "</tr>";
		$content .= "<tr style='background-color:#94ae47;color:#fff;'>";
		$content .= "<td style='padding-left:20px;'>Boletim eletrônico diário da UN Leste</td>";
		
		$content .= "<td style='padding-right:20px;text-align:right;'>Ano $newsletter->year - edição nº $newsletter->edition de $newsletter->date </td>";
		
		$content .= '</tr>';
		$content .=	'</table>';
		$content .= '<br />';

		$content .= '<table border="0" cellpadding="0" cellspacing="0" width="750" align="center" style="font-family:Verdana; font-size:12px; line-height:18px; text-align:justify">';
		$content .= '<td colspan="3" style="font-style:italic; text-align:right; padding-bottom:30px; font-size:12px;">';
		$content .= '</td>';
		
		$content .= '<tr>';

		$content .= '<td valign="top">';
		$content .= '<table border="0" cellpadding="0" cellspacing="0" width="530" style="font-family:Verdana; font-size:12px; line-height:18px; text-align:justify;"> ';

		if(isset($news)) {
			foreach ($news as $new) {
				
				$content .= '<tr style="margin-bottom:10px;">';
					$content .= '<td width="150" valign="top">' . View::image("noticias/$new->image", "style='display:block'") . '</td>';
					$content .= '<td style="padding-left:10px; width:370px;" valign="top">';
						$content .= View::link('newsletters/remove/tipo/noticia/newsletter/'.$newsletter->id.'/id/'. $new->id, '<i class="icon-remove"></i>'); 
						$content .= '<h2 style="font-size:12px; line-height:14px; color:#b9d75e; margin:0; border-bottom:1px dotted #b9d75e; text-align:left;">' .  $new->section . '</h2>';						
						$content .= '<h1 style="font-size:16px; line-height:22px; color:#02588b; margin:0; font-weight:bold; text-align:left;">' . $new->title . '</h1>';
						$content .= '<p>' . $new->featured . '</p>';						
						$content .= '<a href="http://'. $_SERVER['HTTP_HOST']. '/mlinforma-sql/home/news/newsletter/'.$newsletter->id.'/news/'.$new->id.'" target="_blank" style="padding:3px 5px; background-color:#ff8d33; color:#fff; font-weight:bold; text-decoration:none">Leia mais </a> </td>';						  
				$content .= '</tr>';

				$content .= '<tr>';
				$content .= '<td colspan="2" style="padding:20px 0;"></td>';
				$content .= '</tr>';

			}
		}

		$content .= '</table>';
		$content .= '</td>';		

		$content .= '<td width="20"></td>';
		$content .= '<td valign="top" style="background-color:#f5f2d6; width:180px; padding:10px; border-bottom:15px solid #fff; text-align:left; font-size:11px; line-height:16px;">';
	 	$content .= '<h1 style="font-size:16px; line-height:22px; color:#02588b; margin:0; font-weight:bold; text-align:left;">Agenda de <br />eventos ML</h1>';
	    
	    if(isset($events)){
		    foreach ($events as $event){				
		        $content .= "<p><strong> $event->title</strong><br />";
			    $content .= "<span style='color:#dd7828;'>Data:</span>"; 
							if($event->finish != " " && isset($event->finish) && !empty($event->finish)) { 
								$content .= "".$event->beginning . " a " . $event->finish." <br />";
							} else {
								$content .= $event->beginning . "<br />";
							}
				$content .= '<span style="color:#dd7828;">Hora:</span>';
					        	if($event->time_finish != " " && isset($event->time_finish) && !empty($event->time_finish)){
					        		$content .= substr($event->time_beginning,0,2)."h".substr($event->time_beginning,3,5)." às ".substr($event->time_finish,0,2)."h".substr($event->time_finish,3,5)."<br />"; 
					        	}else{ 
					        		$content .= substr($event->time_beginning,0,2)."h".substr($event->time_beginning,3,5) . "<br />";
					        	}
		        $content .= "<span style='color:#dd7828;'>Local:</span> $event->location  <br />";
		        $content .= "<span style='color:#dd7828;'>Participantes:</span> $event->participants</p>";
			}
		}

	    $content .= '</td>';
		$content .= '</tr>';

		$content .= '<tr>';
		$content .= '<td colspan="3">';
	    $content .= '<br /><br />';
		$content .= '<h1 style="font-size:16px; line-height:22px; color:#02588b; border-top: 1px solid #02588b; padding:5px 0 0 0; text-transform:uppercase;">Aniversariantes</h1>';
	      
	    if(isset($birthdays)){
		    foreach ($birthdays as $birthday) {
			    $content .= '<div style="width:22%; text-align:center; padding:0 10px;font-size:10px; line-height:12px; float:left; margin-top:10px;">';
			    $content .= View::link('newsletters/remove/tipo/aniversario/newsletter/'.$newsletter->id.'/id/'. $birthday->id, '<i class="icon-remove"></i>');
			    $content .= '<center>';
			    $content .= View::image("aniversariantes/$birthday->thumb", "style='display:block' width='90'");
			    $content .= '</center>';
			    $content .= '<br />';
			    $content .= $birthday->name.' - '.$birthday->unit.' - '.$birthday->born_in;
			    $content .= '</div>';
			}		
		}	
	         
	    $content .= '</td>';
		$content .= '</tr>';

		$content .= "</table>";
		$content .= "<br /><br />";
		$content .= '<table border="0" cellpadding="0" cellspacing="0" width="750" align="center" style="font-family:Verdana; font-size:12px; line-height:18px; text-align:justify">';
			$content .= '<tr style="background-color:#94ae47;color:#fff;">';
				$content .= '<td style="padding-left:20px;">Polo de Comunicação Leste</td>';
				$content .= '<td  style="padding-right:20px;text-align:right;">Envie sugestões de pauta no e-mail <a href="mailto:comunicacaoml@sabesp.com.br" style="padding:0px; background-color: transparent; color:#fff; font-weight:normal; text-decoration:none;font-size:12px;">comunicacaoml@sabesp.com.br</a></td>';
			$content .= '</tr> ';
				$content .= '<td colspan="2">';
				$content .= View::image("template_boletim/rodape.jpg", "style='display:block'");
				$content .= '</td>'; 
		$content .= '</table>';


		$assunto = 'ML Informa - Ano ' . $newsletter->year . ' - Edição nº ' . $newsletter->edition . ' de ' . $newsletter->date;

	    mail($this->postParam('email'), utf8_decode($assunto), utf8_decode($content), $header, "-r".'marcos@neopixdesign.com.br');

	    echo "<script type='text/java'>alert('Mensagem enviada com sucesso');</script>";
		Controller::redirect("newsletters/generate/id/" . $this->postParam('id'));
	}

}	