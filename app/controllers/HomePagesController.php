<?php

class HomePagesController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug){
		
		$ididioma = Session::get('idioma');

		if($ididioma == 1){
			$pagina = DB::select("Select a.* from tb_paginas a left join tb_paginas b on a.id = b.idtraducao
								  where b.slug = '$slug' and a.ididioma = $ididioma");


			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('pagina/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo, d.titulo as menu from tb_paginas a left join tb_files b on
									  a.idfile = b.id left join tb_categorias c on a.idcategoria = c.id
									  left join tb_secoes d on c.idsecao = d.id where a.slug = '$slug'");
			}

		}else{

			$pagina = DB::select("Select b.* from tb_paginas a left join tb_paginas b 
								  on a.id = b.idtraducao where a.slug = '$slug' and b.ididioma = $ididioma");

			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('page/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo, d.titulo as menu from tb_paginas a left join tb_files b on
									  a.idfile = b.id left join tb_categorias c on a.idcategoria = c.idtraducao
									  left join tb_secoes d on c.idsecao = d.idtraducao where a.slug = '$slug'");

			}

			if($pagina[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}

		}

		if($pagina){

			$data = array(
				'pagina' 		=> $pagina
			);

			Session::put('title',$pagina[0]->titulo);

			$this->layout->content = View::make('home.paginas')->with($data);

		}else{
			return Redirect::to('/');
		}

	}

}
