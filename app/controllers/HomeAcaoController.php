<?php

class HomeAcaoController extends \BaseController {

	protected $layout = 'layouts.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($slug)
	{
		$bolhetins = DB::select('Select a.*, b.arquivo from tb_bolhetim a left join tb_files b on a.idfile = b.id
								 order by created_at');

		$ididioma = Session::get('idioma');

		if($ididioma == 1){
			$pagina = DB::select("Select a.* from tb_paginas a left join tb_paginas b on a.id = b.idtraducao
								  where b.slug = '$slug' and a.ididioma = $ididioma");


			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('ads-em-acao/'.$pagina[0]->slug);
			}else{
				$bolhetins = DB::select('Select a.*, b.arquivo from tb_bolhetim a left join tb_files b on a.idfile = b.id
								         where a.status = 1 order by created_at');				
			}
		}else{

			$pagina = DB::select("Select b.* from tb_paginas a left join tb_paginas b 
								  on a.id = b.idtraducao where a.slug = '$slug' and b.ididioma = $ididioma");


			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('ads-in-action/'.$pagina[0]->slug);
			}else{
				$bolhetins = DB::select('Select a.*, b.arquivo from tb_bolhetim a left join tb_files b on a.idfile = b.id
								 		 where a.status = 1 order by created_at');				
			}

		}

		$this->layout->content = View::make('home.bolhetim')->with('bolhetins',$bolhetins);
	}


}
