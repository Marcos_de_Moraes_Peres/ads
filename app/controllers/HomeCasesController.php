<?php

class HomeCasesController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug)
	{
		$ididioma = Session::get('idioma');

		if($ididioma == 1){
			$pagina = DB::select("Select a.* from tb_paginas a left join tb_paginas b on a.id = b.idtraducao
								  where b.slug = '$slug' and a.ididioma = $ididioma");


			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('cases/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$cases = DB::select("Select a.*, b.arquivo, c.titulo as servico from tb_cases a left join tb_files b on
									 a.idfile = b.id left join tb_servicos c on a.idservico = c.id where a.ididioma = 1
									 and a.status = 1 order by a.ordem asc");
			}

		}else{

			$pagina = DB::select("Select b.* from tb_paginas a left join tb_paginas b 
								  on a.id = b.idtraducao where a.slug = '$slug' and b.ididioma = $ididioma");

			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('cases/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$cases = DB::select("Select a.*, b.arquivo, c.titulo as servico from tb_cases a left join tb_files b on
									 a.idfile = b.id left join tb_servicos c on a.idservico = c.idtraducao where a.ididioma = 2
									 and a.status = 1 order by a.ordem asc");

			}

			if($pagina[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}

		}

		if($pagina){

			$data = array(
				'pagina'	=> $pagina,
				'cases'		=> $cases
			);

			Session::put('title',$pagina[0]->titulo);

			$this->layout->content = View::make('home.cases')->with($data);

		}else{
			return Redirect::to('/');
		}
	}


}
