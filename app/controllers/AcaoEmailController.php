<?php

class AcaoEmailController extends \BaseController {

	protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$id = Input::get('idboletim');
		$email = Input::get('email');

		$boletim = Bolhetim::where('id','=',$id)->get();

		$noticia1 = DB::select("Select b.*, c.arquivo from tb_bolhetim a left join tb_acao_noticias b on a.idnoticia1 = b.id
								left join tb_files c on b.idfile = c.id where a.id = '$id'");

		$noticia2 = DB::select("Select b.*, c.arquivo from tb_bolhetim a left join tb_acao_noticias b on a.idnoticia2 = b.id
								left join tb_files c on b.idfile = c.id where a.id = '$id'");

		$noticia3 = DB::select("Select b.*, c.arquivo from tb_bolhetim a left join tb_acao_noticias b on a.idnoticia3 = b.id
								left join tb_files c on b.idfile = c.id where a.id = '$id'");

		$noticia4 = DB::select("Select b.*, c.arquivo from tb_bolhetim a left join tb_acao_noticias b on a.idnoticia4 = b.id
								left join tb_files c on b.idfile = c.id where a.id = '$id'");

		$data = array(
			'noticia1'	=> $noticia1,
			'noticia2'	=> $noticia2,
			'noticia3'	=> $noticia3,
			'noticia4'	=> $noticia4,
			'boletim'	=> $boletim
		);

		Mail::send('emails.acao', $data, function($message) use($email)
		{
			$message->to($email)->subject('Ads em Ação - Site ADS');
		});

		Session::flash('success', "Email enviado com sucesso!");
		return Redirect::to(URL::previous());

	}

}
