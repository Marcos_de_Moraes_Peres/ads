<?php

class HomeController extends BaseController {

protected $layout = 'layouts.master';

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{

		$idioma = Session::get('idioma');
		$campo 	= Session::get('campo');

		$date = date('Y-m-d');

		$slides = DB::select("Select a.$campo as id, a.descricao, a.link,b.arquivo 
							  from tb_slides a left join tb_files b on a.idfile = b.id 
							  where a.status = 1 and a.ididioma = $idioma order by a.ordem");

		$destaqueFixo = DB::select("Select a.*,b.arquivo from tb_destaques a  left join tb_files b on 
								    a.idfile = b.id where a.ididioma = $idioma and status = 1
								    and a.tipo = 2");

		$destaqueAleatorio = DB::select("Select a.*,b.arquivo from tb_destaques a  
										left join tb_files b on a.idfile = b.id where 
										a.ididioma = $idioma and status = 1 and a.tipo = 1
										order by rand() limit 1");

		$noticias = DB::select("Select a.*, b.slug as url from tb_noticias a left join tb_clientes b on
								a.idcliente = b.$campo where a.status = 1 and a.ididioma = $idioma 
								and a.publicacao <= '$date' order by a.publicacao desc limit 4");

		Session::put('title', 'Home');

		$data = array(
			'slides' 			=> $slides,
			'destaqueFixo'		=> $destaqueFixo,
			'destaqueAleatorio'	=> $destaqueAleatorio,
			'noticias'			=> $noticias
		);

		$this->layout->content = View::make('home.index')->with($data);
	}

}
