<?php

class HomeDepoimentoController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug)
	{
		$ididioma = Session::get('idioma');

		if($ididioma == 1){
			$pagina = DB::select("Select a.* from tb_paginas a left join tb_paginas b on a.id = b.idtraducao
								  where b.slug = '$slug' and a.ididioma = $ididioma");


			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('depoimentos/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$depoimentos = DB::table('tb_depoimentos as a')
				->leftjoin('tb_clientes as b','a.idcliente','=','b.id')
				->leftjoin('tb_files as c','b.idfile','=','c.id')
				->leftjoin('tb_files as d','a.idfile','=','d.id')
				->select(array('a.*','b.nome as cliente','c.arquivo','d.arquivo as imagem'))
				->where('a.ididioma','=',1)
				->where('a.status','=',1)
				->orderBy('a.ordem','ASC')
				->paginate(20);
			}

		}else{

			$pagina = DB::select("Select b.* from tb_paginas a left join tb_paginas b 
								  on a.id = b.idtraducao where a.slug = '$slug' and b.ididioma = $ididioma");

			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('testimonials/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$depoimentos = DB::table('tb_depoimentos as a')
				->leftjoin('tb_clientes as b','a.idcliente','=','b.id')
				->leftjoin('tb_files as c','b.idfile','=','c.id')
				->leftjoin('tb_files as d','a.idfile','=','d.id')
				->select(array('a.*','b.nome as cliente','c.arquivo','d.arquivo as imagem'))
				->where('a.ididioma','=',2)
				->where('a.status','=',1)
				->orderBy('a.ordem','ASC')
				->paginate(20);
			}

			if($pagina[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}

		}

		if($pagina){

			$data = array(
				'pagina' 		=> $pagina,
				'depoimentos'	=> $depoimentos
			);

			Session::put('title',$pagina[0]->titulo);

			$this->layout->content = View::make('home.depoimentos')->with($data);

		}else{
			return Redirect::to('/');
		}
	}


}
