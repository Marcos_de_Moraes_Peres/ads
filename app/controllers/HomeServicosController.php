<?php

class HomeServicosController extends \BaseController {

protected $layout = 'layouts.master';

	public function index($slug)
	{
		$ididioma = Session::get('idioma');

		if($ididioma == 1){
			$pagina = DB::select("Select a.* from tb_paginas a left join tb_paginas b on a.id = b.idtraducao
								  where b.slug = '$slug' and a.ididioma = $ididioma");


			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('servicos/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$servicos = DB::select("Select a.*,b.arquivo from tb_servicos a left join tb_files b on 
										a.idfile = b.id where a.status = 1 and ididioma = 1 order by ordem");
			}

		}else{

			$pagina = DB::select("Select b.* from tb_paginas a left join tb_paginas b 
								  on a.id = b.idtraducao where a.slug = '$slug' and b.ididioma = $ididioma");

			//Se vier da tradução redireciona
			if($pagina){
				return Redirect::to('services/'.$pagina[0]->slug);
			}else{
				$pagina = DB::select("Select a.*, b.arquivo from tb_paginas a left join tb_files b on
									  a.idfile = b.id where a.slug = '$slug'");

				$servicos = DB::select("Select a.*,b.arquivo from tb_servicos a left join tb_files b on 
										a.idfile = b.id where a.status = 1 and ididioma = 2 order by ordem");

			}

			if($pagina[0]->ididioma <> $ididioma){
				return Redirect::to('/');
			}
		}

		if($pagina){

			$data = array(
				'pagina'	=> $pagina,
				'servicos'	=> $servicos
			);

			Session::put('title',$pagina[0]->titulo);

			$this->layout->content = View::make('home.servicos')->with($data);

		}else{
			return Redirect::to('/');
		}
	}

}
