<?php

class DestaqueController extends \BaseController {

protected $layout = 'layouts.admin.master';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$destaques = DB::table('tb_destaques as a')
		->select(array('a.*'))
		->where('a.ididioma','=',1)
		->orderBy('a.status','DESC')
		->paginate(15);

		$bandeiras = DB::select("Select * from tb_destaques where ididioma = 2");

		$data = array(
			'destaques'	=> $destaques,
			'bandeiras'	=> $bandeiras
		);

		$this->layout->content = View::make('list.destaque')->with($data);
	}

	public function create()
	{
		$this->layout->content = View::make('create.destaque');
	}
	
	public function store()
	{

		$ididioma = Session::get('idioma');

		$datafiles = new Files();

		$file = Input::file('arquivo');

		if(Input::get('tipo') == 2){

			$alter = array(
				'status' => 0
			);

			Destaque::where('tipo','=',2)->update($alter);
		}

		$idfile = 0;

		if($file){

			$destinationPath = base_path().'/assets/img/destaques/';
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

		}

		if($ididioma == 1){

			$destaque = new Destaque();

			$destaque->titulo = Input::get('titulo');
			$destaque->descricao = Input::get('descricao');
			$destaque->link = Input::get('link');
			$destaque->status = Input::get('status');
			$destaque->usuario = Auth::user()->nome;
			$destaque->tipo = Input::get('tipo');
			$destaque->ididioma = 1;

			if($idfile){
				$destaque->idfile = $idfile;
			}

			$destaque->save();

			$idtraducao = $destaque->id;

		}

		if(Input::get('tituloEn')){
			$destaque = new Destaque();

			$destaque->titulo = Input::get('tituloEn');
			$destaque->descricao = Input::get('descricaoEn');
			$destaque->link = Input::get('link');
			$destaque->status = Input::get('status');
			$destaque->tipo = Input::get('tipo');
			$destaque->idtraducao = $idtraducao;
			$destaque->ididioma = 2;

			if($idfile){
				$destaque->idfile = $idfile;
			}

			$destaque->save();
		}
			
			

		Session::flash('success','Destaques atualizados com sucesso!');
		return Redirect::to('adminSpotlight');
	}

	public function edit($id)
	{
		$destaque = DB::select("Select * from tb_destaques where id = $id");

		$destaqueEn = DB::select("Select * from tb_destaques where idtraducao = $id");

		$data = array(
			'destaque'	=> $destaque,
			'destaqueEn'	=> $destaqueEn
		);

		$this->layout->content = View::make('edit.destaque')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$file = Input::file('arquivo');
		$destino = base_path().'/assets/img/destaques/';

		if(Input::get('status') == 1 && Input::get('tipo') == 2){
			Destaque::where('tipo','=',2)->update(array('status' => 0));
		}

		if($file){

			$datafiles = new Files();

			$destinationPath = $destino;
			$filename = str_random(5).$file->getClientOriginalName();

			$datafiles->arquivo = $filename;

			$upload_success = Input::file('arquivo')->move($destinationPath, $filename);

			$datafiles->save();

			$idfile = $datafiles->id;

		}

		if($file){
			$data = array(
				'titulo'	=> Input::get('titulo'),
				'descricao'	=> Input::get('descricao'),
				'link'		=> Input::get('link'),
				'status'	=> Input::get('status'),
				'tipo'		=> Input::get('tipo'),
				'usuario' 	=> Auth::user()->nome,
				'ididioma'	=> 1,
				'idfile'	=> $idfile
			);
		}else{
			$data = array(
				'titulo'	=> Input::get('titulo'),
				'descricao'	=> Input::get('descricao'),
				'link'		=> Input::get('link'),
				'status'	=> Input::get('status'),
				'usuario' 	=> Auth::user()->nome,
				'tipo'		=> Input::get('tipo'),
				'ididioma'	=> 1
			);
		}

		Destaque::where('id','=',$id)->update($data);

		if(Input::get('tituloEn')){
			$idEn = DB::select("Select * from tb_destaques where idtraducao = $id and ididioma = 2");
			$tituloEn = Input::get('tituloEn');
			$descricaoEn = Input::get('descricaoEn');

			if(isset($idEn[0]->id)){
				if($file){
					DB::update("Update tb_destaques set titulo = ?, descricao = ?, idfile = ?, link = ?, status = ?, tipo = ?
								where idtraducao = $id and ididioma = 2",
								array($tituloEn,$descricaoEn,$idfile,Input::get('link'),Input::get('status'),Input::get('tipo')));
				}else{
					DB::update("Update tb_destaques set titulo = ?, descricao = ?, link = ?, status = ?, tipo = ?
								where idtraducao = $id and ididioma = 2",
								array($tituloEn,$descricaoEn,Input::get('link'),Input::get('status'),Input::get('tipo')));
				}
			}else{
				$destaque = new Destaque();

				$destaque->titulo = Input::get('tituloEn');
				$destaque->descricao = Input::get('descricaoEn');
				$destaque->link = Input::get('link');
				$destaque->status = Input::get('status');
				$destaque->tipo = Input::get('tipo');
				$destaque->ididioma = 2;
				$destaque->idtraducao = $id;

				if($file){
					$destaque->idfile = $idfile;
				}else{
					if($idRet = DB::select("Select idfile from tb_destaques where id = $id")){
						$destaque->idfile = $idRet[0]->idfile;
					}
				}

				$destaque->save();
			}
		}else{
			DB::delete("delete from tb_destaques where idtraducao = $id and ididioma = 2");
		}

		Session::flash('success',"Destaque atualizado com sucesso!");
		return Redirect::to('adminSpotlight');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Destaque::where('id','=',$id)->delete();
		Destaque::where('idtraducao','=',$id)->delete();

		Session::flash('success','Registro excluído com sucesso!');
		return Redirect::to('adminSpotlight');
	}

}
