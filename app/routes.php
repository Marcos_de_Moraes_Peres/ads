<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::controller('home', 'HomeController');

Route::resource('page', 'HomePagesController@index');
Route::resource('pagina', 'HomePagesController@index');

Route::resource('clients', 'HomeClientesController@index');
Route::resource('clientes', 'HomeClientesController@index');

Route::resource('client', 'HomeClienteController@index');
Route::resource('cliente', 'HomeClienteController@index');

Route::resource('servico', 'HomeServicoController@index');
Route::resource('service', 'HomeServicoController@index');

Route::resource('servicos', 'HomeServicosController@index');
Route::resource('services', 'HomeServicosController@index');

Route::resource('case', 'HomeCaseController@index');

Route::resource('cases', 'HomeCasesController@index');

Route::resource('testimonials', 'HomeDepoimentoController@index');
Route::resource('depoimentos', 'HomeDepoimentoController@index');

Route::resource('news', 'HomeNoticiasController@index');
Route::resource('noticias', 'HomeNoticiasController@index');

Route::resource('contato', 'HomeContatoController@index');
Route::resource('contact', 'HomeContatoController@index');

Route::resource('ads-em-acao', 'HomeAcaoController@index');
Route::resource('ads-in-action', 'HomeAcaoController@index');

Route::resource('ads-em-acao-show', 'HomeAcaoShowController@index');
Route::resource('ads-in-action-show', 'HomeAcaoShowController@index');

Route::resource('acao-show', 'HomeAcaoShowController@show');

Route::resource('curriculo', 'HomeCurriculoController@index');

Route::resource('news-client', 'HomeNoticiaClienteController@index');
Route::resource('noticias-cliente', 'HomeNoticiaClienteController@index');

Route::resource('new', 'HomeNoticiaController@index');
Route::resource('noticia', 'HomeNoticiaController@index');

//////////////////// Página de erro 404

App::missing(function($exception)
{
    return Redirect::to('erro');
});

Route::resource('erro', 'ErroController');

//////////////////// Fim da Página de erro

//////////////////// Envio de Formulários de Contato

Route::post('formContato', 'HomeContatoController@getFormContato');
Route::post('formCurriculo', 'HomeCurriculoController@getFormCurriculo');

//////////////////// Fim Envio


/**************** Tradução *****************/

Route::get('translate/{id}',function($id){
	Session::put('idioma',$id);
	if($id == 1){
		Session::put('campo','id');
		Session::put('local','local_pt');
	}else{
		Session::put('campo','idtraducao');
		Session::put('local','local_en');
	}
	return Redirect::to(URL::previous());
});

/**************** Login *****************/

Route::resource('login', 'LoginController');

Route::resource('search', 'SearchController');

Route::group(array('before' => 'auth'), function(){

	Route::resource('admin', 'AdminController');

	Route::controller('section', 'SecaoController');
	Route::resource('sections', 'SecaoController');

	Route::controller('categories', 'CategoriaController');
	Route::resource('category', 'CategoriaController');

	Route::controller('adminSlides', 'SlideController');
	Route::resource('adminSlide', 'SlideController');

	Route::controller('adminClientes', 'ClienteController');
	Route::resource('adminCliente', 'ClienteController');

	Route::controller('adminDepoimentos', 'DepoimentoController');
	Route::resource('adminDepoimento', 'DepoimentoController');

	Route::controller('adminServicos', 'ServicoController');
	Route::resource('adminServico', 'ServicoController');

	Route::controller('adminCases', 'CaseController');
	Route::resource('adminCase', 'CaseController');

	Route::resource('pages', 'PaginaController');

	Route::resource('adminNoticia', 'NoticiaController');

	Route::resource('adminSpotlight', 'DestaqueController');

	Route::resource('adminGallery', 'GaleriaController');

	Route::resource('adminContato', 'ContatoController');

	Route::resource('adminCurriculo', 'CurriculoController');

	Route::resource('adminUser', 'UsuarioController');

	Route::resource('adminAcaoEmail', 'AcaoEmailController');

	Route::resource('adminAcaoNoticia', 'AcaoNoticiaController');

	Route::resource('adminAcaoBolhetim', 'AcaoBolhetimController');

	Route::controller('adminImage', 'ImagemController');
	Route::resource('adminImages', 'ImagemController');

	Route::controller('adminVideo', 'VideoController');
	Route::resource('adminVideos', 'VideoController');

	/************** Routes Posts **************/

	Route::post('adminContato', 'ContatoController@getFilter');
	Route::post('adminContatoExcel', 'ContatoController@getExcel');
	Route::post('adminCurriculo', 'CurriculoController@getFilter');
	Route::post('adminSlide', 'SlideController@getFiltro');

	/************** Requisições Ajax **************/

	Route::post('adminSpotlight/check', 'DestaqueController@check');

	/************** FIm Requisições ***************/

	Route::get('logout',function(){	
		Auth::logout();
		return Redirect::to('login');
	});

});
