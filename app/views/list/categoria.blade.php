@section('content')

<script type="text/javascript">
	$(document).ready(function(){
		$('#tabela').dataTable({
			"order": [[ 1, "asc" ]],
			columnDefs: [
		    	{ type: 'date-eu', targets: 4 },
		    	{ type: 'date-eu', targets: 5 }
		    ]
		});
	});
</script>

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i>
				Gerencia de Categorias
			</h3>
			<a href="{{ URL::to('category/create') }}" class="btn btn-info btn-lg btn-animate-demo pull-right" style="margin-top:-27px">
				<i class="fa fa-plus" title="Inativo"> Nova Categoria</i>
			</a>
		</div>
	</div>
	<table class="table users-table table-condensed table-hover" id="tabela">
		<thead>
			<tr>
				<th class="visible-lg">Status</th>
				<th class "visible-lg">Titulo</th>
				<th class "visible-lg">Idioma</th>
				<th class "visible-lg">Seção</th>
				<th class "visible-lg">Data de Atualização</th>
				<th class "visible-lg">Data de Criação</th>
				<th>Ações</th>
			</tr>
		</thead>
			@foreach($categorias as $categoria)
				<tr>
					@if($categoria->status)
						<td class="visible-lg"><i class="fa fa-check" title="Ativo"></i></td>
					@else
						<td class="visible-lg"><i class="fa fa-times-circle" title="Inativo"></i></td>
					@endif
					<td class="visible-lg">{{ $categoria->titulo }} </td>
					<td class="visible-lg">
						<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396661022_Brazil_flat.png")}}'>
						@foreach($bandeiras as $bandeira)
							@if($bandeira->idtraducao == $categoria->id)
								<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396660938_United-Kingdom_flat.png")}}'>
							@endif
						@endforeach
					</td>
					<td class="visible-lg">{{ $categoria->secao }} </td>
					<td>{{ date("d/m/Y", strtotime($categoria->updated_at)) }}</td>
					<td>{{ date("d/m/Y", strtotime($categoria->created_at)) }}</td>
					<td>
						<a href='{{URL::to("/category/$categoria->id/edit ")}}' data-original-title="Chat" class="btn btn-warning btn-xs">
							Editar
						</a>
						{{ Form::open(array('url' => '/category/' . $categoria->id, 'class' => 'btn', 'id' => 'deletar')) }}
							{{ Form::hidden('_method', 'DELETE') }}
							{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs excluir')) }}
						{{ Form::close() }}
					</td>
				</tr>
		@endforeach
	</table>
</div>

@stop