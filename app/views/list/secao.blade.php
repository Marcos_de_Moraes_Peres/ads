@section('content')

<script type="text/javascript">
	$(document).ready(function(){
		$('#tabela').dataTable({
			"order": [[ 1, "asc" ]],
			columnDefs: [
		    	{ type: 'date-eu', targets: 3 },
		    	{ type: 'date-eu', targets: 4 }
		    ]
		});
	});
</script>

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i> 
				Gerencia de Seções
			</h3>
			<a href="{{ URL::to('sections/create') }}" class="btn btn-info btn-lg btn-animate-demo pull-right" style="margin-top:-27px">
				<i class="fa fa-plus" title="Inativo"> Nova Seção</i>
			</a>
		</div>
	</div>
	<table class="table users-table table-condensed table-hover" id="tabela">
		<thead>
			<tr>
				<th class="visible-lg">Status</th>
				<th class "visible-lg">Titulo</th>
				<th class "visible-lg">Idiomas</th>
				<th class "visible-lg">Data de Atualização</th>
				<th class "visible-lg">Data de Criação</th>
				<th>Ações</th>
			</tr>
		</thead>
			@foreach($secoes as $secao)
				<tr>
					@if($secao->status)
						<td class="visible-lg"><i class="fa fa-check" title="Ativo"></i></td>
					@else
						<td class="visible-lg"><i class="fa fa-times-circle" title="Inativo"></i></td>
					@endif
				<td class="visible-lg">{{ $secao->titulo }} </td>
				<td class="visible-lg">
					<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396661022_Brazil_flat.png")}}'>
					@foreach($bandeiras as $bandeira)
						@if($bandeira->idtraducao == $secao->id)
							<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396660938_United-Kingdom_flat.png")}}'>
						@endif
					@endforeach
				</td>
				<td>{{ date("d/m/Y", strtotime($secao->updated_at)) }}</td>
				<td>{{ date("d/m/Y", strtotime($secao->created_at)) }}</td>
				<td>
					<a href='{{URL::to("/sections/$secao->id/edit ")}}' data-original-title="Chat" class="btn btn-warning btn-xs">
						Editar
						</a>

						{{ Form::open(array('url' => '/sections/' . $secao->id, 'class' => 'btn', 'id' => 'deletar')) }}
							{{ Form::hidden('_method', 'DELETE') }}
							{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs excluir')) }}
						{{ Form::close() }}

					</td>
				</tr>
		@endforeach
	</table>
</div>

@stop