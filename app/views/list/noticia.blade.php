@section('content')

<script type="text/javascript">
	$(document).ready(function(){
		$('#tabela').dataTable({
			"order": [[ 4, "desc" ]],
			columnDefs: [
		    	{ type: 'date-eu', targets: 4 }
		    ]
		});
	});
</script>

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i> 
				Gerencia de Notícias
			</h3>
			<a href="{{ URL::to('adminNoticia/create') }}" class="btn btn-info btn-lg btn-animate-demo pull-right" style="margin-top:-27px">
				 <i class="fa fa-plus" title="Inativo"> Nova Notícia</i>
			</a>
		</div>
	</div>
	<table class="table users-table table-condensed table-hover" id="tabela">
		<thead>
			<tr>
				<th class "visible-lg">Status</th>
				<th class "visible-lg">Titulo</th>
				<th class "visible-lg">Idioma</th>
				<th class "visible-lg">Cliente</th>
				<th class "visible-lg">Data</th>
				<th class "visible-lg">Ações</th>
			</tr>
		</thead>
			@foreach($noticias as $noticia)
				<tr>
					@if($noticia->status)
						<td class="visible-lg"><i class="fa fa-check" title="Ativo" style="color:#090"></i></td>
					@else
						<td class="visible-lg"><i class="fa fa-times-circle" title="Inativo" style="color:#F00"></i></td>
					@endif
					<td class="visible-lg">{{ $noticia->titulo }} </td>
					<td class="visible-lg">
						<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396661022_Brazil_flat.png")}}'>
						@foreach($bandeiras as $bandeira)
							@if($bandeira->idtraducao == $noticia->id)
								@if($bandeira->ididioma == 2)
									<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396660938_United-Kingdom_flat.png")}}'>
								@else
									<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396660946_Spain.png")}}'>
								@endif
							@endif
						@endforeach
					</td>
					<td class="visible-lg">{{ $noticia->cliente }} </td>
					<td>{{ date("d/m/Y", strtotime($noticia->updated_at)) }}</td>
					<td>
						<a href='{{URL::to("/adminNoticia/$noticia->id/edit ")}}' data-original-title="Chat" class="btn btn-warning btn-xs">
							Editar
						</a>

						{{ Form::open(array('url' => '/adminNoticia/' . $noticia->id, 'class' => 'btn', 'id' => 'deletar', 'style' => 'padding:0')) }}
							{{ Form::hidden('_method', 'DELETE') }}
							{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs excluir')) }}
						{{ Form::close() }}

					</td>
				</tr>
		@endforeach
	</table>
</div>

@stop