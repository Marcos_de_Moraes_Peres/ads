@section('content')

<script type="text/javascript">
	$(document).ready(function(){
		$('#tabela').dataTable({
			"order": [[ 3, "asc" ]],
			columnDefs: [
		    	{ type: 'date-eu', targets: 3 }
		    ]
		});
	});
</script>

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i> 
				Gerencia de Boletins
			</h3>
			<a href="{{ URL::to('adminAcaoBolhetim/create') }}" class="btn btn-info btn-lg btn-animate-demo pull-right" style="margin-top:-27px">
				<i class="fa fa-plus" title="Inativo"> Novo Boletim</i>
			</a>
		</div>
	</div>
	<table class="table users-table table-condensed table-hover" id="tabela">
		<thead>
			<tr>
				<th class "visible-lg">Status</th>
				<th class "visible-lg">Titulo</th>
				<th class "visible-lg">Edição</th>
				<th class "visible-lg">Data</th>
				<th>Ações</th>
				<th>Email</th>
			</tr>
		</thead>
			@foreach($bolhetim as $key => $bolheto)
				<tr>	
					@if($bolheto->status)
						<td class="visible-lg"><i class="fa fa-check" title="Ativo"></i></td>
					@else
						<td class="visible-lg"><i class="fa fa-times-circle" title="Inativo"></i></td>
					@endif				
					<td class="visible-lg">{{ $bolheto->titulo }} </td>
					<td class="visible-lg">{{ $bolheto->mes }}/{{ $bolheto->ano }} </td>
					<td>{{ date("d/m/Y", strtotime($bolheto->created_at)) }}</td>
					<td>
						<a href='{{URL::to("/adminAcaoBolhetim/$bolheto->id/edit ")}}' data-original-title="Chat" class="btn btn-warning btn-xs">
							Editar
						</a>
						{{ Form::open(array('url' => '/adminAcaoBolhetim/' . $bolheto->id, 'class' => 'btn', 'id' => 'deletar')) }}
							{{ Form::hidden('_method', 'DELETE') }}
							{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs excluir')) }}
						{{ Form::close() }}
						<a href='{{URL::to("/ads-em-acao-show/$bolheto->slug")}}' data-original-title="Chat" class="btn btn-info btn-xs" target="_blank">
							Visualizar
						</a>
					</td>
					<td>
						{{ Form::open(array('url' => 'adminAcaoEmail', 'class' => 'form-horizontal cascade-forms', 'id' => "validEmail$key", 'role' => 'form','enctype' => "multipart/form-data"))}}
							<input type="text" name="email" placeholder="Digite seu email...">
							<input type="hidden" name="idboletim" value="{{$bolheto->id}}">
							{{ Form::submit('Enviar Email', array('class' => 'btn btn-info btn-xs')) }}
						{{ Form::close() }}
					</td>
				</tr>
		@endforeach
	</table>
</div>

@stop