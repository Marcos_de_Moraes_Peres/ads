@section('content')

<script type="text/javascript">
	$(document).ready(function(){
		$('#tabela').dataTable({
			"order": [[ 1, "asc" ]],
			columnDefs: [
		    	{ type: 'date-eu', targets: 3},
		    	{ type: 'date-eu', targets: 4}
		    ]
		});
	});
</script>

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i> 
				Gerencia de Slides
			</h3>
			<a href="{{ URL::to('adminSlide/create') }}" class="btn btn-info btn-lg btn-animate-demo pull-right" style="margin-top:-27px">
				 <i class="fa fa-plus" title="Inativo"> Novo Slide</i>
			</a>
		</div>
	</div>
	<div class="col-md-12">
		<div class="panel panel-cascade">
			<div class="panel-heading">
				<h3 class="panel-title">Filtros</h3>
			</div>
			<div class="panel-body">
				{{ Form::open(array("action" => "SlideController@getFiltro",'files'=>true, 'class' => 'form-horizontal cascade-forms', 'id' => 'validateFiltro', 'role' => 'form','enctype' => "multipart/form-data" )) }}
					<div class="row">
						<label class="col-lg-1 col-md-2 control-label">Status</label>
						<div class="col-lg-2 col-md-9">

							{{ Form::select('status', array('2' => 'Todos','1' => 'Ativo', '0' => 'Inativo'), null,array('class' => 'form-control form-cascade-control')) }}

						</div>
						<label class="col-lg-1 col-md-2 control-label">Titulo</label>
						<div class="col-lg-4 col-md-9">
							{{

								Form::text('titulo',null,array(
									'class' =>  'form-control form-cascade-control input-small',
									'name' => 'titulo',
									'id' =>'titulo',
									'placeholder' => 'Digite o titulo...'
								))

							}}
						</div>
						<div class="col-lg-3 col-md-9">
							<div class="col-lg-6 col-md-9">
								{{ Form::submit('Pesquisar', array('class' => 'btn btn-primary pull-right')) }}
							</div>
						</div>
					</div>				
				{{ Form::close() }}
			</div> <!-- /Panel Body -->
		</div>
	</div>
	<table class="table users-table table-condensed table-hover" id="tabela">
		<thead>
			<tr>
				<th class="visible-lg">Status</th>
				<th class "visible-lg">Titulo</th>
				<th class "visible-lg">Idioma</th>
				<th class "visible-lg">Data de Atualização</th>
				<th class "visible-lg">Data de Criação</th>
				<th>Ações</th>
			</tr>
		</thead>
			@foreach($slides as $slide)
				<tr>

					@if($slide->status)
						<td class="visible-lg"><i class="fa fa-check" title="Ativo" style="color:#090"></i></td>
					@else
						<td class="visible-lg"><i class="fa fa-times-circle" title="Inativo" style="color:#F00"></i></td>
					@endif

					<td class="visible-lg">{{ $slide->titulo }} </td>
					<td class="visible-lg">
						<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396661022_Brazil_flat.png")}}'>
						@foreach($bandeiras as $bandeira)
							@if($bandeira->idtraducao == $slide->id)
								<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396660938_United-Kingdom_flat.png")}}'>
							@endif
						@endforeach
					</td>
					<td>{{ date("d/m/Y", strtotime($slide->updated_at)) }}</td>
					<td>{{ date("d/m/Y", strtotime($slide->created_at)) }}</td>
					<td>
						<a href='{{URL::to("/adminSlide/$slide->id/edit ")}}' data-original-title="Chat" class="btn btn-warning btn-xs">
							Editar
						</a>

						{{ Form::open(array('url' => '/adminSlide/' . $slide->id, 'class' => 'btn', 'id' => 'deletar')) }}
							{{ Form::hidden('_method', 'DELETE') }}
							{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs excluir')) }}
						{{ Form::close() }}

					</td>
				</tr>
		@endforeach
	</table>
	{{$slides->links()}}
</div>

@stop