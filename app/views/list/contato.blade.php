@section('content')

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i> 
				Relatório de Contato
			</h3>
		</div>
	</div>

	
	<div class="col-md-12">
		<div class="panel panel-cascade">
			<div class="panel-heading">
				<h3 class="panel-title">Filtros</h3>
			</div>
			<div class="panel-body">
				{{ Form::open(array("action" => "ContatoController@getFilter",'files'=>true, 'class' => 'form-horizontal cascade-forms', 'id' => 'validateFiltro', 'role' => 'form','enctype' => "multipart/form-data" )) }}
					<div class="row">
						<label class="col-lg-1 col-md-2 control-label">Início:</label>
						<div class="col-lg-3 col-md-9">
							@if(isset($dataInicio))
								<input type="text" id="headInicio" name="headInicio" value="{{$dataInicio}}" class="form-control form-cascade-control input-small picker"></p>
							@else
								<input type="text" id="headInicio" name="headInicio" class="form-control form-cascade-control input-small picker"></p>
							@endif
						</div>
						<label class="col-lg-1 col-md-2 control-label">Fim:</label>
						<div class="col-lg-3 col-md-9">
							@if(isset($dataFim))
								<input type="text" id="headFim" name="headFim" value="{{$dataFim}}" class="form-control form-cascade-control input-small picker"></p>
							@else
								<input type="text" id="headFim" name="headFim" class="form-control form-cascade-control input-small picker"></p>
							@endif
						</div>
						<div class="col-lg-3 col-md-9">
							{{ Form::submit('Pesquisar', array('class' => 'btn btn-primary')) }}
						</div>
					</div>
				{{ Form::close() }}
			</div> <!-- /Panel Body -->
		</div>
	</div>

	<div class="col-md-7" style="margin-bottom:15px;"> 
		<div class="col-md-4 pull-right">
			@if(isset($contatos))
				{{ Form::open(array("action" => "ContatoController@getExcel",'files'=>true, 'class' => 'form-horizontal cascade-forms', 'id' => 'validateFiltros', 'role' => 'form','enctype' => "multipart/form-data" )) }}
					<input type="hidden" value="{{$dataInicio}}" name="inicio">
					<input type="hidden" value="{{$dataFim}}" name="fim">
					{{ Form::submit('Exportar Planilha', array('class' => 'btn btn-success btn-block active btn-animate-demo')) }}
				{{ Form::close()}}
			@else
				<a class="btn btn-success btn-block active btn-animate-demo" disabled="disabled">Exportar Planilha</a>
			@endif
		</div>
	</div>

	<table class="table users-table table-condensed table-hover" id="tabela">
		<thead>
			<tr>
				<th class="visible-lg">Nome</th>
				<th class="visible-lg">Empresa</th>
				<th class "visible-lg">Email</th>
				<th class "visible-lg">Telefone</th>
				<th class "visible-lg">Data</th>
				<th class "visible-lg">Ações</th>
				<th class "visible-lg">Mensagem</th>
			</tr>
		</thead>
		@if(isset($contatos))
			@foreach($contatos as $contato)

				<tr>
					<td class="visible-lg">{{ $contato->nome }} </td>
					<td class="visible-lg">{{ $contato->empresa }} </td>
					<td class="visible-lg">{{ $contato->telefone }} </td>
					<td class="visible-lg">{{ $contato->email }} </td>
					<td>{{ date("d/m/Y H:i:s", strtotime($contato->created_at)) }}</td>
					<td>
						<a href='{{URL::to("/adminContato/$contato->id/edit")}}' data-original-title="Chat" class="btn btn-danger btn-xs excluir " id="deletar">
							Deletar
						</a>
					</td>
					<td>
						<div class="panel-group" id="accordion">
							<div class="panel panel-cascade">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$contato->id}}">
									<i class="fa fa-search"> Visualizar </i>
								</a>
					</td>
				</tr>
					<td colspan="5">
								<div id="collapse{{$contato->id}}" class="panel-collapse collapse" style="height: auto;">
									<div class="panel-body">
										<b>Assunto:</b> {{$contato->assunto}} <br />
										<b>Mensagem:</b> {{$contato->mensagem}}
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>

			@endforeach
		@endif
	</table>
</div>

@stop