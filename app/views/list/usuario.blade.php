@section('content')

<script type="text/javascript">
	$(document).ready(function(){
		$('#tabela').dataTable({
			"order": [[ 1, "asc" ]],
			columnDefs: [
		    	{ type: 'date-eu', targets: 2 },
		    	{ type: 'date-eu', targets: 3 }
		    ]
		});
	});
</script>

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i> 
				Gerencia de Usuários
			</h3>
			<a href="{{ URL::to('adminUser/create') }}" class="btn btn-info btn-lg btn-animate-demo pull-right" style="margin-top:-27px">
				 <i class="fa fa-plus" title="Inativo"> Novo Usuário</i>
			</a>
		</div>
	</div>
	<table class="table users-table table-condensed table-hover" id="tabela">
		<thead>
			<tr>
				<th class "visible-lg">Nome</th>
				<th class "visible-lg">Login</th>
				<th class "visible-lg">Data de Atualização</th>
				<th class "visible-lg">Data de Criação</th>
				<th>Ações</th>
			</tr>
		</thead>
			@foreach($usuarios as $usuario)
				<tr>
					<td class="visible-lg">{{ $usuario->nome }} </td>
					<td class="visible-lg">{{ $usuario->email }} </td>
					<td>{{ date("d/m/Y", strtotime($usuario->updated_at)) }}</td>
					<td>{{ date("d/m/Y", strtotime($usuario->created_at)) }}</td>
					<td>
						<a href='{{URL::to("/adminUser/$usuario->id/edit ")}}' data-original-title="Chat" class="btn btn-warning btn-xs">
							Editar
						</a>

						{{ Form::open(array('url' => '/adminUser/' . $usuario->id, 'class' => 'btn', 'id' => 'deletar')) }}
							{{ Form::hidden('_method', 'DELETE') }}
							{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs excluir')) }}
						{{ Form::close() }}

					</td>
				</tr>
		@endforeach
	</table>
	{{$usuarios->links()}}
</div>

@stop