@section('content')

<script type="text/javascript">
	$(document).ready(function(){
		$('#tabela').dataTable({
			"order": [[ 1, "asc" ]],
			columnDefs: [
		    	{ type: 'date-eu', targets: 1 }
		    ]
		});
	});
</script>

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i> 
				Gerencia de Notícias
			</h3>
			<a href="{{ URL::to('adminAcaoNoticia/create') }}" class="btn btn-info btn-lg btn-animate-demo pull-right" style="margin-top:-27px">
				<i class="fa fa-plus" title="Inativo"> Nova Notícia</i>
			</a>
		</div>
	</div>
	<table class="table users-table table-condensed table-hover" id="tabela">
		<thead>
			<tr>
				<th class "visible-lg">Titulo</th>
				<th class "visible-lg">Data</th>
				<th>Ações</th>
			</tr>
		</thead>
			@foreach($noticias as $noticia)
				<tr>					
					<td class="visible-lg">{{ $noticia->titulo }} </td>
					<td>{{ date("d/m/Y", strtotime($noticia->created_at)) }}</td>
					<td>
						<a href='{{URL::to("/adminAcaoNoticia/$noticia->id/edit ")}}' data-original-title="Chat" class="btn btn-warning btn-xs">
							Editar
						</a>

						{{ Form::open(array('url' => '/adminAcaoNoticia/' . $noticia->id, 'class' => 'btn', 'id' => 'deletar')) }}
							{{ Form::hidden('_method', 'DELETE') }}
							{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs excluir')) }}
						{{ Form::close() }}

					</td>
				</tr>
		@endforeach
	</table>
</div>

@stop