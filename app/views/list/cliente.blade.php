@section('content')

<script type="text/javascript">
	$(document).ready(function(){
		$('#tabela').dataTable({
			"order": [[ 1, "asc" ]],
			columnDefs: [
		    	{ type: 'date-eu', targets: 3 }
		    ]
		});
	});
</script>

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i> 
				Gerencia de Clientes
			</h3>
			<a href="{{ URL::to('adminCliente/create') }}" class="btn btn-info btn-lg btn-animate-demo pull-right" style="margin-top:-27px">
				<i class="fa fa-plus" title="Inativo"> Novo Cliente</i>
			</a>
		</div>
	</div>
	<table class="table users-table table-condensed table-hover" id="tabela">
		<thead>
			<tr>
				<th class "visible-lg">Status</th>
				<th class "visible-lg">Nome</th>
				<th class "visible-lg">Idioma</th>
				<th class "visible-lg">Data</th>
				<th>Ações</th>
			</tr>
		</thead>
			@foreach($clientes as $cliente)
				<tr>
					@if($cliente->status)
						<td class="visible-lg"><i class="fa fa-check" title="Ativo" style="color:#090"></i></td>
					@else
						<td class="visible-lg"><i class="fa fa-times-circle" title="Inativo" style="color:#F00"></i></td>
					@endif
					<td class="visible-lg">{{ $cliente->nome }} </td>
					<td class="visible-lg">
						<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396661022_Brazil_flat.png")}}'>
						@foreach($bandeiras as $bandeira)
							@if($bandeira->idtraducao == $cliente->id)
								@if($bandeira->ididioma == 2)
									<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396660938_United-Kingdom_flat.png")}}'>
								@else
									<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396660946_Spain.png")}}'>
								@endif
							@endif
						@endforeach
					</td>
					<td>{{ date("d/m/Y", strtotime($cliente->updated_at)) }}</td>
					<td>
						<a href='{{URL::to("/adminCliente/$cliente->id/edit ")}}' data-original-title="Chat" class="btn btn-warning btn-xs">
							Editar
						</a>
						
						{{ Form::open(array('url' => '/adminCliente/' . $cliente->id, 'class' => 'btn', 'id' => 'deletar')) }}
							{{ Form::hidden('_method', 'DELETE') }}
							{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs excluir')) }}
						{{ Form::close() }}

					</td>
				</tr>
		@endforeach
	</table>
</div>

@stop