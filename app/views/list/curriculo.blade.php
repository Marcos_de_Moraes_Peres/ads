@section('content')

<script type="text/javascript">
	$(document).ready(function(){
		$('#tabela').dataTable({
			"order": [[ 0, "asc" ]],
			columnDefs: [
		    	{ type: 'date-eu', targets: 6 }
		    ]
		});
	});
</script>


<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i>
				Relatório de Curriculo
			</h3>
		</div>
	</div>
	<div class="col-md-12">
		<div class="panel panel-cascade">
			<div class="panel-heading">
				<h3 class="panel-title">Filtros</h3>
			</div>
			<div class="panel-body">
				{{ Form::open(array("action" => "CurriculoController@getFilter",'files'=>true, 'class' => 'form-horizontal cascade-forms', 'id' => 'validateFiltro', 'role' => 'form','enctype' => "multipart/form-data" )) }}
					<div class="row">
						<label class="col-lg-1 col-md-2 control-label">Inicio:</label>
						<div class="col-lg-3 col-md-9">
							@if(isset($dataInicio))
								<input type="text" id="headInicio" name="headInicio" value="{{$dataInicio}}" class="form-control form-cascade-control input-small picker"></p>
							@else
								<input type="text" id="headInicio" name="headInicio" class="form-control form-cascade-control input-small picker"></p>
							@endif
						</div>
						<label class="col-lg-1 col-md-2 control-label">Fim:</label>
						<div class="col-lg-3 col-md-9">
							@if(isset($dataFim))
								<input type="text" id="headFim" name="headFim" value="{{$dataFim}}" class="form-control form-cascade-control input-small picker"></p>
							@else
								<input type="text" id="headFim" name="headFim" class="form-control form-cascade-control input-small picker"></p>
							@endif
						</div>
						<div class="col-lg-3 col-md-9">
							<div class="col-lg-6 col-md-9">
								{{ Form::submit('Pesquisar', array('class' => 'btn btn-primary pull-right')) }}
							</div>
						</div>
					</div>				
				{{ Form::close() }}
			</div> <!-- /Panel Body -->
		</div>
	</div>

	<table class="table users-table table-condensed table-hover" style="text-align:center;" id="tabela">
		<thead>
			<tr>
				<th class="visible-lg" style="text-align:center;">Nome</th>
				<th class "visible-lg" style="text-align:center;">Area</th>
				<th class "visible-lg" style="text-align:center;">Curriculo</th>
				<th class "visible-lg" style="text-align:center;">Situação</th>				
				<th class "visible-lg" style="text-align:center;">Email</th>				
				<th class "visible-lg" style="text-align:center;">Telefone</th>				
				<th class "visible-lg" style="text-align:center;">Data de criação</th>
				<th class "visible-lg" style="text-align:center;">Ações</th>
			</tr>
		</thead>
		@if(isset($curriculos))
			@foreach($curriculos as $curriculo)
				<tr>
					<td class="visible-lg">{{ $curriculo->nome }}</td>
					<td class="visible-lg">{{ $curriculo->iddepto }}</td>
					<td class="visible-lg">
						<a href='{{URL::to("assets/docs/$curriculo->file")}}' target="_blank"><i class="fa fa-file-text"></i></a>
					</td>
					<td class="visible-lg">{{ $curriculo->situacao }}</td>
					<td class="visible-lg">{{ $curriculo->email }}</td>
					<td class="visible-lg">{{ $curriculo->telefone }}</td>
					<td>{{ date("d/m/Y", strtotime($curriculo->created_at)) }}</td>
					<td>
						<a href='{{URL::to("/adminCurriculo/$curriculo->id/edit")}}' data-original-title="Chat" class="btn btn-danger btn-xs excluir " id="deletar">
							Deletar
						</a>
					</td>
				</tr>
			@endforeach
		@endif
	</table>
</div>

@stop