@section('content')

<style type="text/css">
.tip {
	color: #fff;
	background:#1d1d1d;
	display:none; /*--Hides by default--*/
	padding:10px;
	position:absolute;	z-index:1000;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
}
</style>

<script type="text/javascript">
		
		$(document).ready(function() {

		$('#tabela').dataTable({
			"order": [[ 1, "asc" ]],
			columnDefs: [
		    	{ type: 'date-eu', targets: 3 }
		    ]
		});

		//Tooltips
		$(".tip_trigger").hover(function(){
			tip = $(this).find('.tip');
			tip.show(); //Show tooltip
		}, function() {
			tip.hide(); //Hide tooltip
		}).mousemove(function(e) {
		var mousex = e.pageX + 20; //Get X coodrinates
		var mousey = e.pageY + 20; //Get Y coordinates
		var tipWidth = tip.width(); //Find width of tooltip
		var tipHeight = tip.height(); //Find height of tooltip

		//Distance of element from the right edge of viewport
		var tipVisX = $(window).width() - (mousex + tipWidth);
		//Distance of element from the bottom of viewport
		var tipVisY = $(window).height() - (mousey + tipHeight);

		if ( tipVisX < 20 ) { //If tooltip exceeds the X coordinate of viewport
			mousex = e.pageX - tipWidth - 20;
			} if ( tipVisY < 20 ) { //If tooltip exceeds the Y coordinate of viewport
				mousey = e.pageY - tipHeight - 20;
			}
			//Absolute position the tooltip according to mouse position
			tip.css({  top: mousey, left: mousex });
		});
});

</script>


<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i> 
				Gerencia de Galerias
			</h3>
			<a href="{{ URL::to('adminGallery/create') }}" class="btn btn-info btn-lg btn-animate-demo pull-right" style="margin-top:-27px">
				<i class="fa fa-plus" title="Inativo"> Nova Galeria</i>
			</a>
		</div>
	</div>
	<table class="table users-table table-condensed table-hover" id="tabela">
		<thead>
			<tr>
				<th class="visible-lg">Titulo</th>
				<th class="visible-lg">Cases</th>
				<th class="visible-lg">Notícias</th>
				<th class "visible-lg">Data</th>
				<th class "visible-lg">Gerências</th>
				<th>Ações</th>
			</tr>
		</thead>
		@foreach($galerias as $galeria)
		<tr>
			<td class="visible-lg">{{ $galeria->titulo }} </td>
			<td class="visible-lg"><a class="tip_trigger">
				<span class="badge bg-primary">{{count($galeria->cases)}}</span>
				<span class="tip">
					@foreach($galeria->cases as $case)
						{{$case->cases}}<br />
					@endforeach
				</span></a>
			</td>
			<td class=""><a class="tip_trigger">
				<span class="badge bg-primary">{{count($galeria->noticias)}}</span>
				<span class="tip">
					@foreach($galeria->noticias as $noticia)
						{{$noticia->noticias}}<br />
					@endforeach
				</span></a>
			</td>
			<td>{{ date("d/m/Y", strtotime($galeria->updated_at)) }}</td>
			<td>
				<a href='{{URL::to("/adminImages/$galeria->id/edit ")}}' data-original-title="Chat" class="btn btn-info btn-xs">
					<i class="fa fa-picture-o"></i> Imagens
				</a>
				<a href='{{URL::to("/adminVideos/$galeria->id/edit ")}}' data-original-title="Chat" class="btn btn-info btn-xs">
					<i class="fa fa-video-camera"></i> Videos
				</a>
			</td>
			<td>
				<a href='{{URL::to("/adminGallery/$galeria->id/edit ")}}' data-original-title="Chat" class="btn btn-warning btn-xs">
					Editar
				</a>

				{{ Form::open(array('url' => '/adminGallery/' . $galeria->id, 'class' => 'btn', 'id' => 'deletar')) }}
					{{ Form::hidden('_method', 'DELETE') }}
					{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs excluir')) }}
				{{ Form::close() }}

			</td>
			</tr>
		@endforeach
	</table>
</div>

@stop