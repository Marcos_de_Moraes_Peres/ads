@section('content')

<script type="text/javascript">
	$(document).ready(function(){
		$('#tabela').dataTable({
			"order": [[ 1, "asc" ]],
			columnDefs: [
		    	{ type: 'date-eu', targets: 4 }
		    ]
		});
	});
</script>

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i>
				Gerencia de Cases
			</h3>
			@if(Auth::user()->id == 1)
			<a href="{{ URL::to('adminCase/create') }}" class="btn btn-info btn-lg btn-animate-demo pull-right" style="margin-top:-27px">
				<i class="fa fa-plus" title="Inativo"> Novo Case</i>
			</a>
			@endif
		</div>
	</div>
	<table class="table users-table table-condensed table-hover" id="tabela">
		<thead>
			<tr>
				<th class "visible-lg">Status</th>
				<th class "visible-lg">Titulo</th>
				<th class "visible-lg">Idioma</th>
				<th class "visible-lg">Servico</th>
				<th class "visible-lg">Data</th>
				<th>Ações</th>
			</tr>
		</thead>
			@foreach($cases as $case)
				<tr>
					@if($case->status)
						<td class="visible-lg"><i class="fa fa-check" title="Ativo" style="color:#090"></i></td>
					@else
						<td class="visible-lg"><i class="fa fa-times-circle" title="Inativo" style="color:#F00"></i></td>
					@endif
					<td class="visible-lg">{{ $case->titulo }} </td>
					<td class="visible-lg">
						<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396661022_Brazil_flat.png")}}'>
						@foreach($bandeiras as $bandeira)
							@if($bandeira->idtraducao == $case->id)
								@if($bandeira->ididioma == 2)
									<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396660938_United-Kingdom_flat.png")}}'>
								@else
									<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396660946_Spain.png")}}'>
								@endif
							@endif
						@endforeach
					</td>
					<td class="visible-lg">{{ $case->servico }} </td>
					<td>{{ date("d/m/Y", strtotime($case->updated_at)) }}</td>
					<td>
						<a href='{{URL::to("/adminCase/$case->id/edit ")}}' data-original-title="Chat" class="btn btn-warning btn-xs">
							Editar
						</a>

						{{ Form::open(array('url' => '/adminCase/' . $case->id, 'class' => 'btn', 'id' => 'deletar')) }}
							{{ Form::hidden('_method', 'DELETE') }}
							{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs excluir')) }}
						{{ Form::close() }}

					</td>
				</tr>
			@endforeach
	</table>
</div>

@stop