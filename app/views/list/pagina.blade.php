@section('content')

<script type="text/javascript">
	$(document).ready(function(){
		$('#tabela').dataTable({
			"order": [[ 0, "asc" ]],
			columnDefs: [
		    	{ type: 'date-eu', targets: 4 }
		    ]
		});
	});
</script>


<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i> 
				Gerencia de Páginas
			</h3>
			<a href="{{ URL::to('pages/create') }}" class="btn btn-info btn-lg btn-animate-demo pull-right" style="margin-top:-27px">
				 <i class="fa fa-plus"> Nova Página</i>
			</a> 
		</div>
	</div>
	<table class="table users-table table-condensed table-hover" id="tabela">
		<thead>
			<tr>
				<th class "visible-lg">Titulo</th>
				<th class "visible-lg">Idioma</th>
				<th class "visible-lg">Categoria</th>
				<th class "visible-lg">Seção</th>
				<th class "visible-lg">Data</th>
				<th>Ações</th>
			</tr>
		</thead>
			@foreach($paginas as $pagina)
				<tr>
					<td class="visible-lg">{{ $pagina->titulo }} </td>
					<td class="visible-lg">
						<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396661022_Brazil_flat.png")}}'>
						@foreach($bandeiras as $bandeira)
							@if($bandeira->idtraducao == $pagina->id)
								@if($bandeira->ididioma == 2)
									<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396660938_United-Kingdom_flat.png")}}'>
								@else
									<img class="img-circle"src='{{URL::to("assets/img/idiomas/1396660946_Spain.png")}}'>
								@endif
							@endif
						@endforeach
					</td>
					<td class="visible-lg">{{ $pagina->categoria }} </td>
					<td class="visible-lg">{{ $pagina->secao }} </td>
					<td>{{ date("d/m/Y", strtotime($pagina->updated_at)) }}</td>
					<td>
						<a href='{{URL::to("/pages/$pagina->id/edit ")}}' data-original-title="Chat" class="btn btn-warning btn-xs">
							Editar
						</a>

						{{ Form::open(array('url' => '/pages/' . $pagina->id, 'class' => 'btn', 'id' => 'deletar')) }}
							{{ Form::hidden('_method', 'DELETE') }}
							{{ Form::submit('Deletar', array('class' => 'btn btn-danger btn-xs excluir')) }}
						{{ Form::close() }}

					</td>
				</tr>
		@endforeach
	</table>
	{{$paginas->links()}}
</div>

@stop