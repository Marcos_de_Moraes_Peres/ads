@section('content')

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i>
				Ordenar Slides
			</h3>
		</div>
	</div>
	<table class="table users-table table-condensed table-hover">
		<thead>
			<tr>
				<th class "visible-lg">Titulo</th>
			</tr>
		</thead>
	</table>
	{{ Form::open(array('url' => 'adminSlides/filter', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateDepoimento', 'role' => 'form','enctype' => "multipart/form-data" )) }}
		<ul id="sortable">
			@foreach($slides as $key => $slide)
				<li class="ui-state-default"><span>{{$key+1}} -</span>{{$slide->titulo}}
					<input type="hidden" name="{{ $key }}" value="{{ $slide->id }}" />
				</li>
			@endforeach
		</ul>
		<div class="col-lg-4 col-md-9">
			{{ Form::submit('Salvar Ordem', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
		</div>
	{{ Form::close() }}
</div>

@stop