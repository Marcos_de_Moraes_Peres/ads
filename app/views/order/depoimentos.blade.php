@section('content')

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i>
				Ordenar Depoimentos
			</h3>
		</div>
	</div>
	<table class="table users-table table-condensed table-hover">
		<thead>
			<tr>
				<th class "visible-lg">Nome</th>
			</tr>
		</thead>
	</table>
	{{ Form::open(array('url' => 'adminDepoimentos/filter', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateDepoimento', 'role' => 'form','enctype' => "multipart/form-data" )) }}
		<ul id="sortable">
			@foreach($depoimentos as $key => $depoimento)
				<li class="ui-state-default"><span>{{$key+1}} -</span>{{$depoimento->nome}}
					<input type="hidden" name="{{ $key }}" value="{{ $depoimento->id }}" />
				</li>
			@endforeach
		</ul>
		<div class="col-lg-6 col-md-9">
			{{ Form::submit('Salvar Ordem', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
		</div>
	{{ Form::close() }}
</div>

@stop