@section('content')

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i>
				Ordenar Cases
			</h3>
		</div>
	</div>
	<div class="col-md-12">
		<div class="panel panel-cascade">
			<div class="panel-heading">
				<h3 class="panel-title">Filtros</h3>
			</div>
		</div>
	</div>
	<table class="table users-table table-condensed table-hover">
		<thead>
			<tr>
				<th class "visible-lg">Titulo</th>
			</tr>
		</thead>
	</table>
	{{ Form::open(array('url' => 'adminCases/filter', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateDepoimento', 'role' => 'form','enctype' => "multipart/form-data" )) }}
		@if(isset($cases[0]->id))
			<ul id="sortable">
				@foreach($cases as $key => $case)
					<li class="ui-state-default"><span>{{$key+1}} -</span>{{$case->titulo}}
						<input type="hidden" name="{{ $key }}" value="{{ $case->id }}" />
					</li>
				@endforeach
			</ul>
			<div class="col-lg-4 col-md-9">
				{{ Form::submit('Salvar Ordem', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
			</div>
		@endif
	{{ Form::close() }}
</div>

@stop