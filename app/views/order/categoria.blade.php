@section('content')

<div class="panel">
	<br />
	<div class="panel-heading text-primary">
		<div>
			<h3 class="panel-title"><i class="fa fa-list-alt"></i>
				Ordenar Categorias
			</h3>
		</div>
	</div>
	<div class="col-md-12">
		<div class="panel panel-cascade">
			<div class="panel-heading">
				<h3 class="panel-title">Filtros</h3>
			</div>
			<div class="panel-body">
				{{ Form::open(array("url" => "categories/filtro",'files'=>true, 'class' => 'form-horizontal cascade-forms', 'id' => 'validateFiltro', 'role' => 'form','enctype' => "multipart/form-data" )) }}
					<div class="row">
						<div class="col-lg-3 col-md-9">
							<select name="idsecao" id="idsecao" class="form-control form-cascade-control">
								<option></option>
								@foreach($secoes as $secao)
									@if(isset($idsecao))
										@if($idsecao == $secao->id)
											<option value="{{$secao->id}}" selected="true">{{$secao->titulo}}</option>
										@else
											<option value="{{$secao->id}}">{{$secao->titulo}}</option>
										@endif
									@else
										<option value="{{$secao->id}}">{{$secao->titulo}}</option>
									@endif
								@endforeach
							</select>
						</div>
						<div class="col-lg-3 col-md-9">
							{{ Form::submit('Pesquisar', array('class' => 'btn btn-primary')) }}
						</div>
					</div>
				{{ Form::close() }}
			</div> <!-- /Panel Body -->
		</div>
	</div>
	<table class="table users-table table-condensed table-hover">
		<thead>
			<tr>
				<th class "visible-lg">Titulo</th>
			</tr>
		</thead>
	</table>
	{{ Form::open(array('url' => 'categories/filter', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateDepoimento', 'role' => 'form','enctype' => "multipart/form-data" )) }}
		@if(isset($categorias[0]->id))
			<ul id="sortable">
				@foreach($categorias as $key => $categoria)
					<li class="ui-state-default"><span>{{$key+1}} -</span>{{$categoria->titulo}}
						<input type="hidden" name="{{ $key }}" value="{{ $categoria->id }}" />
					</li>
				@endforeach
			</ul>
			<div class="col-lg-4 col-md-9">
				{{ Form::submit('Salvar Ordem', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
			</div>
		@endif
	{{ Form::close() }}
</div>

@stop