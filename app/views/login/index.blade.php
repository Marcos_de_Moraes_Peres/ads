@section('content')

{{ HTML::style('assets/admin/css/login.css'); }}


<div class="list-group side-menu ">
</div>

<section id="lock-screen">
	<div class="row animated fadeILeftBig">
		<div class="login-holder col-md-6 col-md-offset-3">
       <h2 class="page-header text-center text-primary"> <img src="http://192.168.1.48/ads/public/assets/img/logo.png" /> </h2> 
			@if ( count($errors) > 0)
				<ul>
					@foreach ($errors->all() as $e)
						<li>{{ $e }}</li>
					@endforeach
				</ul>
			@endif
			{{ Form::open(array('url' => 'login','id' => 'validatePage', 'role' => 'form','enctype' => "multipart/form-data" )) }}
				<div class="form-group">
					<input type="text" class="form-control" id="login" name="login" placeholder="Seu Login">
				</div>
				<div class="form-group" style="height: 42px;">
					<input type="password" class="" id="password" name="password"  placeholder="Sua senha" style="width: 100%;height: 100%;border: none;padding-left: 19px;">
				</div>
				<div class="form-footer" style="text-align:center">
					<button type="submit" class="btn btn-primary btn-lg btn-submit">Login</button>
				</div>
			{{ Form::close() }}
		</div>
	</div>
</section>

<!-- Load JS here for Faster site load =============================-->

{{ HTML::script('assets/admin/js/jquery-1.10.2.min.js'); }}
{{ HTML::script('assets/admin/js/jquery-ui-1.10.3.custom.min.js'); }}
{{ HTML::script('assets/admin/js/jquery.ui.touch-punch.min.js'); }}
{{ HTML::script('assets/admin/js/bootstrap.min.js'); }}
{{ HTML::script('assets/admin/js/bootstrap-select.js'); }}
{{ HTML::script('assets/admin/js/jquery.tagsinput.js'); }}
{{ HTML::script('assets/admin/js/jquery.placeholder.js'); }}
{{ HTML::script('assets/admin/js/bootstrap-typeahead.js'); }}
{{ HTML::script('assets/admin/js/application.js'); }}
{{ HTML::script('assets/admin/js/moment.min.js'); }}
{{ HTML::script('assets/admin/js/jquery.dataTables.min.js'); }}
{{ HTML::script('assets/admin/js/jquery.sortable.js'); }}
{{ HTML::script('assets/admin/js/jquery.gritter.js'); }}
{{ HTML::script('assets/admin/js/jquery.nicescroll.min.js'); }}
{{ HTML::script('assets/admin/js/skylo.js'); }}
{{ HTML::script('assets/admin/js/prettify.min.js'); }}
{{ HTML::script('assets/admin/js/jquery.noty.js'); }}
{{ HTML::script('assets/admin/js/scroll.js'); }}
{{ HTML::script('assets/admin/js/jquery.panelSnap.js'); }}
{{ HTML::script('assets/admin/js/login.js'); }}

@stop