@include("partials.header")
@include("partials.menu")

	@if (Session::has('success'))
		<div class="alert alert-success">
			<div class="panel-heading">
				<h3 class="panel-title">
					<span class="glyphicon glyphicon-success-sign"></span>
					{{ Session::get('success') }}
					<span class="pull-right">
						<a href="#" class="panel-close" style="color:#2E2E2E;"><strong><button type="button" class="close" aria-hidden="true">&times;</button></strong></a>
					</span>
				</h3>
			</div>
		</div>
	@elseif(Session::has('warning'))
		<div class="alert alert-warning">
			<div class="panel-heading">
				<h3 class="panel-title">
					<span class="glyphicon glyphicon-warning-sign"></span>
					{{ Session::get('warning') }}
					<span class="pull-right">
						<a href="#" class="panel-close" style="color:#2E2E2E;"><strong><button type="button" class="close" aria-hidden="true">&times;</button></strong></a>
					</span>
				</h3>
			</div>
		</div>
	@elseif(Session::has('danger'))
		<div class="alert alert-danger">
			<div class="panel-heading">
				<h3 class="panel-title">
					<span class="glyphicon glyphicon-danger-sign"></span>
					{{ Session::get('danger') }}
					<span class="pull-right">
						<a href="#" class="panel-close" style="color:#2E2E2E;"><strong><button type="button" class="close" aria-hidden="true">&times;</button></strong></a>
					</span>
				</h3>
			</div>
		</div>
	@endif

	@yield('content')

@include("partials.footer")
@include("partials.scripts")