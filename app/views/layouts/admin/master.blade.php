<!DOCTYPE html>

<html lang="pt-BR">
	<head>

		@include('partials.admin.header')

	</head>
	<body>

		<div class="site-holder">

			@include('partials.admin.navbar')

			<div class="box-holder">

				@include('partials.admin.sidebar')

				<div class="content">

					@if (Session::has('success'))
						<div class="alert alert-success">
							<div class="panel-heading">
								<h3 class="panel-title">
									{{ Session::get('success') }}
									<span class="pull-right">
										<a href="#" class="panel-close" style="color:#2E2E2E;"><strong><i class="fa fa-times"></i></strong></a>
									</span>
								</h3>
							</div>
						</div>
					@elseif(Session::has('warning'))
						<div class="alert alert-warning">
							<div class="panel-heading">
								<h3 class="panel-title">
									{{ Session::get('warning') }}
									<span class="pull-right">
										<a href="#" class="panel-close" style="color:#2E2E2E;"><strong><i class="fa fa-times"></i></strong></a>
									</span>
								</h3>
							</div>
						</div>
					@elseif(Session::has('danger'))
						<div class="alert alert-danger">
							<div class="panel-heading">
								<h3 class="panel-title">
									{{ Session::get('danger') }}
									<span class="pull-right">
										<a href="#" class="panel-close" style="color:#2E2E2E;"><strong><i class="fa fa-times"></i></strong></a>
									</span>
								</h3>
							</div>
						</div>
					@endif

					@yield('content')
				</div>

				@include('partials.admin.sidebar_hidden')

			</div>
		</div>

		@include('partials.admin.footer')

	</body>
</html>