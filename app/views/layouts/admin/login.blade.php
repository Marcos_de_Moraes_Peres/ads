<!DOCTYPE html>

<html lang="pt-BR">
	<head>
		{{ HTML::style('assets/admin/css/bootstrap.css'); }}
		{{ HTML::style('assets/admin/css/style.css'); }}
		{{ HTML::style('assets/admin/css/login.css'); }}
		{{ HTML::style('assets/admin/css/custom.css'); }}
	</head>
	<body>
		@if (Session::has('message'))
			<div class="alert alert-info">
				<div class="panel-heading">
					<h3 class="panel-title">
						{{ Session::get('message') }}
						<span class="pull-right">
							<a href="#" class="panel-close" style="color:#2E2E2E;"><strong><i class="fa fa-times"></i></strong></a>
						</span>
					</h3>
				</div>
			</div>
		@elseif(Session::has('warning'))
			<div class="alert alert-warning">
				<div class="panel-heading">
					<h3 class="panel-title">
						{{ Session::get('warning') }}
						<span class="pull-right">
							<a href="#" class="panel-close" style="color:#2E2E2E;"><strong><i class="fa fa-times"></i></strong></a>
						</span>
					</h3>
				</div>
			</div>
		@elseif(Session::has('danger'))
			<div class="alert alert-danger">
				<div class="panel-heading">
					<h3 class="panel-title">
						{{ Session::get('danger') }}
						<span class="pull-right">
							<a href="#" class="panel-close" style="color:#2E2E2E;"><strong><i class="fa fa-times"></i></strong></a>
						</span>
					</h3>
				</div>
			</div>
		@endif

		@yield('content')

	</body>
</html>