@section('content')
	
	@if(Session::get('idioma') == 1)

		<div class="container" style="margin-top:100px;margin-bottom:100px;text-align:center;">
			<div class="row">
				<h1>
					Desculpe!
				</h1>
				<h2>
					Página Não Encontrada!
				</h2>
			</div>
		</div>
	@else

		<div class="container" style="margin-top:100px;margin-bottom:100px;text-align:center;">
			<div class="row">
				<h1>
					Sorry!
				</h1>
				<h2>
					Page Not Found!
				</h2>
			</div>
		</div>

	@endif

@stop