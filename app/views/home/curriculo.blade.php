@section('content')

<div class="divbreadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
                    <li><a href="{{URL::to('/')}}">Home</a></li>
                    <li class="active">Curriculo</li>
                </ol>
            </div>
        </div><!--fecha row-->
    </div>
</div>
<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-sm-12 col-md-8 box_conteudo">
            <h1>{{$pagina[0]->titulo}}</h1>
            {{ Form::open(array('url' => 'formCurriculo', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateCurriculo', 'role' => 'form','enctype' => "multipart/form-data" )) }}
                <input type="hidden" value="{{$pagina[0]->slug}}" name="slug">
                <fieldset> 
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label">Nome <span style="color:red">*</span></label>
                        <div class="col-sm-9">
                            <input id="nome" name="nome" value="{{Session::get('nome')}}" type="text" class="form-control"> 
                        </div>
                    </div>   
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label">Email <span style="color:red">*</span></label>
                        <div class="col-sm-9">
                            <input id="email" name="email" value="{{Session::get('email')}}" type="text" class="form-control"> 
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label">Telefone <span style="color:red">*</span></label>
                        <div class="col-sm-3">
                            <input id="tel" name="tel" type="text" value="{{Session::get('tel')}}"  class="form-control"> 
                        </div>
                    </div>  
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label">Cargo pretendido <span style="color:red">*</span></label>
                        <div class="col-sm-9">
                         	<input id="depto" name="depto" value="{{Session::get('depto')}}" type="text" class="form-control"> 
                        </div>
                    </div>  
                     <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label">Situação <span style="color:red">*</span></label>
                        <div class="col-sm-3">
                            <select name="situacao" class="form-control">
                                <option value="Estagio">Estágio</option>
                                <option value="Formado">Formado</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label">Arquivo <span style="color:red">*</span></label>
                        <div class="col-sm-9">
                           <input type="file" id="arquivo" name="arquivo" class="form-control" />
                           <span class="help-block">Formato: Word ou PDF</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-md-3 control-label">Código verificador <span style="color:red">*</span></label>
                            <div class="col-sm-3">
                            	Quanto é {{$v1}} + {{$v2}}?
                                <input id="soma" name="soma" maxlength="2" type="text" class="form-control"> 
                            </div>
                        </div>
                    <div class="col-sm-9 pull-right">
                      {{ Form::submit('Enviar', array('class' => 'btn btn-primary input-medium', 'id' => 'contact-submit')) }}
                    </div>
                </fieldset>
            {{ Form::close() }}

            {{Session::forget('nome')}}
            {{Session::forget('email')}}
            {{Session::forget('tel')}}
            {{Session::forget('depto')}}
        </div>
    </div> <!-- /fecha row -->
</div> <!-- /fecha container -->

@stop