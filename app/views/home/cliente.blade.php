@section('content')

<div class="divbreadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
          <li><a href="{{URL::to('/')}}">Home</a></li>
          @if(Session::get('idioma') == 1)
            <li><a>Clientes</a></li>
          @else
            <li><a>Clients</a></li>
          @endif
          <li class="active">{{$cliente[0]->nome}}</li>
        </ol>
      </div>
    </div><!--fecha row-->
  </div>
</div>
<div class="container">
	<!-- Example row of columns -->
	<div class="row">
    <div class="col-sm-8 box_conteudo clientes">
      @if(Session::get('idioma') == 1)
        <h1 class="h1_secao">Clientes</h1>
      @else
        <h1 class="h1_secao">Clients</h1>
      @endif
      <h1>{{$cliente[0]->nome}}</h1>
      {{$cliente[0]->conteudo}}
      <h2>Equipe de Atendimento</h2>
        {{$cliente[0]->atendimento}}
    </div>
    <div class="col-sm-4">
      <div class="col-md-12 clientebox">
        <a href="{{URL::to($cliente[0]->site)}}" target="_blank">
          <img src='{{URL::to("assets/img/clientes/".$cliente[0]->arquivo)}}'>
        </a>
      </div>
      @if(isset($noticias[0]->titulo))
        <h2>Releases</h2>
      @endif
      <ul class="noticias">
        @foreach($noticias as $noticia)
          <li><strong>{{date("d/m/Y", strtotime($noticia->publicacao))}}</strong><br>
            @if(Session::get('idioma') == 1)
              <a href='{{URL::to("noticia/$noticia->slug")}}'>{{$noticia->titulo}}</a>
            @else
              <a href='{{URL::to("new/$noticia->slug")}}'>{{$noticia->titulo}}</a>
            @endif
          </li>
        @endforeach
      </ul> 
    </div>
  </div> <!-- /fecha row -->
</div> <!-- /fecha container -->

@stop