@section('content')

<div class="divbreadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					@if(Session::get('idioma') == 1)
						<li class="active">Serviços</li>
					@else
						<li class="active">Services</li>
					@endif
				</ol>
			</div>
		</div><!--fecha row-->
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-10 box_conteudo">
			<h1>{{$pagina[0]->titulo}}</h1>
			<?php $flag = 1 ?>
			@foreach($servicos as $servico)
				@if(Session::get('idioma') == 1)
					<a href='{{URL::to("servico/$servico->slug")}}'>
				@else
					<a href='{{URL::to("service/$servico->slug")}}'>
				@endif
					<div class="col-xs-6 col-sm-3 boxservicos">
						<img src='{{URL::to("assets/img/servicos/timthumb.php?src=$servico->arquivo&a=t&h=145")}}' width="100%" alt="" style="max-height:131px;">
						<span class="servico cor{{$flag}}">
							{{$servico->titulo}}
						</span>
					</div>
				</a>
				@if($flag == 10)
					<?php $flag = 1 ?>
				@else
					<?php $flag++ ?>
				@endif
			@endforeach
		</div> <!-- /fecha box_conteudo -->
	</div> <!-- /fecha row -->
</div> <!-- /fecha container -->

@stop