@section('content')

<div class="divbreadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					@if(Session::get('idioma') == 1)
						<li><a href='{{URL::to("noticias/".$secao[0]->slug."")}}'>Notícias</a></li>
					@else
						<li><a href='{{URL::to("news/".$secao[0]->slug."")}}'>News</a></li>
					@endif
					<li class="active">{{$cliente[0]->nome}}</li>
				</ol>
			</div>
		</div><!--fecha row-->
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-9 box_conteudo">
			@if(Session::get('idioma') == 1)
				<h1 class="h1_secao">Notícias</h1>
			@else
				<h1 class="h1_secao">News</h1>
			@endif
			<h1>{{$cliente[0]->nome}}</h1>
			@foreach($noticias as $noticia)
				<div class="boxnoticias">
					<h3 class="h3_secao">
						{{date("d/m/Y", strtotime($noticia->publicacao))}} 
					</h3>
					<h2 class="case">{{$noticia->titulo}}</h2>
					<p>{{$noticia->resumo}}</p>
					@if(Session::get('idioma') == 1)
						<a class="leiamais" href='{{URL::to("noticia/$noticia->slug")}}'>Veja mais <b class="glyphicon glyphicon-plus-sign"></b></a>
					@else
						<a class="leiamais" href='{{URL::to("new/$noticia->slug")}}'>Veja mais <b class="glyphicon glyphicon-plus-sign"></b></a>
					@endif
				</div>
			@endforeach
			{{$noticias->links()}}
		</div>
		<div class="col-md-3">
			<ul class="submenu">
				@foreach($clientes as $cliente)
					@if(Session::get('idioma') == 1)
						<li><a href='{{URL::to("noticias-cliente/$cliente->slug")}}'>{{$cliente->nome}}</a></li>
					@else
						<li><a href='{{URL::to("news-client/$cliente->slug")}}'>{{$cliente->nome}}</a></li>
					@endif
				@endforeach
			</ul>
		</div>
	</div> <!-- /fecha row -->
</div><!-- /fecha container -->

@stop