@section('content')

<div class="divbreadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					@if(Session::get('idioma') == 1)
						<li><a href='{{URL::to("servicos/".$secao[0]->slug."")}}'>Serviços</a></li>
					@else
						<li><a href='{{URL::to("services/".$secao[0]->slug."")}}'>Services</a></li>
					@endif
					<li class="active">{{$servico[0]->titulo}}</li>
				</ol>
			</div>
		</div><!--fecha row-->
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-8 box_conteudo">  
			<h1>{{$servico[0]->titulo}}</h1>
			{{$servico[0]->conteudo}}
		</div>
		<div class="col-sm-4">
			@if($servico[0]->arquivo)
				<?php $arquivo = $servico[0]->arquivo ?>
				<img src='{{URL::to("assets/img/servicos/timthumb.php?src=$arquivo&a=t&h=230")}}' width="100%" alt="">
			@endif
<!--
			@if(isset($cases[0]->slug))
				@if(Session::get('idioma') == 1)
					<h2>Cases</h2>
				@else
					<h2>Cases</h2>
				@endif
			@endif
			@foreach($cases as $case)
				<a href='{{URL::to("case/$case->slug")}}'>
					<img src='{{URL::to("assets/img/timthumb.php?src=cases/$case->arquivo&a=t&w=75&h=75")}}' class="pull-left media-object">
				</a>
			@endforeach
            -->
		</div>
        
	</div> <!-- /fecha row -->
</div> <!-- /fecha container -->

@stop