@section('content')

<div class="divbreadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					@if(Session::get('idioma') == 1)
						<li><a href='{{URL::to("noticias/".$secao[0]->slug."")}}'>Notícias</a></li>
					@else
						<li><a href='{{URL::to("news/".$secao[0]->slug."")}}'>News</a></li>
					@endif
					<li class="active">{{$noticia[0]->titulo}}</li>
				</ol>
			</div>
		</div><!--fecha row-->
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-9">
			<div class="box_conteudo">
				<h3 class="h3_secao">{{date("d/m/Y", strtotime($noticia[0]->publicacao))}} | {{$noticia[0]->cliente}}</h3>
				<h1>{{$noticia[0]->titulo}}</h1>
				{{$noticia[0]->conteudo}}
			</div>
			@if(isset($imagens[0]->link) || isset($videos[0]->link))
				@if(Session::get('idioma') == 1)
					<h2>Galeria de fotos</h2>
				@else
					<h2>Photo gallery</h2>
				@endif
			@endif
			@if(isset($imagens[0]->link))
				<div class="row">
					<ul class="galeria" id="galeria">
						@foreach($imagens as $imagem)
							<li>
								<a href='{{URL::to("assets/img/galeria/$imagem->link")}}' rel="prettyPhoto[galeria]">
									<img src='{{URL::to("assets/img/timthumb.php?src=galeria/$imagem->link&a=t&w=75&h=75")}}' class="pull-left media-object">
								</a>
							</li>
						@endforeach
					</ul>
				</div>
				<div class="holder" style="text-align:center;">
				</div>
			@endif
			@if(isset($videos[0]->link))
				<div>
					@foreach($videos as $video)
						<iframe src="{{$video->link}}" width="49%" height="230" frameborder="0" allowfullscreen></iframe>
					@endforeach
				</div>
			@endif
		</div>
		<div class="col-md-3">
			<ul class="submenu">
				@foreach($clientes as $cliente)
					@if(Session::get('idioma') == 1)
						<li><a href='{{URL::to("noticias-cliente/$cliente->slug")}}'>{{$cliente->nome}}</a></li>
					@else
						<li><a href='{{URL::to("news-client/$cliente->slug")}}'>{{$cliente->nome}}</a></li>
					@endif
				@endforeach
			</ul>
		</div>
	</div> <!-- /fecha row -->
</div> <!-- /fecha container -->

@stop