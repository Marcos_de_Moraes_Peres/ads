@section('content')

<div class="divbreadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					<li class="active">Cases</li>
				</ol>
			</div>
		</div><!--fecha row-->
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12 box_conteudo clientes">
			<h1>{{$pagina[0]->titulo}}</h1>
			@foreach($cases as $case)
				<div class="col-xs-6 col-sm-3 col-md-2">
					<a href='{{URL::to("case/$case->slug")}}'>
						<img src='{{URL::to("assets/img/cases/$case->arquivo")}}' class="img-responsive"> 
					</a>
				</div>	
			@endforeach
		</div>
	</div> <!-- /fecha row -->
</div> <!-- /fecha container -->

@stop