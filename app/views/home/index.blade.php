@section('content')

<div class="bannercontainer">
  <div class="banner">
    <ul>
      @foreach($slides as $slide)
        <li data-transition="boxfade" data-slotamount="10">
          <img src='{{URL::to("assets/img/slides/$slide->arquivo")}}' />
          <div class="caption lfb big_white1" data-x="5" data-y="80" data-speed="900" data-start="1700" data-easing="easeOutBack"><span>{{$slide->descricao}}</span></div>
        </li>
      @endforeach
    </ul>
    <div class="tp-bannertimer"></div>
  </div>
</div>
 
<div class="container box_news_home">
  <!-- Example row of columns -->
  <div class="row">

    @foreach($destaqueFixo as $destaque)
      <div class="col-sm-6 col-md-4 box_news"> 
        <img src='{{URL::to("assets/img/destaques/$destaque->arquivo")}}'>
        <h2>{{$destaque->titulo}}</h2>
        <p>{{$destaque->descricao}}</p>
        @if(Session::get('idioma') == 1)
          <p><a class="leiamais" href='{{URL::to("$destaque->link")}}'>Veja mais <b class="glyphicon glyphicon-plus-sign"></b></a></p>
        @else
          <p><a class="leiamais" href='{{URL::to("$destaque->link")}}'>See more <b class="glyphicon glyphicon-plus-sign"></b></a></p>
        @endif
      </div>
    @endforeach

    @foreach($destaqueAleatorio as $destaque)
      <div class="col-sm-6 col-md-4 box_news"> 
        <img src='{{URL::to("assets/img/destaques/$destaque->arquivo")}}'>
        <h2>{{$destaque->titulo}}</h2>
        <p>{{$destaque->descricao}}</p>
        @if(Session::get('idioma') == 1)
          <p><a class="leiamais" href='{{URL::to("$destaque->link")}}'>Veja mais <b class="glyphicon glyphicon-plus-sign"></b></a></p>
        @else
          <p><a class="leiamais" href='{{URL::to("$destaque->link")}}'>See more <b class="glyphicon glyphicon-plus-sign"></b></a></p>
        @endif
      </div>
    @endforeach
    
    <div class="col-md-4 box_news">
      @if(Session::get('idioma') == 1)
        <h2 style="margin:0;">Notícias</h2>
      @else
        <h2 style="margin:0;">News</h2>
      @endif
      <ul class="noticias">
        @foreach($noticias as $noticia)
          <li><strong>{{date("d/m/Y", strtotime($noticia->publicacao))}}</strong><br>
          @if(Session::get('idioma') == 1)
            <a href='{{URL::to("noticia/$noticia->slug")}}'>{{$noticia->titulo}}</a></li>
          @else
            <a href='{{URL::to("new/$noticia->slug")}}'>{{$noticia->titulo}}</a></li>
          @endif
        @endforeach
      </ul>
    </div>
  </div>
</div> <!-- /container -->

@stop