@section('content')

<div class="divbreadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb hidden-xs hidden-sm">
          <li><a href="{{URL::to('/')}}">Home</a></li>
          <li class="active">{{$pagina[0]->menu}}</li>       
          <li class="active">{{$pagina[0]->titulo}}</li>
        </ol>
      </div>
    </div><!--fecha row-->
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-sm-12 col-md-8 box_conteudo">
      <h1>{{$pagina[0]->titulo}}</h1>
      {{$pagina[0]->conteudo}}
    </div>
    @if($pagina[0]->arquivo)
      <div class="col-sm-4 col-md-4">
        <img src='{{URL::to("assets/img/timthumb.php?src=paginas/".$pagina[0]->arquivo."&a=t&h=230")}}' width="100%" alt="">
      </div>
    @endif
  </div> <!-- /fecha row -->
</div> <!-- /fecha container -->

@stop