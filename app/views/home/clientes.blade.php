@section('content')

<div class="divbreadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					@if(Session::get('idioma') == 1)
						<li class="active">Clientes</li>
					@else
						<li class="active">Clients</li>
					@endif
					<li class="active">{{$pagina[0]->titulo}}</li>
				</ol>
			</div>
		</div><!--fecha row-->
	</div>
</div>
<div class="container">
	<!-- Example row of columns -->
	<div class="row">
		<div class="col-md-12 box_conteudo clientes">
			<h1>{{$pagina[0]->titulo}}</h1>
			@foreach($clientes as $cliente)
				<div class="col-xs-6 col-sm-3 col-md-2">
					@if(Session::get('idioma') == 1)
						<a href='{{URL::to("cliente/$cliente->slug")}}'>
					@else
						<a href='{{URL::to("client/$cliente->slug")}}'>
					@endif
						<img src='{{URL::to("assets/img/clientes/$cliente->arquivo")}}' class="img-responsive">
					</a>
				</div>
			@endforeach
		</div>
	</div> <!-- /fecha row -->
	<div class="row">
		<div class="col-md-12">
        	<a href="../pagina/segmentos-atendidos">Segmentos atendidos <b class="glyphicon glyphicon-plus-sign"></b></a>
        </div>
    </div>
</div> <!-- /fecha container -->

@stop