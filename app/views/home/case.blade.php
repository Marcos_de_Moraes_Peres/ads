@section('content')

<div class="divbreadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					<li><a href='{{URL::to("cases/".$secao[0]->slug."")}}'>Cases</a></li>
					<li class="active">{{$case[0]->titulo}}</li>
				</ol>
			</div>
		</div><!--fecha row-->
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-md-9 box_conteudo">
			<h1 class="h1_secao">Cases</h1>
			<h1>{{$case[0]->cliente}}<br />
			{{$case[0]->titulo}}</h1>
			{{$case[0]->conteudo}} 
        </div>
		
        <div class="col-sm-4 col-md-3">	
          <div class="col-md-12 clientebox">
            <img src='{{URL::to("assets/img/cases/".$case[0]->arquivo)}}'>        
          </div>
      
			@if(isset($imagens[0]->link) || isset($videos[0]->link))
				@if(Session::get('idioma') == 1)
					<h2>Galeria de fotos</h2>
				@else
					<h2>Photo gallery</h2>
				@endif
			@endif
			@if(isset($imagens[0]->link))
				<div class="row">
					<ul class="galeria" id="galeria" style="padding:4.4%;">
						@foreach($imagens as $imagem)
							<li>
								<a href='{{URL::to("assets/img/galeria/$imagem->link")}}' rel="prettyPhoto[galeria]">
									<img src='{{URL::to("assets/img/timthumb.php?src=galeria/$imagem->link&a=t&w=75&h=75")}}' class="pull-left media-object">
								</a>
							</li>
						@endforeach
					</ul>
				</div>
				<div class="holder" style="text-align:center;">
				</div>
			@endif
			@if(isset($videos[0]->link))
				<div>
					@foreach($videos as $video)
						<iframe src="{{$video->link}}" width="100%" height="230" frameborder="0" allowfullscreen></iframe>
					@endforeach
				</div>
			@endif
		</div>
	</div> <!-- /fecha row -->
    <div class="row">
    	<div class="col-xs-12" style="margin-top:20px;"> 
				@if(Session::get('idioma') == 1)
        	<a href='{{URL::to("cases/".$secao[0]->slug."")}}'> <b class="glyphicon glyphicon-chevron-left"></b> Outros Cases</a>
				@else
        	<a href='{{URL::to("cases/".$secao[0]->slug."")}}'> <b class="glyphicon glyphicon-chevron-left"></b> Others cases</a>
				@endif 
        </div>
    </div>
</div> <!-- /fecha container -->

@stop