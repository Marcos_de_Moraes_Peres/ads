@section('content')

<div class="divbreadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					@if(Session::get('idioma') == 1)
						<li class="active">Contato</li>
					@else
						<li class="active">Contact</li>
					@endif
				</ol>
			</div>
		</div><!--fecha row-->
	</div>
</div>
<div class="container">
	<!-- Example row of columns -->
	<div class="row">
		<div class="col-sm-12 col-md-8 box_conteudo">
			<h1>{{$pagina[0]->titulo}}</h1>
			{{ Form::open(array('url' => 'formContato', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateForm', 'role' => 'form','enctype' => "multipart/form-data" )) }}
				<fieldset>
					<input type="hidden" value="{{$pagina[0]->slug}}" name="slug">
					@if(Session::get('idioma') == 1)
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Nome <span style="color:red">*</span></label>
							<div class="col-sm-9">
								<input id="nome" name="nome" value="{{Session::get('nome')}}" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Empresa</label>
							<div class="col-sm-9">
								<input id="empresa" name="empresa" value="{{Session::get('empresa')}}" type="text" class="form-control"> 
							</div>
						</div>  
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Email <span style="color:red">*</span></label>
							<div class="col-sm-9">
								<input id="email" name="email" value="{{Session::get('email')}}" type="text" class="form-control"> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Telefone <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<input id="tel" name="tel" value="{{Session::get('tel')}}" type="text" class="form-control"> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Assunto <span style="color:red">*</span></label>
							<div class="col-sm-9">
								<input id="assunto" name="assunto" value="{{Session::get('assunto')}}" type="text" class="form-control"> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Mensagem <span style="color:red">*</span></label>
							<div class="col-sm-9">
								<textarea id="mensagem" name="mensagem" class="form-control" rows="5">{{Session::get('mensagem')}}</textarea>
							</div>
						</div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 control-label">Código verificador <span style="color:red">*</span></label>
                                <div class="col-sm-3">
                                    Quanto é {{$v1}} + {{$v2}}?
                                    <input id="soma" name="soma" maxlength="2" type="text" class="form-control"> 
                                </div>
                        </div>
						<div class="col-sm-9 pull-right">
							{{ Form::submit('Enviar', array('class' => 'btn btn-primary input-medium', 'id' => 'contact-submit')) }}
						</div>
					@else
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Name <span style="color:red">*</span></label>
							<div class="col-sm-9">
								<input id="nome" name="nome" type="text" value="{{Session::get('nome')}}" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Business</label>
							<div class="col-sm-9">
								<input id="empresa" name="empresa" value="{{Session::get('empresa')}}" type="text" class="form-control"> 
							</div>
						</div>  
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Email <span style="color:red">*</span></label>
							<div class="col-sm-9">
								<input id="email" name="email" value="{{Session::get('email')}}" type="text" class="form-control"> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Phone <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<input id="tel" name="tel" type="text" value="{{Session::get('tel')}}" class="form-control"> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Subject <span style="color:red">*</span></label>
							<div class="col-sm-9">
								<input id="assunto" name="assunto" value="{{Session::get('assunto')}}" type="text" class="form-control"> 
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Message <span style="color:red">*</span></label>
							<div class="col-sm-9">
								<textarea id="mensagem" name="mensagem" class="form-control" rows="5">{{Session::get('mensagem')}}</textarea>
							</div>
						</div>
                        <div class="form-group">
                            <label class="col-sm-3 col-md-3 control-label">Code checker <span style="color:red">*</span></label>
                                <div class="col-sm-3">
                                    How much is {{$v1}} + {{$v2}}?
                                    <input id="soma" name="soma" maxlength="2" type="text" class="form-control"> 
                                </div>
                        </div>
						<div class="form-group">
							<label class="col-sm-3 col-md-3 control-label">Soma: {{$v1}} + {{$v2}} <span style="color:red">*</span></label>
							<div class="col-sm-3">
								<input id="soma" name="soma" maxlength="2" type="text" class="form-control"> 
							</div>
						</div>
						<div class="col-sm-9 pull-right">
							{{ Form::submit('Submit', array('class' => 'btn btn-primary input-medium', 'id' => 'contact-submit')) }}
						</div>
					@endif
				</fieldset>
			{{ Form::close() }}

			{{Session::forget('nome')}}
			{{Session::forget('empresa')}}
			{{Session::forget('email')}}
			{{Session::forget('tel')}}
			{{Session::forget('assunto')}}
			{{Session::forget('mensagem')}}
		</div>
	</div> <!-- /fecha row -->
</div> <!-- /fecha container -->

@stop