@section('content')


<div class="divbreadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					@if(Session::get('idioma') == 1)
						<li class="active">Ads em Ação</li>
					@else
						<li class="active">Ads in Action</li>
					@endif
				</ol>
			</div>
		</div><!--fecha row-->
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-8 box_conteudo">
			@if(Session::get('idioma') == 1)
				<h1>Ads em Ação</h1>
			@else
				<h1>Ads in Action</h1>
			@endif
			@foreach($bolhetins as $bolhetim)
				<div class="col-xs-6 col-sm-4 col-md-4" style="text-align:center">
					@if(Session::get('idioma') == 1)
						<a href='{{URL::to("ads-em-acao-show/$bolhetim->slug")}}' target="_blank">
					@else
						<a href='{{URL::to("ads-in-action-show/$bolhetim->slug")}}' target="_blank">
					@endif
						<img class="img-responsive img-thumbnail" src="{{URL::to('assets/img/acao/'.$bolhetim->arquivo)}}" /> {{$bolhetim->titulo}}
					</a>
				</div>
			@endforeach
		</div>
	</div> <!-- /fecha row -->
</div> <!-- /fecha container -->

@stop