@section('content')
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	 

</head>
<body style="font-size: 12px; margin: 0; padding: 0; font-family: Arial, sans-serif; line-height: 22px; color: #555555; width: 100%;" bgcolor="#f4f4f4">

 <table class="wrapper" cellspacing="0" style="font-size: 12px; font-family: Arial, sans-serif; line-height: 22px; color: #555555; table-layout: fixed;" width="100%" cellpadding="0"><tr><td style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" bgcolor="#f4f4f4"> 	
 
		
	<br>	
 
	 
	
	<table class="main-content-wrap" rules="none" cellspacing="0" border="0" frame="border" align="center" style="font-size: 12px; border-color: #e3e3e3 #e3e3e3 #e3e3e3 #e3e3e3; border-collapse: collapse; background-color: #ffffff; font-family: Arial, sans-serif; line-height: 22px; color: #555555; border-spacing: 0; border-width: 10px 0px 0px 0px; border-style: solid solid solid solid;" bgcolor="#ffffff" width="600" cellpadding="0">
	<tr><td class="header" valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;">
		 
				
		<table cellspacing="0" align="center" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;background:#e3e3e3" width="600" cellpadding="0">
			<tr>
				<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="260">
                <p style="font-size: 10px; font-family: Arial, sans-serif; color: #555555; margin: 2px; padding-left: 30px;">{{$boletim[0]->mes}} {{$boletim[0]->ano}}</p>
                </td> 
				<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="260">
                <p style="font-size: 10px; font-family: Arial, sans-serif; color: #555555; margin: 2px; padding-right: 30px; text-align:right;">www.adsbrasil.com.br</p>
                </td> 
				
			</tr>
		</table> 
		
		<table cellspacing="0" align="center" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555; margin-top:20px" width="540" cellpadding="0">
			<tr>

				<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="260">
				
					<img class="block" src="{{URL::to('assets/img/boletim/logo.jpg')}}" border="0" alt="" style="border: none; display: block;" />
		
				</td> 
                
				<td style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="20">&nbsp;</td>
				<td class="ourwebsite" align="right" valign="top" style="font-size: 12px; font-family: Arial, sans-serif; line-height: 16px; color: #555555;" width="260">
				
					<img class="block" src="{{URL::to('assets/img/boletim/emacao.jpg')}}" border="0" alt="" style="border: none; display: block;" />
				
				</td>
			</tr>

		</table> 
		
		<img class="block" src="{{URL::to('assets/img/boletim/divider-600x61.gif')}}" border="0" height="31" alt="" style="border: none; display: block;" width="600" />
			 
	</td></tr>	
	<tr><td class="content" id="iframe_module_content" valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;">
		 
		
		<!-- Module #1 | 1 col, 540px -->
		<table cellspacing="0" align="center" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="540" cellpadding="0">
			<tr>

				
				<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;"><a href="{{URL::to('acao-show/'.$noticia1[0]->slug)}}"><img class="block" src="{{URL::to('assets/img/acao/'.$noticia1[0]->arquivo)}}" alt="" style="border: none; display: block;" width="540" /></a>
				
				</td>
				
			</tr>	
		</table> 

		<img class="block" src="{{URL::to('assets/img/boletim/divider-600x31-2.gif')}}" border="0" height="31" alt="" style="border: none; display: block;" width="600" />
 

		<table cellspacing="0" align="center" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="540" cellpadding="0">
			<tr>
			
				<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="166">
				
					<h1 style="font-size: 24px; font-weight: bold; margin: 0 0 8px; padding: 0; line-height: 30px; color: #252525; letter-spacing: -1px;"><span style="color: #252525;">{{$noticia1[0]->titulo}}</span></h1>
		
				</td>
				<td style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="20">&nbsp;</td>
				<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="352">
				
					<p style="font-size: 12px; font-family: Arial, sans-serif; line-height: 22px; color: #555555; margin: 0 0 12px; padding: 0;">{{$noticia1[0]->resumo}}<br>
 
<a href="{{URL::to('acao-show/'.$noticia1[0]->slug)}}" style="text-decoration: underline; color: #333;">Leia mais</a></p>
				
				</td>
				
			</tr>
		</table> 

		<img class="block" src="{{URL::to('assets/img/boletim/divider-600x61.gif')}}" border="0" height="31" alt="" style="border: none; display: block;" width="600" />
 
		<table cellspacing="0" align="center" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="540" cellpadding="0">
			<tr>
				
				<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;"><a href="{{URL::to('acao-show/'.$noticia2[0]->slug)}}"><img class="block" src="{{URL::to('assets/img/acao/'.$noticia2[0]->arquivo)}}" alt="" style="border: none; display: block;" width="540" /></a>
				
				</td>
				
			</tr>	
		</table> 

		<img class="block" src="{{URL::to('assets/img/boletim/divider-600x31-2.gif')}}" border="0" height="31" alt="" style="border: none; display: block;" width="600" />
 
		<table cellspacing="0" align="center" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="540" cellpadding="0">
			<tr>
			
				<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="166">
				
					<h1 style="font-size: 24px; font-weight: bold; margin: 0 0 8px; padding: 0; line-height: 30px; color: #252525; letter-spacing: -1px;"><span style="color: #252525;">{{$noticia2[0]->titulo}}</span></h1>
		
				</td>

				<td style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="20">&nbsp;</td>
				<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="352">
				
					<p style="font-size: 12px; font-family: Arial, sans-serif; line-height: 22px; color: #555555; margin: 0 0 12px; padding: 0;">{{$noticia2[0]->resumo}}<br><a href="{{URL::to('acao-show/'.$noticia2[0]->slug)}}" style="text-decoration: underline; color: #333;">Leia mais</a>
					</p>
				
				</td>
				
			</tr>
		</table>
 

		<img class="block" src="{{URL::to('assets/img/boletim/divider-600x61.gif')}}" border="0" height="31" alt="" style="border: none; display: block;" width="600" />  

		<table cellspacing="0" align="center" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="540" cellpadding="0">
			<tr>
				
				<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="260">
				
					<p style="font-size: 12px; font-family: Arial, sans-serif; line-height: 22px; color: #555555; margin: 0 0 12px; padding: 0;"><a href="{{URL::to('acao-show/'.$noticia3[0]->slug)}}"><img src="{{URL::to('assets/img/acao/'.$noticia3[0]->arquivo)}}" height="180" alt="" style="border: none;" width="260" /></a></p>
					
				  <h2 style="font-weight: bold; font-size: 18px; line-height: 24px; margin: 0 0 10px; color: #252525; padding: 0;"><span style="color: #252525;">{{$noticia3[0]->titulo}}</span></h2>
					
					<p style="font-size: 12px; font-family: Arial, sans-serif; line-height: 22px; color: #555555; margin: 0 0 12px; padding: 0;">{{$noticia3[0]->resumo}}<br>
<a href="{{URL::to('acao-show/'.$noticia3[0]->slug)}}" style="text-decoration: underline; color: #333;">Leia mais</a></p>
		
				</td>
				<td style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="20">&nbsp;</td>
				<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="260">
				
					<p style="font-size: 12px; font-family: Arial, sans-serif; line-height: 22px; color: #555555; margin: 0 0 12px; padding: 0;"><a href="{{URL::to('acao-show/'.$noticia4[0]->slug)}}"><img src="{{URL::to('assets/img/acao/'.$noticia4[0]->arquivo)}}" height="180" alt="" style="border: none;" width="260" /></a></p>
					
				  <h2 style="font-weight: bold; font-size: 18px; line-height: 24px; margin: 0 0 10px; color: #252525; padding: 0;"><span style="color: #252525;">{{$noticia4[0]->titulo}}</span></h2>

					
				  <p style="font-size: 12px; font-family: Arial, sans-serif; line-height: 22px; color: #555555; margin: 0 0 12px; padding: 0;">{{$noticia4[0]->resumo}}
                  <br><a href="{{URL::to('acao-show/'.$noticia4[0]->slug)}}" style="text-decoration: underline; color: #333;">Leia mais</a>
				  </p>
					
				</td>
				
			</tr>

		</table>
        
	</td></tr>
	<tr><td class="footer" valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;">
	 
		
		<img class="block" src="{{URL::to('assets/img/boletim/divider-600x31-2.gif')}}" border="0" height="31" alt="" style="border: none; display: block;" width="600" />
		
		<table cellspacing="0" align="center" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" bgcolor="#f0f0f0" width="600" cellpadding="0">

			<tr>
				<td class="footer-content" valign="top" style="font-size: 12px; border-top-color: #D4D6D4; font-family: Arial, sans-serif; line-height: 22px; color: #555555; border-top-width: 1px; border-top-style: solid;">
						
					<img class="block" src="{{URL::to('assets/img/boletim/footer-divider-600x16.gif')}}" border="0" height="16" alt="" style="border: none; display: block;" width="600" />
					
					<table cellspacing="0" align="center" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="540" cellpadding="0">
						<tr>
							<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="402">
							
								<h4 style="font-weight: bold; font-size: 12px; line-height: 18px; margin: 0 0 14px; color: #252525; padding: 0;"><span style="color: #252525;">Sobre a ADS</span></h4>
								
								A ADS Comunicação Corporativa é uma das mais respeitadas agências de Relações Públicas no Brasil, com mais de 40 anos de experiência na elaboração e implementação de programas de comunicação corporativa e de marketing para empresas nacionais e internacionais.
								<a href="http://www.adsbrasil.com.br/" style="text-decoration: underline; color: #333;">Saiba mais</a>

								
							</td>
							 
							<td style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="20">&nbsp;</td>
							<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="96">
							
								<h4 style="font-weight: bold; font-size: 12px; line-height: 18px; margin: 0 0 14px; color: #252525; padding: 0;"><span style="color: #252525;">Redes sociais</span></h4>
								
							<a href="https://twitter.com/ADSBrasil"><img src="{{URL::to('assets/img/boletim/social1.png')}}" border="0" alt="" style="border: none;" /></a>
							<a href="https://www.facebook.com/adscomunicacao?fref=ts"><img src="{{URL::to('assets/img/boletim/social2.png')}}" border="0" alt="" style="border: none;" /></a></td>

						</tr>
					</table>
					
					<img class="block" src="{{URL::to('assets/img/boletim/footer-divider-600x31.gif')}}" border="0" height="31" alt="" style="border: none; display: block;" width="600" />
					 
					<table cellspacing="0" align="center" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555;" width="540" cellpadding="0">
						<tr>
							<td valign="top" style="font-size: 12px; line-height: 22px; font-family: Arial, sans-serif; color: #555555; text-align:center;"><b>ADS Comunicação Corporativa | </b> Rua Michigan, 69 - Brooklin - 04566-903 - São Paulo - SP<br>
						      T: 11 5090 3007 | 
						      F: 11 5090 3010 | E: <a href="mailto:contato@adsbrasil.com.br">contato@adsbrasil.com.br</a><br>
<br>
</td>
						</tr>	
					</table> 
					
					<img class="block" src="{{URL::to('assets/img/boletim/rodape.gif')}}" border="0" alt="" style="border: none; display: block;" width="600" />
					<img class="block" src="{{URL::to('assets/img/boletim/footer-divider-600x31-2.gif')}}" border="0" height="31" alt="" style="border: none; display: block;" width="600" />
					
				</td>
			</tr>

		</table>
		 
	
	</td></tr>
	</table>
	 
	
	<br><br><br>
		 	
</td></tr></table> 
</body>
</html>

@stop