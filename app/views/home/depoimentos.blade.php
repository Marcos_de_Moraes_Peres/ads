@section('content')

<div class="divbreadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					@if(Session::get('idioma') == 1)
						<li class="active">Depoimentos</li>
					@else
						<li class="active">Testimonials</li>
					@endif
				</ol>
			</div>
		</div><!--fecha row-->
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-10 box_conteudo">
			<h1>{{$pagina[0]->titulo}}</h1>
			<!-- inicio depoimento -->
			@foreach($depoimentos as $depoimento)
				<div class="media depoimento">
					@if($depoimento->imagem)
						<img src='{{URL::to("assets/img/depoimentos/$depoimento->imagem")}}' class="pull-left media-object">
					@else
						<img src='{{URL::to("assets/img/clientes/$depoimento->arquivo")}}' class="pull-left media-object">
					@endif
					<div class="media-body"> 
						<p>{{$depoimento->conteudo}}</p>
						<span class="depoimento_contato">{{$depoimento->nome}}</span>
					</div>
				</div>
			@endforeach
			<!-- fim depoimento -->
		</div>
	</div><!-- /fecha row -->
</div> <!-- /fecha container -->

@stop