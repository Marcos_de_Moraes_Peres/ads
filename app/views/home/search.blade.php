@section('content')

<div class="divbreadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb hidden-xs hidden-sm">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					@if(Session::get('idioma') == 1)
						<li class="active">Busca</li>
					@else
						<li class="active">Search</li>
					@endif
				</ol>
			</div>
		</div><!--fecha row-->
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			@if($paginas || $servicos || $cases || $clientes || $noticias)
				<ul class="noticias">
					@if($paginas)
						<h3>Páginas</h3>
						<hr />
						@foreach($paginas as $pagina)
							<li>
								<h2>{{$pagina->titulo}}</h2>
								<p>{{$pagina->resumo}}</p>
								<div class="breadcrumbs">
									@if(Session::get('idioma') == 1)
										<p>Ultima atualização em: {{date("d/m/Y", strtotime($pagina->updated_at))}}</p>
									@else
										<p>Last updated: {{date("m/d/Y", strtotime($pagina->updated_at))}}</p>
									@endif
								</div>
								@if(Session::get('idioma') == 1)
									<a style="color:#cf1328;" class="leiamais" href='{{URL::to("/$pagina->local/$pagina->slug")}}'>Leia mais <b class="glyphicon glyphicon-plus-sign"></b></a>
								@else
									<a style="color:#cf1328;" class="leiamais" href='{{URL::to("/$pagina->local/$pagina->slug")}}'>Read more <b class="glyphicon glyphicon-plus-sign"></b></a>
								@endif
							</li>
						@endforeach
					@endif
					@if($servicos)
						<h3>Serviços</h3>
						<hr />
						@foreach($servicos as $servico)
							<li>
								<h2>{{$servico->titulo}}</h2>
								<div class="breadcrumbs">
									@if(Session::get('idioma') == 1)
										<p>Ultima atualização em: {{date("d/m/Y", strtotime($servico->updated_at))}}</p>
									@else
										<p>Last updated: {{date("m/d/Y", strtotime($servico->updated_at))}}</p>
									@endif
								</div>
								@if(Session::get('idioma') == 1)
									<a style="color:#cf1328;" class="leiamais" href='{{URL::to("/$servico->local/$servico->slug")}}'>Leia mais <b class="glyphicon glyphicon-plus-sign"></b></a>
								@else
									<a style="color:#cf1328;" class="leiamais" href='{{URL::to("/$servico->local/$servico->slug")}}'>Read more <b class="glyphicon glyphicon-plus-sign"></b></a>
								@endif
							</li>
						@endforeach
					@endif
					@if($cases)
						<h3>Cases</h3>
						<hr />
						@foreach($cases as $case)
							<li>
								<h2>{{$case->titulo}}</h2>
								<p>{{$case->resumo}}</p>
								<div class="breadcrumbs">
									@if(Session::get('idioma') == 1)
										<p>Ultima atualização em: {{date("d/m/Y", strtotime($case->updated_at))}}</p>
									@else
										<p>Last updated: {{date("m/d/Y", strtotime($case->updated_at))}}</p>
									@endif
								</div>
								@if(Session::get('idioma') == 1)
									<a style="color:#cf1328;" class="leiamais" href='{{URL::to("/case/$case->slug")}}'>Leia mais <b class="glyphicon glyphicon-plus-sign"></b></a>
								@else
									<a style="color:#cf1328;" class="leiamais" href='{{URL::to("/case/$case->slug")}}'>Read more <b class="glyphicon glyphicon-plus-sign"></b></a>
								@endif
							</li>
						@endforeach
					@endif
					@if($clientes)
						<h3>Clientes</h3>
						<hr />
						@foreach($clientes as $cliente)
							<li>
								<h2>{{$cliente->nome}}</h2>
								<div class="breadcrumbs">
									@if(Session::get('idioma') == 1)
										<p>Ultima atualização em: {{date("d/m/Y", strtotime($cliente->updated_at))}}</p>
									@else
										<p>Last updated: {{date("m/d/Y", strtotime($cliente->updated_at))}}</p>
									@endif
								</div>
								@if(Session::get('idioma') == 1)
									<a style="color:#cf1328;" class="leiamais" href='{{URL::to("/cliente/$cliente->slug")}}'>Leia mais <b class="glyphicon glyphicon-plus-sign"></b></a>
								@else
									<a style="color:#cf1328;" class="leiamais" href='{{URL::to("/cliente/$cliente->slug")}}'>Read more <b class="glyphicon glyphicon-plus-sign"></b></a>
								@endif
							</li>
						@endforeach
					@endif
					@if($noticias)
						<h3>Notícias</h3>
						<hr />
						@foreach($noticias as $noticia)
							<li>
								<h2>{{$noticia->titulo}}</h2>
								<p>{{$noticia->resumo}}</p>
								<div class="breadcrumbs">
									@if(Session::get('idioma') == 1)
										<p>Ultima atualização em: {{date("d/m/Y", strtotime($noticia->updated_at))}} | {{$noticia->cliente}}</p>
									@else
										<p>Last updated: {{date("m/d/Y", strtotime($noticia->updated_at))}} | {{$noticia->cliente}}</p>
									@endif
								</div>
								@if(Session::get('idioma') == 1)
									<a style="color:#cf1328;" class="leiamais" href='{{URL::to("/noticia/$noticia->slug")}}'>Leia mais <b class="glyphicon glyphicon-plus-sign"></b></a>
								@else
									<a style="color:#cf1328;" class="leiamais" href='{{URL::to("/noticia/$noticia->slug")}}'>Read more <b class="glyphicon glyphicon-plus-sign"></b></a>
								@endif
							</li>
						@endforeach
					@endif
				</ul>
			@else
				@if(Session::get('idioma') == 1)
					<p style="margin-bottom:20%">Nenhum resultado encontrado!</p>
				@else
					<p style="margin-bottom:20%">No results found!</p>
				@endif
			@endif
		</div> 
	</div><!--/. fecha row -->
</div><!--/.fecha container -->

@stop