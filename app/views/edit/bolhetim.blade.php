@section('content')

<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Editar Boletim
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::model($new, array('route' => array('adminAcaoBolhetim.update', $new->id), 'method' => 'PUT','id' => 'validateBolhetimEdit','files' => true ,'class' => 'form-horizontal cascade-forms')) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('titulo',$new->titulo,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo'
							))

						}}
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Mês da Edição*</label>
					<div class="col-lg-3 col-md-9">
						<select name='mes' class='form-control form-cascade-control' id='mes'>
							@if('Janeiro' == $new->mes) <option value="Janeiro" selected>Janeiro</option> @else<option value="Janeiro">Janeiro</option>@endif
							@if('Fevereiro' == $new->mes) <option value="Fevereiro" selected>Fevereiro</option> @else<option value="Fevereiro">Fevereiro</option>@endif
							@if('Março' == $new->mes) <option value="Março" selected>Março</option> @else<option value="Março">Março</option>@endif
							@if('Abril' == $new->mes) <option value="Abril" selected>Abril</option> @else<option value="Abril">Abril</option>@endif
							@if('Maio' == $new->mes) <option value="Maio" selected>Maio</option> @else<option value="Maio">Maio</option>@endif
							@if('Junho' == $new->mes) <option value="Junho" selected>Junho</option> @else<option value="Junho">Junho</option>@endif
							@if('Julho' == $new->mes) <option value="Julho" selected>Julho</option> @else<option value="Julho">Julho</option>@endif
							@if('Agosto' == $new->mes) <option value="Agosto" selected>Agosto</option> @else<option value="Agosto">Agosto</option>@endif
							@if('Setembro' == $new->mes) <option value="Setembro" selected>Setembro</option> @else<option value="Setembro">Setembro</option>@endif
							@if('Outubro' == $new->mes) <option value="Outubro" selected>Outubro</option> @else<option value="Outubro">Outubro</option>@endif
							@if('Novembro' == $new->mes) <option value="Novembro" selected>Novembro</option> @else<option value="Novembro">Novembro</option>@endif
							@if('Dezembro' == $new->mes) <option value="Dezembro" selected>Dezembro</option> @else<option value="Dezembro">Dezembro</option>@endif
						</select>
					</div>
					<label class="col-lg-2 col-md-3 control-label">Ano da Edição*</label>
					<div class="col-lg-3 col-md-9">
						<select name='ano' class='form-control form-cascade-control' id='ano'>
							@while($base <= date('Y')+1)
								@if($base == $new->ano)
									<option value="{{$base}}" selected>{{$base}}</option>
								@else
									<option value="{{$base}}">{{$base}}</option>
								@endif
								{{$base++}}
							@endwhile
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Imagem*</label>
					<div class="col-lg-3 col-md-9">
						<input type="file" id="arquivo" name="arquivo" size="40">
					</div>
					<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
					<div class="col-lg-3 col-md-9">
						{{

							Form::text('slug',$new->slug,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'slug',
								'id' =>'slug'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Status*</label>
					<div class="col-lg-3 col-md-9">
						<select name="status" id="status" class='form-control form-cascade-control'>
							@if($new->status == 1)
								<option value="2">Em Aprovação</option>
								<option value="1" selected>Aprovado</option>
							@else
								<option value="2" selected>Em Aprovação</option>
								<option value="1">Aprovado</option>
							@endif
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Notícia 1</label>
					<div class="col-lg-5 col-md-9">
						<select name='noticia1' class='form-control form-cascade-control' id='noticia1'>
							<option value="0">Selecione...</option>
							@foreach($noticias as $noticia)

								@if($noticia->id == $new->idnoticia1)
								 	<option value='{{ $noticia->id }}' selected>{{ $noticia->titulo }}</option>
								@else
									<option value='{{ $noticia->id }}'>{{ $noticia->titulo }}</option>
								@endif

							@endforeach

						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Notícia 2</label>
					<div class="col-lg-5 col-md-9">
						<select name='noticia2' class='form-control form-cascade-control' id='noticia2'>
							<option value="0">Selecione...</option>
							@foreach($noticias as $noticia)

								@if($noticia->id == $new->idnoticia2)
									<option value='{{ $noticia->id }}' selected>{{ $noticia->titulo }}</option>
								@else
									<option value='{{ $noticia->id }}'>{{ $noticia->titulo }}</option>
								@endif

							@endforeach

						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Notícia 3</label>
					<div class="col-lg-5 col-md-9">
						<select name='noticia3' class='form-control form-cascade-control' id='noticia3'>
							<option value="0">Selecione...</option>
							@foreach($noticias as $noticia)

								@if($noticia->id == $new->idnoticia3)
									<option value='{{ $noticia->id }}' selected>{{ $noticia->titulo }}</option>
								@else
									<option value='{{ $noticia->id }}'>{{ $noticia->titulo }}</option>
								@endif

							@endforeach

						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Notícia 4</label>
					<div class="col-lg-5 col-md-9">
						<select name='noticia4' class='form-control form-cascade-control' id='noticia4'>
								<option value="0">Selecione...</option>
							@foreach($noticias as $noticia)

								@if($noticia->id == $new->idnoticia4)
									<option value='{{ $noticia->id }}' selected>{{ $noticia->titulo }}</option>
								@else
									<option value='{{ $noticia->id }}'>{{ $noticia->titulo }}</option>
								@endif

							@endforeach

						</select>
					</div>
				</div>

				<div class="col-md-12">
					{{ Form::submit('Editar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/adminAcaoBolhetim")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop