@section('content')

<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>		
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Editar Categoria
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::model($categoria[0], array('route' => array('category.update', $categoria[0]->id), 'method' => 'PUT','files' => true ,'class' => 'form-horizontal cascade-forms')) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('titulo',$categoria[0]->titulo,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo,'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Status*</label>
					<div class="col-lg-3 col-md-9">

						{{ Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'),$categoria[0]->status,array('class' => 'form-control form-cascade-control')) }}

					</div>
					<label class="col-lg-2 col-md-3 control-label">Seção</label>
					<div class="col-lg-3 col-md-9">
						<select class="form-control form-cascade-control" id="secao" name="secao">
								<option></option>
								<option value="{{$categoria[0]->idsecao}}" selected="true">{{$categoria[0]->secao}}</option>
							@foreach($secoes as $secao)
								<option value="{{$secao->id}}">{{$secao->titulo}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Link</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('link',$categoria[0]->link,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'link',
								'id' =>'link',
								'placeholder' => 'Digite a url...'
							))

						}}
					</div>
				</div>

				<hr />
				<h4>
					Inglês <a><i class="fa fa-plus" id="toogleEn"></i></a>
				</h4>
				@if(isset($traducao[0]->titulo))
					<div class="ingles">
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Título</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::text('tituloEn',$traducao[0]->titulo,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'tituloEn',
										'id' =>'tituloEn'
									))

								}}
							</div>
						</div>
					</div>
				@else
					<div class="ingles" style="display:none;">
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Título</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::text('tituloEn',null,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'tituloEn',
										'id' =>'tituloEn'
									))

								}}
							</div>
						</div>
					</div>
				@endif

				<div class="col-md-12">
					{{ Form::submit('Editar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/category")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop