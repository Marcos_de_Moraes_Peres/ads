@section('content')

<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Editar Notícia
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::model($noticia, array('route' => array('adminAcaoNoticia.update', $noticia->id), 'method' => 'PUT','files' => true ,'class' => 'form-horizontal cascade-forms')) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('titulo',$noticia->titulo,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Imagem*</label>
					<div class="col-lg-3 col-md-9">
						<input type="file" id="arquivo" name="arquivo" size="40">
					</div>
					<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
					<div class="col-lg-3 col-md-9">
						{{

							Form::text('slug',$noticia->slug,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'slug',
								'id' =>'slug'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Resumo</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::textarea('resumo',$noticia->resumo,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'resumo',
								'id' =>'resumo',
								'rows' => 3
							))

						}}
					</div>
				</div>
				<div class="form-group">
					{{
						Form::textarea('conteudo',$noticia->texto,array(
							'class' =>  'ckeditor',
							'id' =>'conteudo',
						))
					}}
				</div>

				<div class="col-md-12">
					{{ Form::submit('Editar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/adminAcaoNoticia")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop