@section('content')
<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Editar Cliente
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::model($cliente[0], array('route' => array('adminCliente.update', $cliente[0]->id), 'method' => 'PUT','id' => 'validateSecao','files' => true ,'class' => 'form-horizontal cascade-forms')) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('titulo',$cliente[0]->nome,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Status*</label>
					<div class="col-lg-3 col-md-9">
						{{ Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), $cliente[0]->status,array('class' => 'form-control form-cascade-control'))}}
					</div>
					<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
					<div class="col-lg-3 col-md-9">
						{{

							Form::text('slug',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'slug',
								'id' =>'slug'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Site</label>
					<div class="col-lg-3 col-md-9">
						{{

							Form::text('site',$cliente[0]->site,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'site',
								'id' =>'site'
							))

						}}
					</div>
					<label class="col-lg-2 col-md-3 control-label">Imagem</label>
					<div class="col-lg-4 col-md-9">

						<input type="file" name="arquivo" size="40">

					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-3 col-md-3 control-label">Equipe de Atendimento</label>
				</div>
				<div class="form-group">
					{{
						Form::textarea('atendimento',$cliente[0]->atendimento,array(
							'class' =>  'ckeditor',
							'id' =>'atendimento',
						))
					}}
				</div>
				<div class="form-group">
					{{
						Form::textarea('conteudo',$cliente[0]->conteudo,array(
							'class' =>  'ckeditor',
							'id' =>'conteudo',
						))
					}}
				</div>

				<hr />
				<h4>
					Inglês <a><i class="fa fa-plus" id="toogleEn"></i></a>
				</h4>
				@if(isset($clienteEn[0]->nome))
					<div class="ingles">
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Título*</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::text('tituloEn',$clienteEn[0]->nome,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'tituloEn',
										'id' =>'tituloEn'
									))

								}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
							<div class="col-lg-3 col-md-9">
								{{

									Form::text('slugEn',$clienteEn[0]->slug,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'slugEn',
										'id' =>'slugEn'
									))

								}}
							</div>
						</div>
						<div class="form-group">
							{{
								Form::textarea('conteudoEn',$clienteEn[0]->conteudo,array(
									'class' =>  'ckeditor',
									'id' =>'conteudoEn',
								))
							}}
						</div>
					</div>
				@else
					<div class="ingles" style="display:none;">
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Título*</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::text('tituloEn',null,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'tituloEn',
										'id' =>'tituloEn'
									))

								}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
							<div class="col-lg-3 col-md-9">
								{{

									Form::text('slugEn',null,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'slugEn',
										'id' =>'slugEn'
									))

								}}
							</div>
						</div>
						<div class="form-group">
							{{
								Form::textarea('conteudoEn',null,array(
									'class' =>  'ckeditor',
									'id' =>'conteudoEn',
								))
							}}
						</div>
					</div>
				@endif

				<div class="col-md-12">
					{{ Form::submit('Editar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/adminCliente")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop