@section('content')
<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Editar Galeria
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::model($galeria[0], array('route' => array('adminGallery.update', $galeria[0]->id), 'method' => 'PUT','id' => 'validateSecao','files' => true ,'class' => 'form-horizontal cascade-forms')) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('titulo',$galeria[0]->titulo,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Status*</label>
					<div class="col-lg-3 col-md-9">
						{{ Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), $galeria[0]->status,array('class' => 'form-control form-cascade-control')) }}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Paginas</label>
					<div class="col-lg-5 col-md-9">
						<select name='idcase[]' class='form-control form-cascade-control' id='pageGal' multiple="multiple">
							@foreach($selectedCases as $case)

								<option value='{{ $case->id }}' selected="true">{{ $case->titulo }}</option>

							@endforeach

							@foreach($cases as $case)

								<option value='{{ $case->id }}'>{{ $case->titulo }}</option>

							@endforeach

						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Produtos</label>
					<div class="col-lg-5 col-md-9">
						<select name='idnoticia[]' class='form-control form-cascade-control' id='productGal' multiple="multiple">
							@foreach($selectedNoticias as $noticia)

								<option value='{{ $noticia->id }}' selected="true">{{ $noticia->titulo }}</option>

							@endforeach

							@foreach($noticias as $noticia)

								<option value='{{ $noticia->id }}'>{{ $noticia->titulo }}</option>

							@endforeach

						</select>
					</div>
				</div>
				<hr />

				<div class="col-md-12">
					{{ Form::submit('Salvar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/adminGallery")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop