@section('content')
{{ HTML::script('assets/admin/js/validacao/validate.js'); }}
<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Editar Página
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::model($pagina[0], array('route' => array('pages.update', $pagina[0]->id), 'method' => 'PUT','id' => 'validateSecao','files' => true ,'class' => 'form-horizontal cascade-forms')) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-8 col-md-9">
						{{
							Form::text('titulo',$pagina[0]->titulo,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo'
							))
						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Template*</label>
					<div class="col-lg-3 col-md-9">
						<select class="form-control form-cascade-control" id="template" name="template">
								<option value="{{$pagina[0]->idtemplate}}">{{$pagina[0]->template}}</option>
							@foreach($templates as $template)
								<option value="{{$template->id}}">{{$template->titulo}}</option>
							@endforeach
						</select>
					</div>
					<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
					<div class="col-lg-3 col-md-9">
						{{
							Form::text('slug',$pagina[0]->slug,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'slug',
								'id' =>'slug'
							))
						}}
					</div>
				</div>
				<div class="panel-heading">
					<label class="col-lg-2 col-md-3 control-label"></label>
					<p>
						*Preencha a Seção ou a Categoria.
					</p>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Categoria</label>
					<div class="col-lg-3 col-md-9">
						<select class="form-control form-cascade-control" id="categoria" name="categoria">
								<option value="{{$pagina[0]->idcategoria}}">{{$pagina[0]->categoria}}</option>
								<option value="">Todos</option>
							@foreach($categorias as $categoria)
								<option value="{{$categoria->id}}">{{$categoria->titulo}}</option>
							@endforeach
						</select>
					</div>
					<label class="col-lg-2 col-md-3 control-label">Seção</label>
					<div class="col-lg-3 col-md-9">
						<select class="form-control form-cascade-control" id="secao" name="secao">
								<option value="{{$pagina[0]->idsecao}}">{{$pagina[0]->secao}}</option>
								<option value="">Todos</option>
							@foreach($secoes as $secao)
								<option value="{{$secao->id}}">{{$secao->titulo}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Imagem</label>
					<div class="col-lg-8 col-md-9">
						<input type="file" name="arquivo" size="40">
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Resumo</label>
					<div class="col-lg-8 col-md-9">
						{{
							Form::textarea('resumo',$pagina[0]->resumo,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'resumo',
								'id' =>'resumo',
								'rows' => 3
							))
						}}
					</div>
				</div>
				<div class="form-group">
					{{
						Form::textarea('conteudo',$pagina[0]->conteudo,array(
							'class' =>  'ckeditor',
							'id' =>'conteudo',
						))
					}}
				</div>
				<hr />
				<h4>
					Inglês <a><i class="fa fa-plus" id="toogleEn"></i></a>
				</h4>
				@if(isset($paginaEn[0]->titulo))
					<div class="ingles">
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Título*</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::text('tituloEn',$paginaEn[0]->titulo,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'tituloEn',
										'id' =>'tituloEn,'
									))

								}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
							<div class="col-lg-3 col-md-9">
								{{

									Form::text('slugEn',$paginaEn[0]->slug,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'slugEn',
										'id' =>'slugEn'
									))

								}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Resumo</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::textarea('resumoEn',$paginaEn[0]->resumo,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'resumoEn',
										'id' =>'resumoEn',
										'rows' => 3
									))

								}}
							</div>
						</div>
						<div class="form-group">
							{{
								Form::textarea('conteudoEn',$paginaEn[0]->conteudo,array(
									'class' =>  'ckeditor',
									'id' =>'conteudoEn',
								))
							}}
						</div>
					</div>
				@else
					<div class="ingles" style="display:none;">
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Título*</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::text('tituloEn',null,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'tituloEn',
										'id' =>'tituloEn,'
									))

								}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
							<div class="col-lg-3 col-md-9">
								{{

									Form::text('slugEn',null,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'slugEn',
										'id' =>'slugEn'
									))

								}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Resumo</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::textarea('resumoEn',null,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'resumoEn',
										'id' =>'resumoEn',
										'rows' => 3
									))

								}}
							</div>
						</div>
						<div class="form-group">
							{{
								Form::textarea('conteudoEn',null,array(
									'class' =>  'ckeditor',
									'id' =>'conteudoEn',
								))
							}}
						</div>
					</div>
				@endif

				<div class="col-md-12">
					{{ Form::submit('Editar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/pages")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop