@section('content')

<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Editar Serviço
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::model($servico[0], array('route' => array('adminServico.update', $servico[0]->id), 'method' => 'PUT','files' => true ,'class' => 'form-horizontal cascade-forms')) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('titulo',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Status*</label>
					<div class="col-lg-3 col-md-9">

						{{ Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), '1',array('class' => 'form-control form-cascade-control')) }}

					</div>
					<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
					<div class="col-lg-3 col-md-9">
						{{

							Form::text('slug',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'slug',
								'id' =>'slug'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Categoria*</label>
					<div class="col-lg-3 col-md-9">
						<select class="form-control form-cascade-control" id="categoria" name="categoria">
							<option value="{{$servico[0]->idcategoria}}">{{$servico[0]->categoria}}</option>
							@foreach($categorias as $categoria)
								<option value="{{$categoria->id}}">{{$categoria->titulo}}</option>
							@endforeach
						</select>
					</div>
					<label class="col-lg-2 col-md-3 control-label">Imagem</label>
					<div class="col-lg-3 col-md-9">

						<input type="file" name="arquivo" size="40">

					</div>
				</div>
				<div class="form-group">
					{{
						Form::textarea('conteudo',null,array(
							'class' =>  'ckeditor',
							'id' =>'conteudo',
						))
					}}
				</div>
				<hr />
				<h4>
					Inglês <a><i class="fa fa-plus" id="toogleEn"></i></a>
				</h4>
				@if(isset($servicoEn[0]->titulo))
					<div class="ingles">
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Título*</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::text('tituloEn',$servicoEn[0]->titulo,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'tituloEn',
										'id' =>'tituloEn'
									))

								}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
							<div class="col-lg-3 col-md-9">
								{{

									Form::text('slugEn',$servicoEn[0]->slug,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'slugEn',
										'id' =>'slugEn'
									))

								}}
							</div>
						</div>
						<div class="form-group">
							{{
								Form::textarea('conteudoEn',$servicoEn[0]->conteudo,array(
									'class' =>  'ckeditor',
									'id' =>'conteudoEn',
								))
							}}
						</div>
					</div>
				@else
					<div class="ingles" style="display:none;">
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Título*</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::text('tituloEn',null,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'tituloEn',
										'id' =>'tituloEn'
									))

								}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
							<div class="col-lg-3 col-md-9">
								{{

									Form::text('slugEn',null,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'slugEn',
										'id' =>'slugEn'
									))

								}}
							</div>
						</div>
						<div class="form-group">
							{{
								Form::textarea('conteudoEn',null,array(
									'class' =>  'ckeditor',
									'id' =>'conteudoEn',
								))
							}}
						</div>
					</div>
				@endif

				<div class="col-md-12">
					{{ Form::submit('Editar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/adminServico")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop