@section('content')
{{ HTML::script('assets/admin/js/validacao/validate.js'); }}
<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Incluir Depoimento
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::model($depoimento[0], array('route' => array('adminDepoimento.update', $depoimento[0]->id), 'method' => 'PUT','files' => true ,'class' => 'form-horizontal cascade-forms')) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Conteudo*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::textarea('conteudo',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'conteudo',
								'id' =>'conteudo',
								'rows' => 3
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Nome*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('nome',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'nome',
								'id' =>'nome'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Status*</label>
					<div class="col-lg-3 col-md-9">

						{{ Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), '1',array('class' => 'form-control form-cascade-control')) }}

					</div>
					<label class="col-lg-2 col-md-3 control-label">Empresa*</label>
					<div class="col-lg-3 col-md-9">
						<select class="form-control form-cascade-control" id="idcliente" name="idcliente">
							<option value="{{$depoimento[0]->idcliente}}">{{$depoimento[0]->cliente}}</option>
							@foreach($clientes as $cliente)
								<option value="{{$cliente->id}}">{{$cliente->nome}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="form-group">
						<label class="col-lg-2 col-md-3 control-label">Imagem</label>
						<div class="col-lg-8 col-md-9">

							<input type="file" name="arquivo" size="40">

						</div>
					</div>
				</div>

				<hr />
				<h4>
					Inglês <a><i class="fa fa-plus" id="toogleEn"></i></a>
				</h4>
				@if(isset($depoimentoEn[0]->conteudo))
					<div class="ingles">
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Conteudo*</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::textarea('conteudoEn',$depoimentoEn[0]->conteudo,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'conteudoEn',
										'id' =>'conteudoEn',
										'rows' => 3
									))

								}}
							</div>
						</div>
					</div>
				@else
					<div class="ingles" style="display:none;">
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Conteudo*</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::textarea('conteudoEn',null,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'conteudoEn',
										'id' =>'conteudoEn',
										'rows' => 3
									))

								}}
							</div>
						</div>
					</div>
				@endif

				<div class="col-md-12">
					{{ Form::submit('Editar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/adminDepoimento")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop