@section('content')
{{ HTML::script('assets/admin/js/validacao/validate.js'); }}
<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Editar Slide
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::model($slide[0], array('route' => array('adminSlide.update', $slide[0]->id), 'method' => 'PUT','id' => 'validateSlide','files' => true ,'class' => 'form-horizontal cascade-forms')) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('titulo',$slide[0]->titulo,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Descrição*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::textarea('descricao',$slide[0]->descricao,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'descricao',
								'id' =>'descricao',
								'rows' => 3
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">link*</label>
					<div class="col-lg-3 col-md-9">
						{{

							Form::text('link',$slide[0]->link,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'link',
								'id' =>'link'
							))

						}}
					</div>
					<label class="col-lg-2 col-md-3 control-label">Status*</label>
					<div class="col-lg-3 col-md-9">

						{{ Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'),$slide[0]->status,array('class' => 'form-control form-cascade-control')) }}

					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Capa</label>
					<div class="col-lg-3 col-md-9">

				 		<input type="file" name="arquivo" size="40">

			 		</div>
				</div>

				<hr />
				<h4>
					Inglês <a><i class="fa fa-plus" id="toogleEn"></i></a>
				</h4>
				@if(isset($slideEn[0]->titulo))
					<div class="ingles">
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Descrição</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::textarea('descricaoEn',$slideEn[0]->descricao,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'descricaoEn',
										'id' =>'descricaoEn',
										'rows' => 3
									))

								}}
							</div>
						</div>
					</div>
				@else
					<div class="ingles" style="display:none;">
						<div class="form-group">
							<label class="col-lg-2 col-md-3 control-label">Descrição</label>
							<div class="col-lg-8 col-md-9">
								{{

									Form::textarea('descricaoEn',null,array(
										'class' =>  'form-control form-cascade-control input-small',
										'name' => 'descricaoEn',
										'id' =>'descricaoEn',
										'rows' => 3
									))

								}}
							</div>
						</div>
					</div>
				@endif

				<hr />

				<div class="col-md-12">
					{{ Form::submit('Editar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/adminSlide")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop