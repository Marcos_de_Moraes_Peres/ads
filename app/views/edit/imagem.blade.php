@section('content')
<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Gerencia de Imagens - Galeria: {{$galeria[0]->titulo}}
				</h3>
			</h3>
		</div>
		<div class="panel-body"s>
			{{ Form::model($galeria[0], array('route' => array('adminImages.update', $galeria[0]->id),'id' => 'validateImagem' ,'method' => 'PUT','files' => true,'class' => 'form-horizontal cascade-forms')) }}
				<div class="form-group">
					<label class=" col-lg-2 control-label">Inserir arquivo ZIP</label>
					<div class="col-lg-4 col-md-9">
						<input type="file" name="ziparchive" size="40">
					</div>
					<label class=" col-lg-2 control-label">Inserir Imagem</label>
					<div class="col-lg-4 col-md-9">

						<input type="file" name="arquivo" size="40">

					</div>
				</div>
				<div class="col-md-7">
					{{ Form::submit('Atualizar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/adminGallery")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}		

		</div>

		@if($imagens)
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-cascade">
							<div class="panel-heading">
								<h3 class="panel-title">
									Galeria de Fotos
								</h3>
							</div>
							<div class="panel-body gallery">
								{{ Form::open(array('url' => 'adminImage/order', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateDepoimento', 'role' => 'form','enctype' => "multipart/form-data" )) }}
									<div class="controls">
										<ul class="list-inline pull-left ">
											<li class="filter" data-filter="mix">
												{{ Form::submit('Salvar Ordem', array('class' => 'btn btn-primary')) }}
											</li>
										</ul>
										<ul class="list-inline pull-right">
									<li class="sort" data-sort="data-name" data-order="asc">
										<a class="btn bg-pink text-white excluir" href='{{URL::to("adminImage/all/".$galeria[0]->id)}}'>Excluir Todos</a>
									</li>
								</ul>
							</div>
									<!-- Gallery Items -->
							<div class="row">
								<ul id="sortable">
									@foreach($imagens as $key => $imagem)
										<li class="mix dogs  mix_all" style="display: inline-block; opacity: 1;">
											<div class="panel panel-cascade panel-gallery ">
												<div class="panel-body nopadding">
													<img src='{{URL::to("assets/img/timthumb.php?src=galeria/$imagem->link&w=210&h=210")}}' alt="">
												</div>
												<div class="panel-footer">
													<h3>
														<a href='{{URL::to("adminImage/excluir/".$imagem->id)}}' class="btn btn-danger text-white excluir"><i class="fa fa-trash-o"></i></a>
														<input type="hidden" name="{{ $key }}" value="{{ $imagem->id }}" />
													</h3>
												</div>
											</div>
											</li>
										@endforeach
										</ul>
									</div>
								{{ Form::close() }}
							</div> <!-- /panel-body -->
						</div><!-- Panel -->
					</div>
				</div>
		@endif


	</div>
</div>

@stop