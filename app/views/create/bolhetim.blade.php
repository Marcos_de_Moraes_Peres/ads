@section('content')

<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Incluir Boletim
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::open(array('url' => 'adminAcaoBolhetim', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateBolhetim', 'role' => 'form','enctype' => "multipart/form-data" )) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('titulo',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Mês da Edição*</label>
					<div class="col-lg-3 col-md-9">
						<select name='mes' class='form-control form-cascade-control' id='mes'>
							<option value="Janeiro">Janeiro</option>
							<option value="Fevereiro">Fevereiro</option>
							<option value="Março">Março</option>
							<option value="Abril">Abril</option>
							<option value="Maio">Maio</option>
							<option value="Junho">Junho</option>
							<option value="Julho">Julho</option>
							<option value="Agosto">Agosto</option>
							<option value="Setembro">Setembro</option>
							<option value="Outubro">Outubro</option>
							<option value="Novembro">Novembro</option>
							<option value="Dezembro">Dezembro</option>
						</select>
					</div>
					<label class="col-lg-2 col-md-3 control-label">Ano da Edição*</label>
					<div class="col-lg-3 col-md-9">
						<select name='ano' class='form-control form-cascade-control' id='ano'>
							@while($base <= date('Y')+1)
								<option value="{{$base}}">{{$base}}</option>
								{{$base++}}
							@endwhile
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Imagem*</label>
					<div class="col-lg-3 col-md-9">
						<input type="file" id="arquivo" name="arquivo" size="40">
					</div>
					<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
					<div class="col-lg-3 col-md-9">
						{{

							Form::text('slug',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'slug',
								'id' =>'slug'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Status*</label>
					<div class="col-lg-3 col-md-9">
						<select name="status" id="status" class='form-control form-cascade-control'>
							<option value="2" selected>Em Aprovação</option>
							<option value="1">Aprovado</option>
						</select>
					</div>
				</div>			
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Notícia 1</label>
					<div class="col-lg-5 col-md-9">
						<select name='noticia1' class='form-control form-cascade-control' id='noticia1'>
							<option value="0">Selecione...</option>
							@foreach($noticias as $noticia)

								 <option value='{{ $noticia->id }}'>{{ $noticia->titulo }}</option>

							@endforeach

						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Notícia 2</label>
					<div class="col-lg-5 col-md-9">
						<select name='noticia2' class='form-control form-cascade-control' id='noticia2'>
							<option value="0">Selecione...</option>
							@foreach($noticias as $noticia)

								 <option value='{{ $noticia->id }}'>{{ $noticia->titulo }}</option>

							@endforeach

						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Notícia 3</label>
					<div class="col-lg-5 col-md-9">
						<select name='noticia3' class='form-control form-cascade-control' id='noticia3'>
							<option value="0">Selecione...</option>
							@foreach($noticias as $noticia)

								 <option value='{{ $noticia->id }}'>{{ $noticia->titulo }}</option>

							@endforeach

						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Notícia 4</label>
					<div class="col-lg-5 col-md-9">
						<select name='noticia4' class='form-control form-cascade-control' id='noticia4'>
								<option value="0">Selecione...</option>
							@foreach($noticias as $noticia)

								 <option value='{{ $noticia->id }}'>{{ $noticia->titulo }}</option>

							@endforeach

						</select>
					</div>
				</div>

				<div class="col-md-12">
					{{ Form::submit('Cadastrar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("adminAcaoBolhetim")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop