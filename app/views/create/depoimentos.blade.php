@section('content')
{{ HTML::script('assets/admin/js/validacao/validate.js'); }}
<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Incluir Depoimento
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::open(array('url' => 'adminDepoimento', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateDepoimento', 'role' => 'form','enctype' => "multipart/form-data" )) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Conteudo*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::textarea('conteudo',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'conteudo',
								'id' =>'conteudo',
								'rows' => 3
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Nome*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('nome',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'nome',
								'id' =>'nome'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Status*</label>
					<div class="col-lg-3 col-md-9">

						{{ Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), '1',array('class' => 'form-control form-cascade-control')) }}

					</div>
					<label class="col-lg-2 col-md-3 control-label">Empresa*</label>
					<div class="col-lg-3 col-md-9">
						<select class="form-control form-cascade-control" id="idcliente" name="idcliente">
							<option></option>
							@foreach($clientes as $cliente)
								<option value="{{$cliente->id}}">{{$cliente->nome}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Imagem</label>
					<div class="col-lg-8 col-md-9">

						<input type="file" name="arquivo" size="40">

					</div>
				</div>

				<hr />
				<h4>
					Inglês <a><i class="fa fa-plus" id="toogleEn"></i></a>
				</h4>
				<div class="ingles" style="display:none;">
					<div class="form-group">
						<label class="col-lg-2 col-md-3 control-label">Conteudo*</label>
						<div class="col-lg-8 col-md-9">
							{{

								Form::textarea('conteudoEn',null,array(
									'class' =>  'form-control form-cascade-control input-small',
									'name' => 'conteudoEn',
									'id' =>'conteudoEn',
									'rows' => 3
								))

							}}
						</div>
					</div>
				</div>

				<div class="col-md-12">
					{{ Form::submit('Cadastrar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/adminDepoimento")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop