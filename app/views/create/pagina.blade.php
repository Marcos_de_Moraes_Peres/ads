@section('content')
{{ HTML::script('assets/admin/js/validacao/validate.js'); }}
<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Incluir Página
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::open(array('url' => 'pages', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateSecao', 'role' => 'form','enctype' => "multipart/form-data" )) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('titulo',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Template*</label>
					<div class="col-lg-3 col-md-9">
						<select class="form-control form-cascade-control" id="template" name="template">
							@foreach($templates as $template)
								<option value="{{$template->id}}">{{$template->titulo}}</option>
							@endforeach
						</select>
					</div>
					<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
					<div class="col-lg-3 col-md-9">
						{{

							Form::text('slug',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'slug',
								'id' =>'slug'
							))

						}}
					</div>
				</div>
				<div class="panel-heading">
					<label class="col-lg-2 col-md-3 control-label"></label>
					<p>
						*Preencha a Seção ou a Categoria.
					</p>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Categoria</label>
					<div class="col-lg-3 col-md-9">
						<select class="form-control form-cascade-control" id="categoria" name="categoria">
							<option></option>
							@foreach($categorias as $categoria)
								<option value="{{$categoria->id}}">{{$categoria->titulo}}</option>
							@endforeach
						</select>
					</div>
					<label class="col-lg-2 col-md-3 control-label">Seção</label>
					<div class="col-lg-3 col-md-9">
						<select class="form-control form-cascade-control" id="secao" name="secao">
							<option></option>
							@foreach($secoes as $secao)
								<option value="{{$secao->id}}">{{$secao->titulo}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Imagem</label>
					<div class="col-lg-8 col-md-9">

						<input type="file" name="arquivo" size="40">

					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Resumo</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::textarea('resumo',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'resumo',
								'id' =>'resumo',
								'rows' => 3
							))

						}}
					</div>
				</div>
				<div class="form-group">
					{{
						Form::textarea('conteudo',null,array(
							'class' =>  'ckeditor',
							'id' =>'conteudo',
						))
					}}
				</div>

				<hr />
				<h4>
					Inglês <a><i class="fa fa-plus" id="toogleEn"></i></a>
				</h4>
				<div class="ingles" style="display:none;">
					<div class="form-group">
						<label class="col-lg-2 col-md-3 control-label">Título*</label>
						<div class="col-lg-8 col-md-9">
							{{

								Form::text('tituloEn',null,array(
									'class' =>  'form-control form-cascade-control input-small',
									'name' => 'tituloEn',
									'id' =>'tituloEn'
								))

							}}
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-md-3 control-label">Nome URL*</label>
						<div class="col-lg-3 col-md-9">
							{{

								Form::text('slugEn',null,array(
									'class' =>  'form-control form-cascade-control input-small',
									'name' => 'slugEn',
									'id' =>'slugEn'
								))

							}}
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-md-3 control-label">Resumo</label>
						<div class="col-lg-8 col-md-9">
							{{

								Form::textarea('resumoEn',null,array(
									'class' =>  'form-control form-cascade-control input-small',
									'name' => 'resumoEn',
									'id' =>'resumoEn',
									'rows' => 3
								))

							}}
						</div>
					</div>
					<div class="form-group">
						{{
							Form::textarea('conteudoEn',null,array(
								'class' =>  'ckeditor',
								'id' =>'conteudoEn',
							))
						}}
					</div>
				</div>

				<div class="col-md-12">
					{{ Form::submit('Cadastrar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/pages")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop