@section('content')
<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Incluir Galeria
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::open(array('url' => 'adminGallery', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateSecao', 'role' => 'form','enctype' => "multipart/form-data" )) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text('titulo',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Status*</label>
					<div class="col-lg-3 col-md-9">
						{{ Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), '1',array('class' => 'form-control form-cascade-control')) }}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Cases</label>
					<div class="col-lg-5 col-md-9">
						<select name='idcase[]' class='form-control form-cascade-control' id='pageGal' multiple="multiple">

							@foreach($cases as $case)

								 <option value='{{ $case->id }}'>{{ $case->titulo }}</option>

							@endforeach

						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Notícias</label>
					<div class="col-lg-5 col-md-9">
						<select name='idnoticia[]' class='form-control form-cascade-control' id='productGal' multiple="multiple">

							@foreach($noticias as $noticia)

								 <option value='{{ $noticia->id }}'>{{ $noticia->titulo }}</option>

							@endforeach

						</select>
					</div>
				</div>
				<hr />

				<div class="col-md-12">
					{{ Form::submit('Cadastrar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/adminGallery")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop