@section('content')
<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Criar Destaques
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::open(array('url' => 'adminSpotlight','files'=>true, 'class' => 'form-horizontal cascade-forms', 'id' => 'validateDestaque', 'role' => 'form','enctype' => "multipart/form-data" )) }}

				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-3 col-md-9">
						{{

							Form::text("titulo",null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => "titulo",
								'id' =>"titulo"
							))

						}}
					</div>
					<label class="col-lg-2 col-md-3 control-label">Capa Destaque</label>
					<div class="col-lg-3 col-md-9">

						<input type="file" name="arquivo" size="40">

					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Descrição*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::textarea("descricao",null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => "descricao",
								'id' =>"descricao",
								'rows' => 3
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Status*</label>
					<div class="col-lg-3 col-md-9">
						{{ Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), '1',array('class' => 'form-control form-cascade-control'))}}
					</div>
					<label class="col-lg-2 col-md-3 control-label">Tipo*</label>
					<div class="col-lg-3 col-md-9">
						{{ Form::select('tipo', array('1' => 'Aleatório', '2' => 'Fixo'), '1',array('class' => 'form-control form-cascade-control'))}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Link*</label>
					<div class="col-lg-8 col-md-9">
						{{

							Form::text("link",null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => "link",
								'id' => "link"
							))

						}}
					</div>
				</div>
				<hr />
				<h4>
					Inglês <a><i class="fa fa-plus" id="toogleEn"></i></a>
				</h4>
				<div class="ingles" style="display:none;">
					<div class="form-group">
						<label class="col-lg-2 col-md-3 control-label">Título*</label>
						<div class="col-lg-8 col-md-9">
							{{

								Form::text('tituloEn',null,array(
									'class' =>  'form-control form-cascade-control input-small',
									'name' => 'tituloEn',
									'id' =>'tituloEn'
								))

							}}
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 col-md-3 control-label">Descrição*</label>
						<div class="col-lg-8 col-md-9">
							{{

								Form::textarea("descricaoEn",null,array(
									'class' =>  'form-control form-cascade-control input-small',
									'name' => "descricaoEn",
									'id' =>"descricaoEn",
									'rows' => 3
								))

							}}
						</div>
					</div>
				</div>

				<div class="col-md-12">
					{{ Form::submit('Cadastrar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/adminSpotlight")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop