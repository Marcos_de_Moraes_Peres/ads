@section('content')

<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Incluir Categoria
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::open(array('url' => 'category', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateSecao', 'role' => 'form','enctype' => "multipart/form-data"))}}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Título*</label>
					<div class="col-lg-8 col-md-9">
						{{
							Form::text('titulo',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo'
							))
						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Link</label>
					<div class="col-lg-8 col-md-9">
						{{
							Form::text('link',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'link',
								'id' =>'link',
								'placeholder' => 'Digite a url...'
							))
						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Status*</label>
					<div class="col-lg-3 col-md-9">
						{{ Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), '1',array('class' => 'form-control form-cascade-control'))}}
					</div>
					<label class="col-lg-2 col-md-3 control-label">Seção</label>
					<div class="col-lg-3 col-md-9">
						<select class="form-control form-cascade-control" id="secao" name="secao">
								<option></option>
							@foreach($secoes as $secao)
								<option value="{{$secao->id}}">{{$secao->titulo}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<hr />
				<h4>
					Inglês <a><i class="fa fa-plus" id="toogleEn"></i></a>
				</h4>
				<div class="ingles" style="display:none;">
					<div class="form-group">
						<label class="col-lg-2 col-md-3 control-label">Título</label>
						<div class="col-lg-8 col-md-9">
							{{
								Form::text('tituloEn',null,array(
									'class' =>  'form-control form-cascade-control input-small',
									'name' => 'tituloEn',
									'id' =>'tituloEn'
								))
							}}
						</div>
					</div>
				</div>

				<div class="col-md-12">
					{{ Form::submit('Cadastrar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/category")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>
			{{ Form::close() }}
		</div>
	</div>
</div>

@stop