@section('content')

<div class="col-md-12">
	<div class="panel panel-cascade">
		<div class="panel-heading">
			<h3 class="panel-title">
				<span class="pull-right">
					<a href="#" class="panel-minimize"><i class="fa fa-chevron-up"></i></a>
				</span>
				<h3 class="panel-title"><i class="fa fa-list-alt"></i>
					Incluir Usuário
				</h3>
			</h3>
		</div>
		<div class="panel-body">
			{{ Form::open(array('url' => 'adminUser', 'class' => 'form-horizontal cascade-forms', 'id' => 'validateUsuario', 'role' => 'form','enctype' => "multipart/form-data" )) }}
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Nome*</label>
					<div class="col-lg-6 col-md-9">
						{{

							Form::text('titulo',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'titulo',
								'id' =>'titulo,'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Login*</label>
					<div class="col-lg-6 col-md-9">
						{{

							Form::text('login',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'login',
								'id' =>'login,'
							))

						}}
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2 col-md-3 control-label">Senha*</label>
					<div class="col-lg-6 col-md-9">
						{{

							Form::text('password',null,array(
								'class' =>  'form-control form-cascade-control input-small',
								'name' => 'password',
								'id' =>'password,'
							))

						}}
					</div>
				</div>
				
				<div class="col-md-12">
					{{ Form::submit('Cadastrar', array('class' => 'btn btn-primary btn-animate-demo pull-right')) }}
					<a href='{{URL::to("/adminUser")}}' class="btn btn-danger btn-animate-demo pull-right">Cancelar</a>
				</div>

			{{ Form::close() }}

		</div>
	</div>
</div>

@stop