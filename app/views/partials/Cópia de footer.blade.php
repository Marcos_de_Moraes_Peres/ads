<div class="servicos">
    <div class="container">
        <div class="row">
            <div class="col-md-8 boxmenu">
                @foreach($menuObject as $key => $secao)
                    @if($key == 0)
                        <ul style="margin-left:0;">
                    @elseif($key < 4)
                        <ul>
                    @endif
                    @if(isset($secao->linkInt))
                        <li><a href="{{URL::to($secao->linkInt)}}" class="secao">{{$secao->titulo}}</a></li>
                    @elseif($secao->link)
                        <li><a href="{{$secao->link}}" target="_blank" class="secao">{{$secao->titulo}}</a></li>
                    @else
                        <li class="secao">{{$secao->titulo}}</li>
                    @endif
                    
                    @foreach($secao->filhos as $categoria)
                        @if($categoria->link)
                            <li><a href="{{$categoria->link}}" target="_blank">{{$categoria->titulo}}</a></li>
                        @elseif(isset($categoria->linkInt))
                            <li><a href="{{URL::to($categoria->linkInt)}}">{{$categoria->titulo}}</a></li>
                        @endif
                    @endforeach
                    

                    @if($key < 3)
                        </ul>
                    @endif

                @endforeach

                </ul>
            </div>
            @if(Session::get('idioma') == 1)
                <div class="col-md-4 boxcontato">
                	<h2>Entre em contato:</h2>
                    <p>Rua Michigan, 69<br>
                    Brooklin - 04566-903 - São Paulo - SP</p>
                    <p>T: 11 5090 3007<br>
                    F: 11 5090 3010<br>
                    E: <a href="mailto:contato@adsbrasil.com.br">contato@adsbrasil.com.br</a></p>
                    <p>Se preferir, preencha o formulário <a href="http://www.adsbrasil.com.br/contato/contato">clicando aqui</a>.</p>
                </div>
            @else
                <div class="col-md-4 boxcontato">
                    <h2>Contact:</h2>
                    <p>Rua Michigan, 69<br>
                    Brooklin - 04566-903 - São Paulo - SP</p>
                    <p>T: +55 11 5090 3007<br>
                    F: +55 11 5090 3010<br>
                    E: <a href="mailto:contato@adsbrasil.com.br">contato@adsbrasil.com.br</a></p>
                    <p>If you prefer, fill out the form <a href="#">clicking here</a>.</p>
                </div>
            @endif

        </div><!--fecha row-->
    </div><!--fecha container-->
</div><!--fecha servicos-->

<div class="navfooter">
	<div class="container">
        <div class="row">
            <div class="col-lg-3 nomerodape">
               <h2>ADS Comunicação Corporativa</h2>
            </div>
            @if(Session::get('idioma') == 1)
                <div class="col-lg-9 pull-right logosrodape">
                	<p>Parceira internacional</p>
                    <img src="{{URL::to('assets/img/logo_ecco2.png')}}" style="margin-right:30px;">
                    <p>Associada à</p>
                    <img src="{{URL::to('assets/img/logo_associada2.png')}}">
                </div>
            @else
                <div class="col-lg-8 pull-right logosrodape">
                    <p>Iternational partnership</p>
                    <img src="{{URL::to('assets/img/logo_ecco2.png')}}">
                    <p>Associated with</p>
                    <img src="{{URL::to('assets/img/logo_associada2.png')}}">
                </div>
            @endif
        </div><!--fecha row-->
    </div>
</div>