<div class="container">
  <div class="navbar">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{URL::to('/')}}"><span>ADS</span></a>
      <div class="col-xs-7 col-sm-4 col-md-4 hidden-xs pull-right">
        <div class="formbusca">
        {{ Form::open(array('url' => 'search','files'=>true, 'class' => 'form-horizontal cascade-forms', 'id' => 'validateSearch', 'role' => 'form','enctype' => "multipart/form-data" )) }}
          <div class="input-group">
            @if(Session::get('idioma') == 1)
              <input type="text" name="busca" class="form-control input-sm" placeholder="Buscar no site...">
            @else
              <input type="text" name="busca" class="form-control input-sm" placeholder="Search...">
            @endif
            <span class="input-group-btn btn-group-sm">
              <button class="btn btn-default azul" type="submit"><span class="glyphicon glyphicon-search"></span></button>
            </span>
          </div><!-- /input-group -->
        {{ Form::close() }}
        </div><!--fecha formbusca--->
            <ul class="btsocial pull-right visible-sm">
                <li><a href="https://twitter.com/ADSBrasil" class="social tw" data-toggle="tooltip" data-placement="bottom" title="Twitter"><span>Twitter</span></a></li>
                <li><a href="https://www.facebook.com/adscomunicacao?fref=ts" class="social fb" data-toggle="tooltip" data-placement="bottom" title="Facebook"><span>Facebook</span></a></li>
                <!---
                <li><a href="gm" class="social gm" data-toggle="tooltip" data-placement="bottom" title="G+"><span>G+</span></a></li>
                -->
                @if(Session::get('idioma') == 1)
                  <li><a href='{{URL::to("/translate/2")}}' class="social en" data-toggle="tooltip" data-placement="bottom" title="English"><span>English</span></a></li>
                @else
                  <li><a href='{{URL::to("/translate/1")}}' class="social br" data-toggle="tooltip" data-placement="bottom" title="Português"><span>Português</span></a></li>
                @endif 
          </ul>
      </div><!-- /.col-md-4 -->
    </div><!-- fecha navbar-header -->
    <div class="navbar-collapse collapse menuitens">
      <ul class="nav navbar-nav">
      		<li class="visible-xs">
				<div class="formbusca">
                {{ Form::open(array('url' => 'search','files'=>true, 'class' => 'form-horizontal cascade-forms', 'id' => 'validateSearch', 'role' => 'form','enctype' => "multipart/form-data" )) }}
                  <div class="input-group">
                    @if(Session::get('idioma') == 1)
                      <input type="text" name="busca" class="form-control input-sm" placeholder="Buscar no site...">
                    @else
                      <input type="text" name="busca" class="form-control input-sm" placeholder="Search...">
                    @endif
                    <span class="input-group-btn btn-group-sm">
                      <button class="btn btn-default azul" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                    </span>
                  </div><!-- /input-group -->
                {{ Form::close() }}
                </div><!--fecha formbusca--->
            </li>
        @foreach($menuObject as $secao)
          @if(isset($secao->linkInt) && !isset($secao->filhos[1]))
            <li><a href="{{URL::to($secao->linkInt)}}">{{$secao->titulo}}</a></li>
          @elseif($secao->link)
            <li><a href="{{$secao->link}}" target="_blank">{{$secao->titulo}}</a></li>
          @else
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown">{{$secao->titulo}}<b class="caret"></b></a>
              <ul class="dropdown-menu" role="menu">
              @foreach($secao->filhos as $categoria)
                @if($categoria->link)
                  <li><a href="{{$categoria->link}}" target="_blank">{{$categoria->titulo}}</a></li>
                @elseif(isset($categoria->linkInt))
                  <li><a href="{{URL::to($categoria->linkInt)}}">{{$categoria->titulo}}</a></li>
                @endif
              @endforeach
              </ul>
            </li>
          @endif
        @endforeach
      </ul>
      <ul class="btsocial pull-right hidden-xs hidden-sm">
        <li><a href="https://twitter.com/ADSBrasil" class="social tw" data-toggle="tooltip" data-placement="bottom" title="Twitter"><span>Twitter</span></a></li>
        <li><a href="https://www.facebook.com/adscomunicacao?fref=ts" class="social fb" data-toggle="tooltip" data-placement="bottom" title="Facebook"><span>Facebook</span></a></li>
        <!---
        <li><a href="gm" class="social gm" data-toggle="tooltip" data-placement="bottom" title="G+"><span>G+</span></a></li>
        -->
        @if(Session::get('idioma') == 1)
          <li><a href='{{URL::to("/translate/2")}}' class="social en" data-toggle="tooltip" data-placement="bottom" title="English"><span>English</span></a></li>
        @else
          <li><a href='{{URL::to("/translate/1")}}' class="social br" data-toggle="tooltip" data-placement="bottom" title="Português"><span>Português</span></a></li>
        @endif 
      </ul>
    </div><!--/.navbar-collapse -->
  </div><!-- fecha navbar -->
</div><!-- fecha container -->