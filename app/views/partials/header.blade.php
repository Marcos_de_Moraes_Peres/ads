<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>ADS - {{Session::get('title')}}</title>
    <meta name="description" content="">
    <meta name="author" content="Marcos Peres - Neopix Design">
    <meta name="viewport" content="width=device-width">

    {{ HTML::style('//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic'); }}
    {{ HTML::style('//fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic'); }} 
    {{ HTML::style('assets/css/bootstrap.css'); }}
    {{ HTML::style('assets/css/bootstrap-theme.min.css'); }}
    {{ HTML::style('assets/css/style.css'); }}
    {{ HTML::style('assets/css/settings.css'); }}
    {{ HTML::style('assets/css/main.css'); }}
    {{ HTML::style('assets/js/prettyPhoto/css/prettyPhoto.css'); }}
    {{ HTML::style('assets/js/jPages/css/jPages.css'); }}

    {{ HTML::script('assets/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js'); }}
    {{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js'); }}
    {{ HTML::script('assets/rs-plugin/js/jquery.themepunch.plugins.min.js'); }}
    {{ HTML::script('assets/rs-plugin/js/jquery.themepunch.revolution.min.js'); }}
    {{ HTML::script('assets/js/prettyPhoto/js/jquery.prettyPhoto.js'); }}
    {{ HTML::script('assets/js/jPages/js/jPages.js'); }}
</head>
<body>
<!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
