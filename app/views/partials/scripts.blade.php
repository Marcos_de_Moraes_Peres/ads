{{ HTML::script('assets/js/vendor/bootstrap.min.js')}}
{{ HTML::script('assets/js/plugins.js')}}
{{ HTML::script('assets/js/main.js')}}
{{ HTML::script('assets/js/jqueryvalidate.js')}}
{{ HTML::script('assets/js/pluginMask.js')}}
{{ HTML::script('assets/js/additional-methods.js')}}

        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
		<script type="text/javascript">

            var tpj=jQuery;
            tpj.noConflict();

			tpj('a[data-toggle="tooltip"]').tooltip();

            tpj(document).ready(function() {

            if (tpj.fn.cssOriginal!=undefined)
                tpj.fn.css = tpj.fn.cssOriginal;

                tpj('.banner').revolution(
                    {
                        /*
                        delay:9000,
                        startwidth:1170,
                        startheight:500,
                        hideThumbs:10,
                        fullWidth:"on",
                        forceFullWidth:"on"

                        */

                        delay:9000,
                        startheight:450,
                        startwidth:890,

                        hideThumbs:10,                     

                        navigationType:"both",					//bullet, thumb, none, both		(No Thumbs In FullWidth Version !)
                        navigationArrows:"verticalcentered",		//nexttobullets, verticalcentered, none
                        navigationStyle:"round",				//round,square,navbar

                        touchenabled:"on",						// Enable Swipe Function : on/off
                        onHoverStop:"on",						// Stop Banner Timet at Hover on Slide on/off

                        navOffsetHorizontal:0,
                        navOffsetVertical:10,
                        
                        shadow:0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
                        fullWidth:"on",						// Turns On or Off the Fullwidth Image Centering in FullWidth Modus            
                    });

///////////////////////////////////// PrettyPhoto

                tpj(".galeria a[rel^='prettyPhoto']").prettyPhoto();

                tpj(".holder").jPages({
                    containerID: "galeria",
                    perPage: 8
                });

///////////////////////////////////// Máscaras

                tpj('#tel').focusout(function(){
                    var phone, element;
                    element = tpj(this);
                    element.unmask();
                    phone = element.val().replace(/\D/g, '');
                    if(phone.length > 10) {
                        element.mask("(99) 99999-999?9");
                    } else {
                        element.mask("(99) 9999-9999?9");
                    }
                }).trigger('focusout');                

///////////////////////////////////// Validate

                tpj('#validateForm').validate({
                    errorClass: 'error',
                    successClass: 'success',
                    rules:{
                        nome:{
                            required: true,
                        },
                        email:{
                            required: true,
                            email: true,
                        },
                        tel:{
                            required:true,
                        },
                        mensagem:{
                            required:true,
                        },
                        assunto:{
                            required:true,
                        },
                        soma:{
                            required:true,
                        }
                    },
                    messages:{
                        nome:{
                            required: "Campo Nome é obrigatorio.",
                        },
                        email:{
                            required: "O campo Email é obrigatório.",
                            email: "Digite um e-mail válido.",
                        },
                        tel:{
                            required: "O campo Telefone é obrigatório.",
                        },
                        mensagem:{
                            required: "O campo Mensagem é obrigatório"
                        },
                        assunto:{
                            required: "O campo Assunto é obrigatório"
                        },
                        soma:{
                            required: "O campo Soma é obrigatório"
                        }
                    }
                });

                tpj('#validateCurriculo').validate({
                    errorClass: 'error',
                    successClass: 'success',
                    rules:{
                        nome:{
                            required: true,
                        },
                        email:{
                            required: true,
                            email: true,
                        },
                        tel:{
                            required:true,
                        },
                        mensagem:{
                            required:true,
                        },
                        soma:{
                            required:true,
                        },
                        arquivo:{
                            required:true,
                            extension:'doc|docx|pdf'
                        }
                    },
                    messages:{
                        nome:{
                            required: "Campo Nome é obrigatorio.",
                        },
                        email:{
                            required: "O campo Email é obrigatório.",
                            email: "Digite um e-mail válido.",
                        },
                        tel:{
                            required: "O campo Telefone é obrigatório.",
                        },
                        mensagem:{
                            required: "O campo Mensagem é obrigatório"
                        },
                        soma:{
                            required: "O campo Soma é obrigatório"
                        },
                        arquivo:{
                            required: "O campo Arquivo é obrigatório",
                            extension: "Insira apenas arquivo Word ou PDF"
                        }
                    }
                });
                
                tpj('#validateSearch').validate({
                    errorClass: 'error',
                    successClass: 'success',
                    rules:{
                        busca:{
                            required: true,
                        }
                    }
                });

//////////////////// Fechar Alert

                tpj('.alert').fadeOut(5000);
            });

        </script>
    </body>
</html>