<nav class="navbar" role="navigation">

  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header"> 
    <a class="navbar-brand" href="{{URL::to('admin')}}">
      <span>ADS</span></a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse">
    <ul class="nav navbar-nav user-menu navbar-right ">
      <li>
        <a class="user dropdown-toggle" data-toggle="dropdown">
          <span class="username" style="padding-left:10px;"><i class="fa fa-user"></i>  {{Auth::user()->nome}}</span>
        </a>
        <ul class="dropdown-menu">
          <li><a href="{{URL::to('logout')}}" class="text-danger"><i class="fa fa-lock"></i> Logout</a></li>
        </ul>
      </li>
    </ul>
  </div><!-- /.navbar-collapse -->
</nav>