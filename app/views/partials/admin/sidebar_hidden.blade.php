<div class="right-sidebar right-sidebar-hidden">
  						<div class="right-sidebar-holder" style="overflow: hidden; outline: none;" tabindex="5000">

  							<!-- @Demo part only The part from here can be removed till end of the @demo  -->
  							<a href="screens.html" class="btn btn-danger btn-block">Logout </a>


  							<h4 class="page-header text-primary text-center">
  								Theme Options
  								<a href="#" class="theme-panel-close text-primary pull-right"><strong><i class="fa fa-times"></i></strong></a>
  							</h4>

  							<ul class="list-group theme-options">
  								<li class="list-group-item" href="#">	
  									<div class="checkbox">
  										<label>
  											<input type="checkbox" id="fixedNavbar" value=""> Fixed Top Navbar
  										</label>
  									</div>
  									<div class="checkbox">
  										<label>
  											<input type="checkbox" id="fixedNavbarBottom" value=""> Fixed Bottom Navbar
  										</label>
  									</div>
  									<div class="checkbox">
  										<label>
  											<input type="checkbox" id="boxed" value=""> Boxed Version
  										</label>
  									</div>

  									<div class="form-group backgroundImage hidden">
  										<label class="control-label">Paste Image Url and then hit enter</label>
  										<input type="text" class="form-control" id="backgroundImageUrl">
  									</div>
				<!-- 
				<div class="checkbox">
					<label>
						<input type="checkbox" id="responsive" value=""> Disable Responsiveness
					</label>
				</div> 
			-->			
		</li>
		<li class="list-group-item" href="#">Predefined Themes
			<ul class="list-inline predefined-themes"> 
				<li><a class="badge" style="background-color:#54b5df" data-color-primary="#54b5df" data-color-secondary="#2f4051" data-color-link="#FFFFFF"> &nbsp; </a></li>
				<li><a class="badge" style="background-color:#d85f5c" data-color-primary="#d85f5c" data-color-secondary="#f0f0f0" data-color-link="#474747"> &nbsp; </a></li>
				<li><a class="badge" style="background-color:#3d4a5d" data-color-primary="#3d4a5d" data-color-secondary="#edf0f1" data-color-link="#777e88"> &nbsp; </a></li>
				<li><a class="badge" style="background-color:#A0B448" data-color-primary="#A0B448" data-color-secondary="#485257" data-color-link="#AFB5AA"> &nbsp; </a></li>
				<li><a class="badge" style="background-color:#2FA2D1" data-color-primary="#2FA2D1" data-color-secondary="#484D51" data-color-link="#A5B1B7"> &nbsp; </a></li>
				<li><a class="badge" style="background-color:#2f343b" data-color-primary="#2f343b" data-color-secondary="#525a65" data-color-link="#FFFFFF"> &nbsp; </a></li>
			</ul>
		</li>
		<li class="list-group-item" href="#">Change Primary Color
			<div class="input-group colorpicker-component bscp" data-color="#54728c" data-color-format="hex" id="colr" data-colorpicker-guid="1">
				<span class="input-group-addon"><i style="background-color: rgb(84, 114, 140);"></i></span>
				<input type="text" value="#54728c" id="primaryColor" readonly="" class="form-control">

			</div>
		</li>
		<li class="list-group-item" href="#">Change LeftSidebar Background
			<div class="input-group colorpicker-component bscp" data-color="#f9f9f9" data-color-format="hex" id="Scolr" data-colorpicker-guid="2">
				<span class="input-group-addon"><i style="background-color: rgb(249, 249, 249);"></i></span>
				<input type="text" value="#f9f9f9" id="secondaryColor" readonly="" class="form-control">

			</div>
		</li>
		<li class="list-group-item" href="#">Change Leftsidebar Link Color
			<div class="input-group colorpicker-component bscp" data-color="#54728c" data-color-format="hex" id="Lcolr" data-colorpicker-guid="4">
				<span class="input-group-addon"><i style="background-color: rgb(84, 114, 140);"></i></span>
				<input type="text" value="#54728c" id="linkColor" readonly="" class="form-control">

			</div>
		</li>
		<li class="list-group-item" href="#">Change RightSidebar Background
			<div class="input-group colorpicker-component bscp" data-color="#f9f9f9" data-color-format="hex" id="Rcolr" data-colorpicker-guid="3">
				<span class="input-group-addon"><i style="background-color: rgb(249, 249, 249);"></i></span>
				<input type="text" value="#f9f9f9" id="rightsidebarColor" readonly="" class="form-control">

			</div>
		</li>
	
</ul>
<!-- /.@Demo part only The part to here can be removed   -->
<div id="bic_calendar_right" class="bg-white"><div class="bic_calendar" id="bic_cal_mho"><table class="table header"><tbody><tr></tr></tbody><td><a href="#" class="botonmesanterior"><i class="fa fa-arrow-left"></i></a></td><td colspan="5" class="mesyano span6"><div class="visualmesano">January 2014</div></td><td><a href="#" class="botonmessiguiente"><i class="fa fa-arrow-right"></i></a></td></table><table class="diasmes table table"><tbody><tr class="dias_semana"><td class="primero">M</td><td>T</td><td>W</td><td>Th</td><td>F</td><td>S</td><td class="domingo ultimo">Su</td></tr><tr><td class="diainvalido primero"></td><td class="diainvalido"></td><td id="bic_cal_mho_1_1_2014"><a>1</a></td><td id="bic_cal_mho_2_1_2014"><a>2</a></td><td id="bic_cal_mho_3_1_2014"><a>3</a></td><td id="bic_cal_mho_4_1_2014"><a>4</a></td><td id="bic_cal_mho_5_1_2014" class="ultimo domingo"><a>5</a></td></tr><tr><td id="bic_cal_mho_6_1_2014" class="primero"><a>6</a></td><td id="bic_cal_mho_7_1_2014"><a>7</a></td><td id="bic_cal_mho_8_1_2014"><a>8</a></td><td id="bic_cal_mho_9_1_2014"><a>9</a></td><td id="bic_cal_mho_10_1_2014"><a>10</a></td><td id="bic_cal_mho_11_1_2014"><a>11</a></td><td id="bic_cal_mho_12_1_2014" class="domingo ultimo"><a>12</a></td></tr><tr><td id="bic_cal_mho_13_1_2014" class="primero"><a>13</a></td><td id="bic_cal_mho_14_1_2014"><a>14</a></td><td id="bic_cal_mho_15_1_2014"><a>15</a></td><td id="bic_cal_mho_16_1_2014"><a>16</a></td><td id="bic_cal_mho_17_1_2014"><a>17</a></td><td id="bic_cal_mho_18_1_2014"><a>18</a></td><td id="bic_cal_mho_19_1_2014" class="domingo ultimo"><a>19</a></td></tr><tr><td id="bic_cal_mho_20_1_2014" class="primero"><a>20</a></td><td id="bic_cal_mho_21_1_2014"><a>21</a></td><td id="bic_cal_mho_22_1_2014"><a>22</a></td><td id="bic_cal_mho_23_1_2014"><a>23</a></td><td id="bic_cal_mho_24_1_2014"><a>24</a></td><td id="bic_cal_mho_25_1_2014"><a>25</a></td><td id="bic_cal_mho_26_1_2014" class="domingo ultimo"><a>26</a></td></tr><tr><td id="bic_cal_mho_27_1_2014" class="primero"><a>27</a></td><td id="bic_cal_mho_28_1_2014"><a>28</a></td><td id="bic_cal_mho_29_1_2014"><a>29</a></td><td id="bic_cal_mho_30_1_2014"><a>30</a></td><td id="bic_cal_mho_31_1_2014"><a>31</a></td><td class="diainvalido"></td><td class="diainvalido ultimo"></td></tr></tbody></table></div></div>

<h4 class="page-header text-primary">Current Projects </h4>

<div class="list-group projects">
	<a class="list-group-item" href="#">	
		<p> Archon <span class="pull-right label bg-success">90%</span></p>
		<div class="progress progress-mini">
			<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
				<span class="sr-only">90% Complete</span>
			</div>
		</div>

	</a>
	<a class="list-group-item" href="#">	
		<p> Banshee <span class="pull-right label bg-warning">40%</span></p>
		<div class="progress progress-mini">
			<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
				<span class="sr-only">40% Complete</span>
			</div>
		</div>

	</a>
	<a class="list-group-item" href="#">	
		<p> Cascade <span class="pull-right label bg-primary">40%</span></p>
		<div class="progress progress-mini">
			<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
				<span class="sr-only">75% Complete</span>
			</div>
		</div>
	</a>
</div>
</div>


</div>