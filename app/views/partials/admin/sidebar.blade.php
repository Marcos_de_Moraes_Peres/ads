<div class="left-sidebar">
  <div class="sidebar-holder" data-original-title="" title="">
    <ul class="nav  nav-list">

      <!-- sidebar to mini Sidebar toggle -->

      <li class="nav-toggle">
        <button class="btn  btn-nav-toggle text-primary"><i class="fa toggle-left fa-angle-double-left"></i> </button>
      </li>

      <li class="submenu">
        <a class="dropdown" href="#" data-original-title="Site"><i class="fa fa-sitemap"></i><span class="hidden-minibar">Conteúdo</span></a>
        <ul style="display: none;" data-original-title="" title="">
          <li class="submenu subMenuCustom">
            
            <a class="dropdown" href="#" data-original-title="Páginas"><i class="fa fa-clipboard"></i><span class="hidden-minibar">Páginas</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/pages/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Páginas</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/pages/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Nova Página</span></a>
                </li>
              </ul>
          </li> 
           <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Clientes"><i class="fa fa-group"></i><span class="hidden-minibar">Clientes</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/adminCliente/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Clientes</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminCliente/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Novo Cliente</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminClientes/order')}}" data-original-title="Ordenar"><i class="fa fa-indent"></i><span class="hidden-minibar"> Ordenar Clientes</span></a>
                </li>
              </ul>
          </li>
          <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Depoimentos"><i class="fa fa-quote-left"></i><span class="hidden-minibar">Depoimentos</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/adminDepoimento/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Depoimentos</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminDepoimento/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Novo Depoimento</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminDepoimentos/order')}}" data-original-title="Ordenar"><i class="fa fa-indent"></i><span class="hidden-minibar"> Ordenar Depoimentos</span></a>
                </li>
              </ul>
          </li>
          <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Notícias"><i class="fa fa-list-alt"></i><span class="hidden-minibar">Notícias</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/adminNoticia/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Notícias</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminNoticia/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Nova Notícia</span></a>
                </li>
              </ul>
          </li>
          <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Serviços"><i class="fa fa-truck"></i><span class="hidden-minibar">Serviços</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/adminServico/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Serviços</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminServico/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Novo Serviço</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminServicos/order')}}" data-original-title="Ordenar"><i class="fa fa-indent"></i><span class="hidden-minibar"> Ordenar Servico</span></a>
                </li>
              </ul>
          </li>
          <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Cases"><i class="fa fa-pencil"></i><span class="hidden-minibar">Cases</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/adminCase/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Cases</span></a>
                </li>
               
                <li>
                  <a href="{{URL::to('/adminCase/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Novo Case</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminCases/order')}}" data-original-title="Criar"><i class="fa fa-indent"></i><span class="hidden-minibar"> Ordenar Cases</span></a>
                </li>
              </ul>
          </li> 
          <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Slides"><i class="fa  fa-share-square-o"></i><span class="hidden-minibar">Slides</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/adminSlide/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Slides</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminSlide/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Novo Slide</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminSlides/order')}}" data-original-title="Ordenar"><i class="fa fa-indent"></i><span class="hidden-minibar"> Ordenar Slides</span></a>
                </li>
              </ul>
          </li> 
          <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Destaques"><i class="fa fa-video-camera"></i><span class="hidden-minibar">Destaques</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/adminSpotlight/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Destaques</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminSpotlight/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Novo Destaques</span></a>
                </li>
              </ul>
          </li>  
          <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Cases"><i class="fa fa-picture-o"></i><span class="hidden-minibar">Galerias</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/adminGallery/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Galerias</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminGallery/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Nova Galeria</span></a>
                </li>
              </ul>
          </li> 
        </ul>
      </li>
		       
      <li class="submenu">
        <a class="dropdown" href="#" data-original-title="Relatórios"><i class="fa fa-list"></i><span class="hidden-minibar">Seções / Categorias</span></a>
        <ul style="display: none;" data-original-title="" title="">
           
           <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Seções"><i class="fa fa-folder-open"></i><span class="hidden-minibar">Seções</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/sections/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Seções</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/sections/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Nova Seção</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/section/order')}}" data-original-title="Ordenar"><i class="fa fa-indent"></i><span class="hidden-minibar"> Ordenar Seções</span></a>
                </li>
              </ul>
          </li>
          <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Categorias"><i class="fa fa-list-ol"></i><span class="hidden-minibar">Categorias</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/category/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Categorias</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/category/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Nova Categoria</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/categories/order')}}" data-original-title="Ordenar"><i class="fa fa-indent"></i><span class="hidden-minibar"> Ordenar Categorias</span></a>
                </li>
              </ul>
          </li>
        </ul>
      </li> 
       @if(Auth::user()->id == 1 || Auth::user()->id == 7 || Auth::user()->id == 11 || Auth::user()->id == 12 || Auth::user()->id == 13)
      <li class="submenu">
        <a class="dropdown" href="#" data-original-title="Relatórios"><i class="fa fa-files-o"></i><span class="hidden-minibar">Formulários</span></a>
        <ul style="display: none;" data-original-title="" title="">
           <li class="submenu subMenuCustom">
            <a class="" href="{{URL::to('/adminContato/')}}" data-original-title="Templates"><i class="fa fa-file-text-o"></i><span class="hidden-minibar">Contato</span></a>
          </li>
           <li class="submenu subMenuCustom">
            <a class="" href="{{URL::to('/adminCurriculo/')}}" data-original-title="Templates"><i class="fa fa-file-text-o"></i><span class="hidden-minibar">Currículo</span></a>
          </li>
        </ul>
      </li>    
        <li class="submenu">
        <a class="dropdown" href="#" data-original-title="Relatórios"><i class="fa fa-cogs"></i><span class="hidden-minibar">Administração</span></a>
        <ul style="display: none;" data-original-title="" title="">
          <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Usuários"><i class="fa fa-group"></i><span class="hidden-minibar">Usuários</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/adminUser/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Usuários</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminUser/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Novo Usuário</span></a>
                </li>
              </ul>
          </li>
        </ul>
      </li>
      </li>    
        <li class="submenu">
        <a class="dropdown" href="#" data-original-title="Ads em Ação"><i class="fa fa-envelope"></i><span class="hidden-minibar">Ads em Ação</span></a>
        <ul style="display: none;" data-original-title="" title="">
          <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Notícias"><i class="fa fa-file-text-o"></i><span class="hidden-minibar">Notícias</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/adminAcaoNoticia/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Notícias</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminAcaoNoticia/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Nova Notícia</span></a>
                </li>
              </ul>
          </li>
          <li class="submenu subMenuCustom">
            <a class="dropdown" href="#" data-original-title="Boletins"><i class="fa fa-copy"></i><span class="hidden-minibar">Boletins</span></a>
              <ul style="display: none;" data-original-title="" title="">
                <li>
                  <a href="{{URL::to('/adminAcaoBolhetim/')}}" data-original-title="Listar"><i class="fa fa-list-ol"></i><span class="hidden-minibar"> Listar Boletins</span></a>
                </li>
                <li>
                  <a href="{{URL::to('/adminAcaoBolhetim/create')}}" data-original-title="Criar"><i class="fa fa-plus"></i><span class="hidden-minibar"> Novo Boletim</span></a>
                </li>
              </ul>
          </li>
        </ul>
      </li> 
      @endif
     
    </ul>
  </div>
</div>