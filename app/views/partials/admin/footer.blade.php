{{ HTML::script('assets/admin/js/less-1.5.0.min.js'); }}
{{ HTML::script('assets/admin/js/bootstrap.min.js'); }}
{{ HTML::script('assets/admin/js/bootstrap-select.js'); }}
{{ HTML::script('assets/admin/js/bootstrap-switch.js'); }}
{{ HTML::script('assets/admin/js/jquery.tagsinput.js'); }}
{{ HTML::script('assets/admin/js/jquery.placeholder.js'); }}
{{ HTML::script('assets/admin/js/bootstrap-typeahead.js'); }}
{{ HTML::script('assets/admin/js/application.js'); }}
{{ HTML::script('assets/admin/js/moment.min.js'); }}

{{ HTML::script('assets/admin/js/jquery.sortable.js'); }}
{{ HTML::script('assets/admin/js/jquery.gritter.js'); }}
{{ HTML::script('assets/admin/js/jquery.nicescroll.min.js'); }}
{{ HTML::script('assets/admin/js/prettify.min.js'); }}
{{ HTML::script('assets/admin/js/jquery.noty.js'); }}
{{ HTML::script('assets/admin/js/bic_calendar.js'); }}
{{ HTML::script('assets/admin/js/jquery.accordion.js'); }}
{{ HTML::script('assets/admin/js/skylo.js'); }}
{{ HTML::script('assets/admin/js/lightbox-2.6.min.js'); }}

{{ HTML::script('assets/admin/js/theme-options.js'); }}

{{ HTML::script('assets/admin/js/bootstrap-progressbar.js'); }}
{{ HTML::script('assets/admin/js/bootstrap-progressbar-custom.js'); }}
{{ HTML::script('assets/admin/js/bootstrap-colorpicker.min.js'); }}
{{ HTML::script('assets/admin/js/bootstrap-colorpicker-custom.js'); }}

{{ HTML::script('assets/admin/js/raphael-min.js'); }}
{{ HTML::script('assets/admin/js/morris-0.4.3.min.js'); }}
{{ HTML::script('assets/admin/js/morris-custom.js'); }}

{{ HTML::script('assets/admin/js/charts/jquery.sparkline.min.js'); }}

{{ HTML::script('assets/admin/js/nvd3/lib/d3.v3.js'); }}
{{ HTML::script('assets/admin/js/nvd3/nv.d3.js'); }}
{{ HTML::script('assets/admin/js/nvd3/src/models/legend.js'); }}
{{ HTML::script('assets/admin/js/nvd3/src/models/pie.js'); }}
{{ HTML::script('assets/admin/js/nvd3/src/models/pieChart.js'); }}
{{ HTML::script('assets/admin/js/nvd3/src/utils.js'); }}
{{ HTML::script('assets/admin/js/nvd3/sample.nvd3.js'); }}

{{ HTML::script('assets/admin/js/file-uploads-custom.js')}}
{{ HTML::script('assets/admin/js/jquery.fileupload.js')}}
{{ HTML::script('assets/admin/js/jquery.iframe-transport.js')}}
{{ HTML::script('assets/admin/js/jquery.knob.js')}}

{{ HTML::script('assets/admin/js/core.js')}}
{{ HTML::script('assets/admin/js/dashboard-custom.js')}}
{{ HTML::script('assets/admin/js/maskMoney/src/jquery.maskMoney.js')}}
{{ HTML::script('assets/admin/js/gallery-custom.js'); }}