<meta charset="utf-8">

  <title>ADS - Admin</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Loading Bootstrap -->
{{ HTML::style('assets/admin/css/bootstrap.css'); }}

<!-- Loading Stylesheets -->

{{ HTML::style('assets/admin/css/font-awesome.css'); }}
{{ HTML::style('assets/admin/css/style.css'); }}

<!-- Loading Custom Stylesheets -->
{{ HTML::style('assets/admin/css/custom.css'); }}
{{ HTML::style('assets/admin/js/bootstrap-multiselect/css/bootstrap-multiselect.css'); }}
{{ HTML::style('assets/admin/js/DataTables/media/css/jquery.dataTables.min.css'); }}


<link rel="shortcut icon" href="images/favicon.html">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

<!-- Load JS here for Faster site load =============================-->

{{ HTML::script('assets/admin/js/jquery.js'); }}
{{ HTML::script('assets/admin/js/jquery-ui-1.10.3.custom.min.js'); }}
{{ HTML::script('assets/admin/js/jquery.ui.touch-punch.min.js'); }}
{{ HTML::script('assets/admin/js/bootstrap-multiselect/js/bootstrap-multiselect.js'); }}

{{ HTML::script('assets/js/jqueryvalidate.js')}}
{{ HTML::script('assets/js/additional-methods.js')}}
{{ HTML::script('assets/js/ckeditor/ckeditor.js')}}
{{ HTML::script('assets/js/pluginMask.js')}}
{{ HTML::script('assets/js/validate.js')}}
{{ HTML::script('assets/admin/js/DataTables/media/js/jquery.dataTables.js'); }}
{{ HTML::script('//cdn.datatables.net/plug-ins/725b2a2115b/sorting/date-eu.js'); }}