<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbCases extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_cases', function($table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->text('resumo');
			$table->text('conteudo');
			$table->integer('status');
			$table->integer('ordem');
			
			$table->integer('idfile')->unsigned()->nullable();
			$table->integer('idservico')->unsigned()->nullable();
			$table->integer('idtraducao')->unsigned()->nullable();
			$table->integer('ididioma')->unsigned()->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_cases', function(Blueprint $table)
		{
			//
		});
	}

}
