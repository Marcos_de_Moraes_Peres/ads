<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbContatos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_contatos', function($table)
		{
			$table->increments('id');
			$table->string('nome');
			$table->string('empresa');
			$table->string('telefone');
			$table->string('email');
			$table->string('assunto');
			$table->text('mensagem');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_contatos', function(Blueprint $table)
		{
			//
		});
	}

}
