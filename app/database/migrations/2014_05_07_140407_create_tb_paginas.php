<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbPaginas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_paginas', function($table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->text('resumo');
			$table->text('conteudo');
			$table->integer('status');
			$table->integer('ordem');
			
			$table->integer('idtraducao')->unsigned()->nullable();
			$table->integer('ididioma')->unsigned()->nullable();
			$table->integer('idsecao')->unsigned()->nullable();
			$table->integer('idcategoria')->unsigned()->nullable();
			$table->integer('idfile')->unsigned()->nullable();
			$table->integer('idtemplate')->unsigned()->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_paginas', function(Blueprint $table)
		{
			//
		});
	}

}
