<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbIdiomas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_idiomas', function($table)
		{
			$table->increments('id');
			$table->string('nome');
			$table->string('sigla');
			
			$table->integer('idfile')->unsigned()->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_idiomas', function(Blueprint $table)
		{
			//
		});
	}

}
