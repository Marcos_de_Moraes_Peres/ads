<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbGaleriasLocais extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_galerias_locais', function($table)
		{
			$table->increments('id');			
			
			$table->integer('idgaleria')->unsigned()->nullable();
			$table->integer('idpagina')->unsigned()->nullable();
			$table->integer('idcase')->unsigned()->nullable();

			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_galerias_locais', function(Blueprint $table)
		{
			//
		});
	}

}
