<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbImagens extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_imagens', function($table)
		{
			$table->increments('id');
			$table->string('link');
			$table->integer('ordem');
			
			$table->integer('idgaleria')->unsigned()->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_imagens', function(Blueprint $table)
		{
			//
		});
	}

}
