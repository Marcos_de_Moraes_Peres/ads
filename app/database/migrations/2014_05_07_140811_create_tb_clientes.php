<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbClientes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_clientes', function($table)
		{
			$table->increments('id');
			$table->string('nome');
			$table->text('conteudo');
			$table->integer('status');
			$table->integer('ordem');
			
			$table->integer('idtraducao')->unsigned()->nullable();
			$table->integer('ididioma')->unsigned()->nullable();
			$table->integer('idfile')->unsigned()->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_clientes', function(Blueprint $table)
		{
			//
		});
	}

}
