<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbDestaques extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_destaques', function($table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->string('descricao');
			$table->string('link');
			$table->integer('ordem');
			
			$table->integer('idfile')->unsigned()->nullable();
			$table->integer('ididioma')->unsigned()->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_destaques', function(Blueprint $table)
		{
			//
		});
	}

}
