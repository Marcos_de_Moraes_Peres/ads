<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbSecoes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_secoes', function($table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->string('link');
			$table->integer('status');
			$table->integer('ordem');
			
			$table->integer('idtraducao')->unsigned()->nullable();
			$table->integer('ididioma')->unsigned()->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_secoes', function(Blueprint $table)
		{
			//
		});
	}

}
