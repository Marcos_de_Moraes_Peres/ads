<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbDepoimentos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_depoimentos', function($table)
		{
			$table->increments('id');
			$table->text('conteudo');
			$table->string('nome');
			$table->integer('status');
			$table->integer('ordem');
			
			$table->integer('idtraducao')->unsigned()->nullable();
			$table->integer('ididioma')->unsigned()->nullable();
			$table->integer('idcliente')->unsigned()->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_depoimentos', function(Blueprint $table)
		{
			//
		});
	}

}
