<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbNoticias extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_noticias', function($table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->text('resumo');
			$table->text('conteudo');
			$table->date('publicacao');
			$table->integer('status');
			
			$table->integer('idcliente')->unsigned()->nullable();
			$table->integer('idtraducao')->unsigned()->nullable();
			$table->integer('ididioma')->unsigned()->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_noticias', function(Blueprint $table)
		{
			//
		});
	}

}
