<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbSlides extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_slides', function($table)
		{
			$table->increments('id');
			$table->string('descricao');
			$table->string('link');
			$table->integer('status');
			$table->integer('ordem');
			
			$table->integer('idfile')->unsigned()->nullable();
			$table->integer('idtraducao')->unsigned()->nullable();
			$table->integer('ididioma')->unsigned()->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tb_slides', function(Blueprint $table)
		{
			//
		});
	}

}
