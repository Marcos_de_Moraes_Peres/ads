/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.filebrowserBrowseUrl = '/toniolli/public/assets/js/ckeditor/ckfinder/ckfinder.html',
	config.filebrowserImageBrowseUrl = '/toniolli/public/assets/js/ckeditor/ckfinder/ckfinder.html',
	config.filebrowserFlashBrowseUrl = '/toniolli/public/assets/js/ckeditor/ckfinder/ckfinder.html',
	config.filebrowserUploadUrl = '/toniolli/public/assets/js/ckeditor/ckfinder/core/connector/php/co­nnector.php?command=QuickUpload&type=Fil­es',
	config.filebrowserImageUploadUrl = '/toniolli/public/assets/js/ckeditor/ckfinder/core/connector/php/co­nnector.php?command=QuickUpload&type=Ima­ges',
	config.filebrowserFlashUploadUrl = '/toniolli/public/assets/js/ckeditor/ckfinder/core/connector/php/co­nnector.php?command=QuickUpload&type=Fla­sh'
	
};
